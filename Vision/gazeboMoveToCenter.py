#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.srv import GetModelState
import gazeboCameraCapture as capture
import cv2
import detectBoard
import gazeboStreamCamera

print("Started Gazebo move to center")
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")
gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
gripper_group.set_named_target("OpenGripper")
pose_target = geometry_msgs.msg.Pose()
plana = gripper_group.go()

# reset_world = rospy.ServiceProxy('/gazebo/reset_world', Empty)

# reset_world()
orientation_x = -0.7142126013476185
orientation_y = -0.00023103050360981063
orientation_z = -0.000746655961981416
orientation_w = 0.6999283886270427



# rospy.wait_for_service('/gazebo/get_model_state')
# model_coordinates  = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
# rospy.wait_for_service('/gazebo/set_model_state')
# set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)


def gohome():
    arm_group.set_named_target("Pick")
    plan1 = arm_group.go()
    rospy.sleep(1)



def getRobotLocation():
    next_pose_target = geometry_msgs.msg.Pose()
    print(arm_group.get_current_pose().pose.position.x * 1000)
    print(arm_group.get_current_pose().pose.position.y * 1000)


def alignRobot(center):
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_group.get_current_pose()
    next_pose_target.orientation.x = orientation_x
    next_pose_target.orientation.y = orientation_y
    next_pose_target.orientation.z = orientation_z
    next_pose_target.orientation.w = orientation_w
    next_pose_target.position.x = arm_group.get_current_pose().pose.position.x + float(center[0])/1000
    next_pose_target.position.y = arm_group.get_current_pose().pose.position.y - float(center[1])/1000 # + 90/1000
    next_pose_target.position.z = arm_group.get_current_pose().pose.position.z
    print(arm_group.get_current_pose().pose.position.x * 1000)
    print(arm_group.get_current_pose().pose.position.y * 1000)
    arm_group.set_pose_target(next_pose_target)
    plan2 = arm_group.go()

# gohome()
getRobotLocation()
# offset = gazeboStreamCamera.getOffset()
# print(offset)
# alignRobot(offset)
# offset = gazeboStreamCamera.getOffset()
# image = capture.capture()
# cv2.imshow('capture', image)
# cv2.waitKey(200)
# detectBoard.getImage(image)
# rospy.signal_shutdown()

