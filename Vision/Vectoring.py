#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.srv import GetModelState
import BoundingBoxes


moveit_commander.roscpp_initialize(sys.argv)
#rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")
gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
gripper_group.set_named_target("OpenGripper")
pose_target = geometry_msgs.msg.Pose()
plana = gripper_group.go()
rospy.sleep(3)



rospy.wait_for_service('/gazebo/get_model_state')
model_coordinates  = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
rospy.wait_for_service('/gazebo/set_model_state')

set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)


i = 1.1432198044719495


Error = 0.009170
X_POS = -0.128697
Y_POS = 0.328535
Z_POZ_DROP = 1.055
BOX_OFFSET = 0.0
MOVEOFFEST = 0.095
Grip_length = 0.209

orientation_x = -0.7142126013476185
orientation_y = -0.00023103050360981063
orientation_z = -0.000746655961981416
orientation_w = 0.6999283886270427




boxNameArray = [
"8_clone_0", "8_clone_1", "8_clone_2", "8_clone_3",
"7_clone_0", "7_clone_1", "7_clone_2", "7_clone_3", 
"6_clone_0", "6_clone_1", "6_clone_2", "6_clone_3",
"5_clone_0", "5_clone_1", "5_clone_2", "5_clone_3", 
"4_clone_0", "4_clone_1", "4_clone_2", "4_clone_3",  
"3_clone_0", "3_clone_1", "3_clone_2", "3_clone_3", 
"2_clone_0", "2_clone_1", "2_clone_2", "2_clone_3", 
"1_clone_0", "1_clone_1", "1_clone_2", "1_clone_3", 
]

boxNameArray = [
"18_clone_0", "18_clone_1", "18_clone_2", "18_clone_3",
"17_clone_0", "17_clone_1", "17_clone_2", "17_clone_3", 
"16_clone_0", "16_clone_1", "16_clone_2", "16_clone_3",
"12_clone_0", "12_clone_1", "12_clone_2", "12_clone_3", 
"11_clone_0", "11_clone_1", "11_clone_2", "11_clone_3", 
]


Error = 0.009170
X_POS = -0.128697
Y_POS = 0.328535
Z_POZ_DROP = 1.055
BOX_OFFSET = 0.0
MOVEOFFEST = 0.095
Grip_length = 0.209

orientation_x = -0.7142126013476185
orientation_y = -0.00023103050360981063
orientation_z = -0.000746655961981416
orientation_w = 0.6999283886270427



def gohome():
    arm_group.set_named_target("Pick")
    plan1 = arm_group.go()
    rospy.sleep(2)
    return


def getLocationGazebo():
    count = 0
    array = []
    for i in boxNameArray:
        #print(count)
        resp_coordinates = model_coordinates(i, "boardmaterial")
        factor = 1000
        
        x, y = resp_coordinates.pose.position.x * factor, (resp_coordinates.pose.position.y * factor)
        array.append((y, x))
        if count % 4 == 3: 
            print(array)
            array= []
        count += 1

def openGripper():
    gripper_group.set_joint_value_target([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    planb = gripper_group.go()
    rospy.sleep(1)


def pick(x):

    openGripper()

    print(x[1] + 10, x[0])
    pose_target.orientation.x = orientation_x
    pose_target.orientation.y = orientation_y
    pose_target.orientation.z = orientation_z
    pose_target.orientation.w = orientation_w
    pose_target.position.x = (x[1])/1000
    pose_target.position.y = x[0]/1000
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()


    #drop()
    #closeGripper()
    #raisegripper()


def goHome():

    pose_target.orientation.x = orientation_x
    pose_target.orientation.y = orientation_y
    pose_target.orientation.z = orientation_z
    pose_target.orientation.w = orientation_w
    pose_target.position.x = -600/1000
    pose_target.position.y = -600/1000
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

arr_matrix, _ = BoundingBoxes.runOneSequence()
location_matrix = BoundingBoxes.getFixedMatrix()
board_matrix = BoundingBoxes.getBoardMatrix()
#print(len(arr_matrix))
#print(location_matrix)
#getLocationGazebo()
#name = "3_clone_2"
#resp_coordinates = model_coordinates(name, "boardmaterial")
#print( resp_coordinates.pose.position.y * 1000, resp_coordinates.pose.position.x * 1000)
gohome()
goHome()
# row = 7 
# col = 1
# pick(location_matrix[row][col])
# rospy.sleep(2)
# row = 7 
# col = 3
# pick(location_matrix[row][col])
# rospy.sleep(2)
# row = 7 
# col = 5
# pick(location_matrix[row][col])
# rospy.sleep(2)
# row = 6
# col = 0
# pick(location_matrix[row][col])
# rospy.sleep(2)
# row = 6
# col = 2
# pick(location_matrix[row][col])
# rospy.sleep(2)
# row = 6
# col = 4
# pick(location_matrix[row][col])
# rospy.sleep(2)
row = 1
col = 1
pick(location_matrix[row][col])
rospy.sleep(2)
row = 1
col = 3
pick(location_matrix[row][col])
rospy.sleep(2)
rospy.sleep(2)
row = 1
col = 4
pick(location_matrix[row][col])
rospy.sleep(2)
# row = 0
# col = 0
# pick(location_matrix[row][col])
# rospy.sleep(2)
# goHome()




#loop though and subtract x/2 and and y/2