# import the necessary packages
import numpy as np
import imutils
import cv2
import argparse


class Stitcher:
    def __init__(self):
        # determine if we are using OpenCV v3.X
        self.isv3 = imutils.is_cv3(or_better=True)

    def stitch(self, imageA, imageB, ratio=0.75, reprojThresh=4.0, showMatches=False):
        # unpack the images, then detect keypoints and extract
        # local invariant descriptors from them
        # (imageB, imageA) = images
        imageA_Gray = cv2.cvtColor(imageA,cv2.COLOR_BGR2GRAY)
        imageB_Gray = cv2.cvtColor(imageB,cv2.COLOR_BGR2GRAY)
        descriptor = cv2.SIFT_create()
        kp1, des1 = descriptor.detectAndCompute(imageA_Gray,None)
        kp2, des2 = descriptor.detectAndCompute(imageB_Gray,None)

        # if the match is None, then there aren’t enough matched
        # keypoints to create a panorama
        
        match = cv2.BFMatcher()
        matches = match.knnMatch(des1,des2,k=2)


        good = []
        for m,n in matches:
            if m.distance < 0.03*n.distance:
                good.append(m)


        draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                        singlePointColor = None,
                        flags = 2)

        img3 = cv2.drawMatches(imageA,kp1,imageB,kp2,good,None,**draw_params)
        cv2.imshow("original_image_drawMatches.jpg", img3)
        
        MIN_MATCH_COUNT = 5
        print(len(good))
        if len(good) > MIN_MATCH_COUNT:
            src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
            dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
            print(M)
            h,w = imageA_Gray.shape
            pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
            dst = cv2.perspectiveTransform(pts, M)
            imageB_Gray = cv2.polylines(imageB_Gray,[np.int32(dst)],True,255,3, cv2.LINE_AA)
            #cv2.imshow("original_image_overlapping.jpg", img2)
        else:
            print("Not enought matches are found - %d/%d", (len(good)/MIN_MATCH_COUNT))

        dst = cv2.warpPerspective(imageA,M,(imageB.shape[1] + imageA.shape[1], imageB.shape[0]))
        dst[0:imageB.shape[0],0:imageB.shape[1]] = imageB
        cv2.imshow("original_image_stitched.jpg", dst)

        def trim(frame):
            #crop top
            if not np.sum(frame[0]):
                return trim(frame[1:])
            #crop top
            if not np.sum(frame[-1]):
                return trim(frame[:-2])
            #crop top
            if not np.sum(frame[:,0]):
                return trim(frame[:,1:])
            #crop top
            if not np.sum(frame[:,-1]):
                return trim(frame[:,:-2])
            return frame

        cv2.imshow("original_image_stitched_crop.jpg", trim(dst))
        cv2.waitKey(0)



imageA = cv2.imread('/home/tobenna/robot_capstone/Vision/GazeboCapture/Image1.jpg')
imageB = cv2.imread('/home/tobenna/robot_capstone/Vision/GazeboCapture/Image2.jpg')
# imageA = imutils.resize(imageA, width=400)
# imageB = imutils.resize(imageB, width=400)
# stitch the images together to create a panorama
stitcher = Stitcher()
stitcher.stitch(imageA, imageB, showMatches=True)
# show the images

cv2.imshow("Keypoint Matches", vis)
cv2.imshow("Result", result)
cv2.waitKey(222220)
