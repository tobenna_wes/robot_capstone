
from IPython.display import Image, display
from PIL import Image 
import cv2 
import numpy as np
from pylab import *
import skimage
from skimage import io
from skimage.viewer import ImageViewer
import sys
import pandas as pd
from matplotlib import pyplot as plt
import glob
import json

# uploaded = files.upload()

#open image and make into an array
final_arr = []
images={}
for i in range(1,2):
  if (i < 10):
    image = np.array(Image.open("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png"))
  elif (i<100):
    image = np.array(Image.open("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png"))
  else:
    image = np.array(Image.open("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png"))


  #cv2_imshow(image)
  #height and width of image
  height = image.shape[0]
  width = image.shape[1]
  # define range of blue color in HSV
  #lower_blue = np.array([110,50,50])
  #upper_blue = np.array([130,255,255])
  # define range of blue color in BGR
  lower_blue = np.array([110,50,50])
  upper_blue = np.array([130,255,255])
  #define range of red colur in BGR
  lower_red = np.array([50,50,110])
  upper_red = np.array([255,255,130])
  
  #initialising capture matrix
  rows, cols = (8,8)
  arr_matrix = [[0 for i in range(cols)] for j in range(rows)]
  #for row in arr_matrix: 
      #print(row)

  #initialise pixel array
  arr_pixel = np.array([0,0,0])

  #finding where pieces are
  x,y = (0,0)
  count = 0
  vals = [32, 96, 160, 224, 288, 352, 416, 480]

  for x in range(height):

    for y in range(width):

      #checking first block
      if x in vals and y in vals:
        arr_pixel = image[x][y]
        # print(x,y)
        # print(image[x][y])
        # print(arr_pixel)

        if (arr_pixel[0] > 150) and (arr_pixel[1] < 100) and (arr_pixel[2] < 100):
          arr_matrix[x//64][y//64] = 2 #blue cube
        elif (arr_pixel[0] > 100) and (arr_pixel[1] < 100) and (arr_pixel[2] > 150):
          arr_matrix[x//64][y//64] = 1 #red cube
        else:
          arr_matrix[x//64][y//64] = 0 #empty

      #checking rest of blocks
      # if x in vals and y in vals:
      #   arr_pixel = image[x][y]
  final_arr.append(arr_matrix)
    

  #convert into JSON file 
  images["image" + str(i)]=arr_matrix


for arr in final_arr:
    print(arr)

json.dumps(images)
with open('/home/tobenna/robot_capstone/Vision_ws/OutputFolder/matrix.json', 'w') as outfile:
      json.dump(images, outfile)
