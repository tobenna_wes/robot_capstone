##BOUNDING BOX INITALISING

import cv2
from IPython.display import Image, display
from PIL import Image 
import cv2 as cv 
import numpy as np
from pylab import *
import skimage
from skimage import io
from skimage.viewer import ImageViewer
import sys
import pandas as pd
from matplotlib import pyplot as plt
import glob
import json


#image = cv2.imread("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png")
#start_point = (0,0)
#end_point = (64,64)
#color = (0, 255 ,0)
#thickness = 2
#image = cv2.rectangle(image, start_point, end_point, color, thickness)
#cv2.imshow("image", image)
#cv2.waitKey()
#cv2_imshow(image)


#open image and make into an array
final_arr = []
images={}
global image 
# for i in range(1,100):
i = 1
if (i < 10):
  image = np.array(Image.open("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/Screenshot from 2021-03-09 15-36-19.png"))
elif (i<100):
  image = np.array(Image.open("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png"))
else:
  image = np.array(Image.open("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png"))


# cv2_imshow(image)
#height and width of image
height = image.shape[0]
width = image.shape[1]
print("Height:" , height)
print("width:" , width)
# define range of blue colour in HSV
#lower_blue = np.array([110,50,50])
#upper_blue = np.array([130,255,255])
# define range of blue colour in RGB
lower_blue = np.array([110,50,50])
upper_blue = np.array([130,255,255])
#define range of red colour in RGB
lower_red = np.array([50,50,110])
upper_red = np.array([255,255,130])

#initialising capture matrix
rows, cols = (8,8)
arr_matrix = [[0 for i in range(cols)] for j in range(rows)]
#for row in arr_matrix: 
    #print(row)

#initialise pixel array
arr_pixel = np.array([0,0,0])

#finding where pieces are
x,y = (0,0)
count = 0
vals = [32, 96, 160, 224, 288, 352, 416, 480] #


ZoneSize = height/8
intZoneSize = int(floor(ZoneSize))
initMidPoint = ZoneSize/2
print(ZoneSize)
vals = []
for x in range(8):
  temp = floor(initMidPoint + ZoneSize * x)
  vals.append(temp)
  print(vals)



def points(x,y):
  temp_mid = int(initMidPoint) 
  start_point = (y-temp_mid,x-temp_mid)
  end_point = (y+temp_mid, x+temp_mid)

  return (start_point , end_point)

def boundBox(x,y):
  global image
  (z, w) = points(x,y)
  color = (0, 255 ,0)
  thickness = 2
  #adding box around cube
  image = cv2.rectangle(image, z, w, color, thickness) 


for x in range(height):

  for y in range(width):

    #checking first block
    if x in vals and y in vals:
      arr_pixel = image[x][y]
      
      piece_type = 0
      if (arr_pixel[0] > 200) and (arr_pixel[1] < 50) and (arr_pixel[2] < 50):
        piece_type = 2 #blue 
        print("blue")
        boundBox(x,y)
         
      elif (arr_pixel[0] < 100) and (arr_pixel[1] < 100) and (arr_pixel[2] > 150):
        piece_type = 1 #red 
        print("red")
        boundBox(x,y)
     
      else:
        piece_type = 0 #empty

      arr_matrix[x//intZoneSize][y//intZoneSize] = piece_type

    #checking rest of blocks
    # if x in vals and y in vals:
    #   arr_pixel = image[x][y]
final_arr.append(arr_matrix)
   
#PRINITING IN TO JSON FILE
#images["image" + str(i)]= arr_matrix

for arr in final_arr:
    print(arr)

cv2.imshow("image", image)
cv2.waitKey()

