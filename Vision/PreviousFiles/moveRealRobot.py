#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.srv import GetModelState


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

arm_group = moveit_commander.MoveGroupCommander("manipulator")
pose_target = geometry_msgs.msg.Pose()

orientation_x = -0.7142126013476185
orientation_y = -0.00023103050360981063
orientation_z = -0.000746655961981416
orientation_w = 0.6999283886270427


def gohome():
    arm_group.set_named_target("Pick")
    plan1 = arm_group.go()
    rospy.sleep(2)
    return