
from IPython.display import Image, display
from PIL import Image 
import cv2
import numpy as np
from pylab import *
import skimage
from skimage import io
from skimage.viewer import ImageViewer
import sys
import pandas as pd
from matplotlib import pyplot as plt
import glob
import json
import imutils
image = cv2.imread("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/Screenshot from 2021-03-09 15-36-19.png")
# cv2_imshow(image)
# cv2_imshow(image)
#height and width of image
height = image.shape[0]
width = image.shape[1]
# define range of blue colour in HSV
#lower_blue = np.array([110,50,50])
#upper_blue = np.array([130,255,255])
# define range of blue colour in RGB
lower_blue = np.array([110,50,50])
upper_blue = np.array([130,255,255])
#define range of red colour in RGB
lower_red = np.array([50,50,110])
upper_red = np.array([255,255,130])

#initialising capture matrix
rows, cols = (8,8)
arr_matrix = [[0 for i in range(cols)] for j in range(rows)]
#for row in arr_matrix: 
    #print(row)

#initialise pixel array
arr_pixel = np.array([0,0,0])
x,y = (0,0)
count = 0
vals = [32, 96, 160, 224, 288, 352, 416, 480]


class ShapeDetector:
	def __init__(self):
		pass

	def detect(self, c):
		# initialize the shape name and approximate the contour
		shape = "unidentified"
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.04 * peri, True)
   
		# if the shape has 4 vertices, it is either a square or
		# a rectangle
		if len(approx) <= 4:
			# compute the bounding box of the contour and use the
			# bounding box to compute the aspect ratio
			(x, y, w, h) = cv2.boundingRect(approx)
			ar = w / float(h)
			# a square will have an aspect ratio that is approximately
			# equal to one, otherwise, the shape is a rectangle
			shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"
		
		# otherwise, we assume the shape is a circle
		else:
			shape = "circle"
		# return the name of the shape
		return shape

def loopImages():    
  imagecount = 0
  for x in range(height):

    for y in range(width):

      #checking first block
      if x in vals and y in vals:
        is_black = False
        if (x//64) % 2 == (y//64) % 2:
          is_black = True
        # arr_pixel = image[x][y]
        # sb = 0
        # sg = 0
        # sr = 0
        # for j in range(-31,31):
        #   for k in range(-31, 31):
        #     sb = image[x-j][y-k][0]
        #     sg = image[x-j][y-k][1]
        #     sr = image[x-j][y-k][2]
            
        
        # sb /= 3844
        # sg /= 3844
        # sr /= 3844
        # arr_pixel = [sb, sg, sr]
        # print(x,y)
        # print(image[x][y])
        # print(arr_pixel)
        flag = False
        for j in range(-16, 16):
          if flag:
            break
          for k in range(-16, 16):
            arr_pixel = image[x-j][y-k]
            if (arr_pixel[0] > 150) and (arr_pixel[1] < 100) and (arr_pixel[2] < 100):
              arr_matrix[x//64][y//64] = 2 #blue 
              flag = True
              # print("blue: ", arr_pixel)
              start_point = (y-32,x-32)
              end_point=(y+32,x+32)
              color = (255, 0 ,0)
              thickness = 2
              #adding box around cube
              # image = cv2.rectangle(image, start_point, end_point, color, thickness)
              image_temp = image[x-28:x+28, y-28:y+28]
              #image = image[0:64][192:256][:]
              
              cv2.imshow("image", image_temp)
              cv2.waitKey(300)
              imagecount += 1
              print(imagecount)

              resized = imutils.resize(image_temp, width=300)
              ratio = image_temp.shape[0] / float(resized.shape[0])
              # convert the resized image to grayscale, blur it slightly,
              # and threshold it
              gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)

              blurred = cv2.GaussianBlur(gray, (5, 5), 0)
              if is_black:
                thresh = cv2.threshold(blurred, 40, 255, cv2.THRESH_BINARY)[1]
              else:
                thresh = cv2.threshold(blurred, 80, 255, cv2.THRESH_BINARY)[1]
                thresh = cv2.bitwise_not(thresh)
              # cv2_imshow(thresh)
              
              cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
              cnts = imutils.grab_contours(cnts)
              sd = ShapeDetector()
              # cv2_imshow(image)
              # loop over the contours
              for c in cnts:
                # compute the center of the contour, then detect the name of the
                # shape using only the contour
                M = cv2.moments(c)
                cX = int((M["m10"] / M["m00"]) * ratio)
                cY = int((M["m01"] / M["m00"]) * ratio)
                shape = sd.detect(c)
                # multiply the contour (x, y)-coordinates by the resize ratio,
                # then draw the contours and the name of the shape on the image
                c = c.astype("float")
                c *= ratio
                c = c.astype("int")
                cv2.drawContours(image_temp, [c], -1, (0, 255, 0), 2)
                cv2.putText(image_temp, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
                  0.5, (255, 255, 255), 2)
                # show the output image
                #cv2.imshow("image", image_temp)
                #cv2.waitKey()
                # cv2.waitKey(0)
              # cv2.putText(image, shape, (x + cX, y + cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
              break
            elif (arr_pixel[0] < 100) and (arr_pixel[1] < 100) and (arr_pixel[2] > 150):
              arr_matrix[x//64][y//64] = 1 #red 
              # print("red: ", arr_pixel)
              start_point = (y-32,x-32)
              end_point=(y+32,x+32)
              color = (0, 0 ,255)
              thickness = 2
              #adding box around cube
              # image = cv2.rectangle(image, start_point, end_point, color, thickness) 
              image_temp = image[x-28:x+28, y-28:y+28]
              # image = image[0:64][192:256][:]
              # cv2_imshow(image_temp)
              resized = imutils.resize(image_temp, width=300)
              ratio = image_temp.shape[0] / float(resized.shape[0])
              # convert the resized image to grayscale, blur it slightly,
              # and threshold it
              gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)

              blurred = cv2.GaussianBlur(gray, (5, 5), 0)
              if is_black:
                thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
              else:
                thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
                thresh = cv2.bitwise_not(thresh)
              # cv2_imshow(thresh)
              
              cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
              cnts = imutils.grab_contours(cnts)
              sd = ShapeDetector()
              # cv2_imshow(image)
              # loop over the contours
              for c in cnts:
                # compute the center of the contour, then detect the name of the
                # shape using only the contour
                M = cv2.moments(c)
                cX = int((M["m10"] / M["m00"]) * ratio)
                cY = int((M["m01"] / M["m00"]) * ratio)
                shape = sd.detect(c)
                # multiply the contour (x, y)-coordinates by the resize ratio,
                # then draw the contours and the name of the shape on the image
                c = c.astype("float")
                c *= ratio
                c = c.astype("int")
                cv2.drawContours(image_temp, [c], -1, (0, 255, 0), 2)
                cv2.putText(image_temp, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
                  0.5, (255, 255, 255), 2)
                # show the output image
                #cv2.imshow("image", image_temp)
                #cv2.waitKey()
              flag = True
              break
        if not flag:
          # print("none: ", arr_pixel)
          arr_matrix[x//64][y//64] = 0 #empty

  cv2.imshow("image", image)
  cv2.waitKey()

loopImages()