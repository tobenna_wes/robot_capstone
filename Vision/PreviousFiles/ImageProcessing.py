##IMAGE PROCESSING
from IPython.display import Image, display
from PIL import Image 
import cv2 
import numpy as np
from pylab import *
import skimage
from skimage import io
from skimage.viewer import ImageViewer
import sys
import pandas as pd
from matplotlib import pyplot as plt
import glob

# uploaded = files.upload()

#open image and make into an array
image = array(Image.open("/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png"))
cv2.imshow("image", image)
cv2.waitKey()


#converts image to grayscale
gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#cv2_imshow(gray_image) #shows image in greyscale

#blurs image
blur_image = cv2.GaussianBlur(image, (5,5), 0)
#cv2_imshow(blur_image) #shows blurred grey image

#applys adapting thresholding 
#thresh_image = cv.adaptiveThreshold(blur_image, 255, 1, 1, 11, 2)
#cv2_imshow(thresh_image)


#outputs image contoured
#contours, hierarchy = cv.findContours(thresh_image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)


#height and width of image
height = image.shape[0]
width = image.shape[1]

#height and width of grid
grid_h = height / 8 
grid_w = width / 8


# Check the image matrix data type (could know the bit depth of the image)
#print(image.dtype)


