#!/usr/bin/env python3
from IPython.display import Image, display
from PIL import Image
import cv2
import numpy as np
from pylab import *
import skimage
from skimage import io
from skimage.viewer import ImageViewer
import sys
import pandas as pd
from matplotlib import pyplot as plt
import glob
import json
import imutils

global image

image = cv2.imread(
    '/home/tobenna/robot_capstone/Vision/GazeboCapture/Image.jpeg')


def cropImage(image):
    alpha = 1.0  # Contrast control (1.0-3.0)

    beta = 0  # Brightness control (0-100)

    
    image = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)
    height = image.shape[0]
    width = image.shape[1]

    # image = image[int(height*0.2):int(height*0.78):,int(width*0.3):int(width*0.73)]
    # image = image[int(height*0.33):int(height*0.66):,int(width*0.38):int(width*0.63)]
    # image = image[int(height*0.32):int(height*0.67):,int(width*0.375):int(width*0.638)]
    image = image[int(height*0.3):int(height*1.0):,int(width*0.35):int(width*0.8)]

    scale_percent = 350
    width = int(image.shape[1] * scale_percent / 100)
    height = int(image.shape[0] * scale_percent / 100)

    # dsize
    dsize = (width, height)

    # resize image
    image = cv2.resize(image, dsize)
    cv2.imshow('img', image)
    cv2.waitKey()
    #image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE) 

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    return image



cropImage(image)
