#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 
from std_srvs.srv import Empty
import BoundingBoxes 
import cv2
import isolateandCenterImage as isolated
# import gazeboGripper as gripper
import realGripper as gripper
from checkers_ai.srv import *
from contToAI import contToAIfunc, checkers_client, usage
import math

print("Started Gazebo move to center")
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_robot_to_center', anonymous=True)
robot = moveit_commander.RobotCommander()


# real_robot = True
dev_mode = False
arm_group = None


if gripper.real_robot:
    arm_group = moveit_commander.MoveGroupCommander("manipulator")
else:
    arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")


isolated.dev_mode = dev_mode

def gohome():
    arm_group.set_named_target("GoodCaptureCenter")
    arm_group.set_max_velocity_scaling_factor(0.05)
    arm_group.set_max_acceleration_scaling_factor(0.05)
    plan1 = arm_group.go()
    rospy.sleep(1)



def getFromRobotLocation():
    location_x = arm_group.get_current_pose().pose.position.x * 1000
    location_y = arm_group.get_current_pose().pose.position.y * 1000
    try:
        while True:
            # next_pose_target = geometry_msgs.msg.Pose()
            # print(arm_group.get_current_pose())
            new_location_x = arm_group.get_current_pose().pose.position.x * 1000
            new_location_y = arm_group.get_current_pose().pose.position.y * 1000
            print(int(new_location_x - location_x), int(new_location_y - location_y))
            # print(arm_group.get_current_pose().pose.position.y * 1000)
            rospy.sleep(2)
    except KeyboardInterrupt:
        print('interrupted!')

def getRobotLocation():
    print(arm_group.get_current_pose().pose)
    rospy.sleep(2)

def movetoTrash():
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_group.get_current_pose().pose.position
    next_pose_target.orientation = center_ori
    next_pose_target.position.x = robot_pose.x
    next_pose_target.position.y = 0.556805 
    next_pose_target.position.z = robot_pose.z
    arm_group.set_pose_target(next_pose_target)
    plan2 = arm_group.go()
    rospy.sleep(2)
    gripper.openGripper()
    rospy.sleep(1)

def pickKing():
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_group.get_current_pose().pose.position
    next_pose_target.orientation = center_ori
    next_pose_target.position.x = robot_pose.x
    next_pose_target.position.y = 0.556805 
    next_pose_target.position.z = robot_pose.z
    arm_group.set_pose_target(next_pose_target)
    plan2 = arm_group.go()
    rospy.sleep(2)
    gripper.openGripper()
    rospy.sleep(1)


def alignRobot(center):
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_groups.get_current_pose()
    next_pose_target.orientation.x = orientation_x
    next_pose_target.orientation.y = orientation_y
    next_pose_target.orientation.z = orientation_z
    next_pose_target.orientation.w = orientation_w
    next_pose_target.position.x = arm_group.get_current_pose().pose.position.x + float(center[0])/1000
    next_pose_target.position.y = arm_group.get_current_pose().pose.position.y - float(center[1])/1000 # + 90/1000
    next_pose_target.position.z = arm_group.get_current_pose().pose.position.z
    print(arm_group.get_current_pose().pose.position.x * 1000)
    print(arm_group.get_current_pose().pose.position.y * 1000)
    arm_group.set_pose_target(next_pose_target)
    plan2 = arm_group.go()



def centerRobot(): 
    global center_x, center_y, center_ori
    isNotAligned = True
    center_ori = arm_group.get_current_pose().pose.orientation
    while isNotAligned:

        offset = BoundingBoxes.captureOffset()
        print("allignment started" , offset)
        if abs(offset[0]) <= 5 and abs(offset[1]) <= 5:
            print("Algined")
            isNotAligned = False
            break
        else:

            next_pose_target = geometry_msgs.msg.Pose()
            robot_pose = arm_group.get_current_pose()
            next_pose_target.orientation = center_ori
            next_pose_target.position.x = robot_pose.pose.position.x + float(offset[0] * factor)/1000
            next_pose_target.position.y = robot_pose.pose.position.y - float(offset[1] * factor)/1000 # + 90/1000
            next_pose_target.position.z = robot_pose.pose.position.z
            arm_group.set_max_velocity_scaling_factor(speed)
            arm_group.set_max_acceleration_scaling_factor(speed)
            center_x = next_pose_target.position.x
            center_y = next_pose_target.position.y
            arm_group.set_pose_target(next_pose_target)
            plan2 = arm_group.go()
            rospy.sleep(2)
            print("Running Again")

def moveCloser(height):

    # new_ori = arm_group.get_current_pose().pose.orientation
    new_pos = arm_group.get_current_pose().pose.position
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_group.get_current_pose()
    next_pose_target.orientation = center_ori
    next_pose_target.position = new_pos
    next_pose_target.position.z = height
    arm_group.set_pose_target(next_pose_target)
    plan2 = arm_group.go()
        # rospy.sleep(2)


def dropCloser(height, points = 4):

    # new_ori = arm_group.get_current_pose().pose.orientation
    new_pos = arm_group.get_current_pose().pose
    points = points
    robot_height = new_pos.position.z
    dis_travel = height - robot_height
    new_point = float(dis_travel) / points
    
    for i in range(points):

        # print(i)
        # print(robot_height + (i + 1) * new_point)
        new_pos = arm_group.get_current_pose().pose
        # print(new_pos)
        next_pose_target = geometry_msgs.msg.Pose()
        robot_pose = arm_group.get_current_pose()
        next_pose_target.orientation = center_ori
        next_pose_target.position = new_pos.position
        next_pose_target.position.z = robot_height + (i + 1) * new_point
        arm_group.set_max_velocity_scaling_factor(speed)
        arm_group.set_max_acceleration_scaling_factor(speed)
        arm_group.set_pose_target(next_pose_target)
        plan2 = arm_group.go()
        # rospy.sleep(2)

def moveXYcloser(x,y):

    # new_ori = arm_group.get_current_pose().pose.orientation
    new_pos = arm_group.get_current_pose().pose.position
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_group.get_current_pose()
    next_pose_target.orientation = center_ori
    next_pose_target.position = new_pos
    next_pose_target.position.y = new_pos.y - y/1000
    next_pose_target.position.x = new_pos.x - x/1000
    arm_group.set_pose_target(next_pose_target)
    arm_group.set_max_velocity_scaling_factor(speed)
    arm_group.set_max_acceleration_scaling_factor(speed)
    plan2 = arm_group.go()
    rospy.sleep(2)


def moveToPoint(row , col): 
    
    colorMatrix = BoundingBoxes.captureTypeMatrix()
    print(colorMatrix)
    LocationMatrix = BoundingBoxes.captureLocationMatrix()
    # print(LocationMatrix)
    isNotAligned = True
    moveCloser(gripper.closer_nav_height)
    # new_ori = arm_group.get_current_pose().pose.orientation
    new_pos = arm_group.get_current_pose().pose.position
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_group.get_current_pose()
    next_pose_target.orientation = center_ori
    next_pose_target.position.x = new_pos.x - float(LocationMatrix[row][col][0] * close_up_factor)/1000
    next_pose_target.position.y = new_pos.y + float(LocationMatrix[row][col][1] * close_up_factor)/1000 # + 90/1000
    next_pose_target.position.z = new_pos.z
    arm_group.set_max_velocity_scaling_factor(speed)
    arm_group.set_max_acceleration_scaling_factor(speed)
    arm_group.set_pose_target(next_pose_target)
    plan2 = arm_group.go()
    rospy.sleep(2)
    print("Moved to Point")


def moveToBoardPoint(row , col): 
    

    LocationMatrix = BoundingBoxes.captureBoardMatrix()
    # print(LocationMatrix)
    isNotAligned = True
    dropCloser(gripper.closer_nav_height)
    new_ori = arm_group.get_current_pose().pose.orientation
    new_pos = arm_group.get_current_pose().pose.position
    print(LocationMatrix[row][col])
    next_pose_target = geometry_msgs.msg.Pose()
    robot_pose = arm_group.get_current_pose()
    next_pose_target.orientation = center_ori
    next_pose_target.position.x = center_x - float(LocationMatrix[row][col][0] * close_up_factor)/1000
    next_pose_target.position.y = center_y + float(LocationMatrix[row][col][1] * close_up_factor)/1000 # + 90/1000
    next_pose_target.position.z = new_pos.z
    arm_group.set_max_velocity_scaling_factor(speed)
    arm_group.set_max_acceleration_scaling_factor(speed)
    arm_group.set_pose_target(next_pose_target)
    plan2 = arm_group.go()
    rospy.sleep(2)
    # print("Running Again")

def centerPoint(row , col): 
    isNotAligned = True
    colorMatrix = BoundingBoxes.captureTypeMatrix()
    color = colorMatrix[row][col]
    print(colorMatrix)
    # new_ori = arm_group.get_current_pose().pose.orientation
    while isNotAligned:

        offset = isolated.captureOffset(color)
        print("allignment started" , offset)
        if abs(offset[0]) <= 5 and abs(offset[1]) <= 5:
            print("Algined")
            isNotAligned = False
            break
        else:

            next_pose_target = geometry_msgs.msg.Pose()
            robot_pose = arm_group.get_current_pose()
            next_pose_target.orientation = center_ori
            next_pose_target.position.x = arm_group.get_current_pose().pose.position.x + float(offset[0] * alignment_factor)/1000
            next_pose_target.position.y = arm_group.get_current_pose().pose.position.y - float(offset[1] * alignment_factor)/1000 # + 90/1000
            next_pose_target.position.z = arm_group.get_current_pose().pose.position.z
            # print(arm_group.get_current_pose().pose.position.x * 1000)
            # print(arm_group.get_current_pose().pose.position.y * 1000)
            arm_group.set_max_velocity_scaling_factor(speed)
            arm_group.set_max_acceleration_scaling_factor(speed)
            arm_group.set_pose_target(next_pose_target)
            plan2 = arm_group.go()
            rospy.sleep(2)
            print("Running Again")


def centerKingPoint(color): 
    isNotAligned = True

    # new_ori = arm_group.get_current_pose().pose.orientation
    while isNotAligned:

        offset = isolated.captureOffset(color)
        print("allignment started" , offset)
        if abs(offset[0]) <= 5 and abs(offset[1]) <= 5:
            print("Algined")
            isNotAligned = False
            break
        else:

            next_pose_target = geometry_msgs.msg.Pose()
            robot_pose = arm_group.get_current_pose()
            next_pose_target.orientation = center_ori
            next_pose_target.position.x = arm_group.get_current_pose().pose.position.x + float(offset[0] * alignment_factor)/1000
            next_pose_target.position.y = arm_group.get_current_pose().pose.position.y - float(offset[1] * alignment_factor)/1000 # + 90/1000
            next_pose_target.position.z = arm_group.get_current_pose().pose.position.z
            # print(arm_group.get_current_pose().pose.position.x * 1000)
            # print(arm_group.get_current_pose().pose.position.y * 1000)
            arm_group.set_max_velocity_scaling_factor(speed)
            arm_group.set_max_acceleration_scaling_factor(speed)
            arm_group.set_pose_target(next_pose_target)
            plan2 = arm_group.go()
            rospy.sleep(2)
            print("Running Again")


def pickPiece():

    gripper.openGripper()
    rospy.sleep(0.5)
    moveXYcloser(gripper.camera_offset_x, gripper.camera_offset_y)
    rospy.sleep(0.5)
    dropCloser(gripper.closer_drop_height, 3)
    rospy.sleep(0.5)
    gripper.closeGripper()
    rospy.sleep(1)
    dropCloser(gripper.closer_nav_height, 3)
    rospy.sleep(0.5)



def dropPiece():

    moveXYcloser(gripper.camera_offset_x, gripper.camera_offset_y )
    rospy.sleep(0.5)
    moveCloser(gripper.closer_drop_height)
    rospy.sleep(0.5)
    gripper.openGripper()
    rospy.sleep(1)
    moveCloser(gripper.closer_nav_height)
    rospy.sleep(0.5)


def convertToMatrix(location):
    row = math.floor(location / 4)
    col = (location % 4) * 2
    if row % 2 == 0:
        col += 1

    return row, col

def getNextMove():
    gripper.activateGripper()
    try:
        while True: 
            sec = input('Say when:')
            if sec  == "a":
                contArry = BoundingBoxes.captureTypeMatrix()
                print(contArry)
                tup = contToAIfunc(contArry)
                currPlayer = tup[0]
                oppntPlayer = tup[1]
                bothIsKing = tup[2]
                moveDir = tup[3]
                print("Request: %s %s %s %s"%(currPlayer, oppntPlayer, bothIsKing, moveDir))
                print("\nResponse:")
                resp = checkers_client(currPlayer, oppntPlayer, bothIsKing, moveDir)
                startPt = resp.startPt
                endPts = list(resp.endpts)
                captured = list(resp.captured)
                #if makeKing is not -1 then you change the piece at makeKing to a king
                makeKing = resp.makeKing
                #*****************************************************
                print("Client Exiting...")

                start_row, start_col = convertToMatrix(startPt)

                last_end = len(endPts) - 1
                end_row, end_col = convertToMatrix(endPts[last_end])

                
                # gohome()
                # centerRobot()
                moveToBoardPoint(start_row,start_col)
                rospy.sleep(0.5)
                centerPoint(start_row, start_col)
                rospy.sleep(0.5)
                pickPiece()
                moveToBoardPoint(end_row, end_col)
                rospy.sleep(0.5)
                centerPoint(end_row, end_col)
                rospy.sleep(0.5)
                dropPiece()
                rospy.sleep(0.5)




                if makeKing != -1:
                    moveToBoardPoint(end_row, end_col)
                    rospy.sleep(0.5)
                    centerKingPoint(1)
                    rospy.sleep(0.5)
                    pickPiece()
                    rospy.sleep(0.5)
                    movetoTrash()
                    rospy.sleep(0.5)
                    moveToBoardPoint(0,0)
                    centerKingPoint(3)
                    rospy.sleep(0.5)
                    pickPiece()
                    rospy.sleep(0.5)
                    moveToBoardPoint(end_row, end_col)
                    rospy.sleep(0.5)
                    centerKingPoint(0)
                    rospy.sleep(0.5)
                    dropPiece()


                for i in captured: 
                    rospy.sleep(0.5)
                    mid_row, mid_col = convertToMatrix(i)
                    moveToBoardPoint(mid_row, mid_col)
                    rospy.sleep(0.5)
                    centerPoint(mid_row, mid_col)
                    rospy.sleep(0.5)
                    pickPiece()
                    rospy.sleep(0.5)
                    movetoTrash()
                    rospy.sleep(0.5)


                gohome()
                centerRobot()

    except KeyboardInterrupt:
        print('interrupted!')
        cv2.destroyAllWindows()
        rospy.signal_shutdown()
        pass



    
















# print(BoundingBoxes.captureLocationMatrix())
# factor = 2.55
factor = gripper.axis_scale
close_up_factor = gripper.close_up_factor
alignment_factor = gripper.alignment_factor
speed = gripper.robot_speed

center_ori = geometry_msgs.msg.Pose().orientation
center_x = 0 
center_y = 0


gripper.resetGripper()
gripper.activateGripper()
gohome()
centerRobot()
getNextMove()



# centerRobot()
# contArry = BoundingBoxes.captureTypeMatrix()
# dropCloser(gripper.closer_nav_height)

# moveToBoardPoint(1,6)
# centerPoint(1, 6)
# gripper.openGripper()

# moveXYcloser(gripper.camera_offset_x, gripper.camera_offset_y)
# dropCloser(gripper.closer_drop_height, 3)
# gripper.closeGripper()
# rospy.sleep(1)
# dropCloser(gripper.closer_nav_height, 3)

# moveToBoardPoint(3, 4)
# centerPoint(3, 4)


# moveXYcloser(gripper.camera_offset_x, gripper.camera_offset_y )
# moveCloser(gripper.closer_drop_height)
# gripper.openGripper()
# rospy.sleep(1)
# moveCloser(gripper.closer_nav_height)

# pickPiece()
# moveToBoardPoint(0,3)
# centerPoint(0, 3)
# # moveToBoardPoint(3,6)
# # centerPoint(3, 6)
# moveToBoardPoint(7,6)
# centerPoint(7, 6)
# moveToBoardPoint(5,2)
# centerPoint(5, 2)
# gohome()
# centerRobot()
# moveCloser(gripper.closer_nav_height)

# gohome()
# centerRobot()
# moveCloser(gripper.closer_nav_height)
# moveToBoardPoint(2,0)
# centerPoint(2, 0)












# getRobotLocation()
# centerRobot()
# moveToBoardPoint(2,0)
# centerPoint(2,0)
# pickPiece()
# moveToBoardPoint(4,4)
# centerPoint(4,4)
# dropPiece()
# rospy.sleep(1)


# gohome()
# centerRobot()
# moveToBoardPoint(3,5)
# centerPoint(3,5)
# rospy.sleep(3)

# gohome()
# centerRobot()
# moveToBoardPoint(3,7)
# centerPoint(3,7)
# rospy.sleep(3)

# gohome()
# centerRobot()
# moveToBoardPoint(4,0)
# centerPoint(4,0)
# rospy.sleep(3)

# gohome()
# centerRobot()
# moveToPoint(0,0)
# centerPoint(0,0)
# rospy.sleep(3)


# gohome()
# centerRobot()
# moveToPoint(1,1)
# centerPoint(1,1)
# rospy.sleep(3)

# gohome()
# centerRobot()
# moveToPoint(1,1)
# rospy.sleep(6)
# gohome()
# centerRobot()
# moveToPoint(5,3)
# rospy.sleep(6)
# gohome()
# centerRobot()
# moveToPoint(2,4)






# gohome()
# getRobotLocation()
# gohome()
# offset = gazeboStreamCamera.getOffset()
# print(offset)
# alignRobot(offset)
# offset = gazeboStreamCamera.getOffset()

# detectBoard.getImage(image)
# rospy.signal_shutdown()

