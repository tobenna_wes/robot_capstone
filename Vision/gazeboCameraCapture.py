
#!/usr/bin/env python3
# rospy for the subscriber
import rospy
# ROS Image message
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2

# Instantiate CvBridge
bridge = CvBridge()

image = None
subscriber = None
image_topic = "/camera/color/image_raw"
# rospy.init_node('image_listener')


def image_callback(msg):
    global image
    print("Received an image!")
    try:
        # Convert your ROS Image message to OpenCV2
        cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
    except CvBridgeError as e:
        print(e)
    else:
        # Save your OpenCV2 image as a jpeg 
        time = msg.header.stamp
        cv2.imwrite('/home/tobenna/robot_capstone/Vision/GazeboImages/Image.jpeg', cv2_img)
        image = cv2_img
        rospy.sleep(1)

def main():
    global subscriber
    
    # Define your image topic
    #image_topic = "/camera/color/image_raw"
     #image_topic = "/camera/color/image_raw"
    # Set up your subscriber and define its callback
    subscriber = rospy.Subscriber(image_topic, Image, image_callback)
    # Spin until ctrl + c
    # rospy.spin()
    # rospy.sleep(2)

# if __name__ == '__main__':
#     main()



def stop():
    rospy.sleep(1)
    subscriber.unregister()

def start():
    global subscriber
    subscriber = rospy.Subscriber(image_topic, Image, image_callback)
    rospy.sleep(1)
    print("Started Captrue")


def capture():
    start()
    stop()
    return image







