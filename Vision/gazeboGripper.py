
#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.srv import GetModelState


closer_nav_height = 1
closer_drop_height = 0.82
camera_offset_x = 90
camera_offset_y = 17
axis_scale = 2.3
robot_speed = 0.1
real_robot = False


gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
gripper_group.set_named_target("OpenGripper")
plana = gripper_group.go()
rospy.sleep(3)


def closeGripper():
    Grip_length = 0.22
    gripper_group.set_joint_value_target([Grip_length, Grip_length, Grip_length, Grip_length, Grip_length, Grip_length])
    planb = gripper_group.go()
    rospy.sleep(1)
    return

def openGripper():
    gripper_group.set_joint_value_target([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    planb = gripper_group.go()
    rospy.sleep(1)
    return


openGripper()