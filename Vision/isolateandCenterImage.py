# import the necessary packages

import argparse
import imutils
import cv2
import math
from cv_bridge import CvBridge, CvBridgeError
import rospy
from sensor_msgs.msg import Image
import numpy as np
# rospy.init_node('isolated')
image = cv2.imread(
    "/home/tobenna/robot_capstone/Vision/GazeboCapture/cropped2.png")

color = 1

dev_mode = True


class getPieceCenter:

    def __init__(self):
        self.closest_object = None
        self.image_mid = None

    def isSmaller(self, new):

        distance_1 = math.sqrt(int((self.closest_object[0]-self.image_mid[0])**2) + int(
            (self.closest_object[1]-self.image_mid[1])**2))
        distance_2 = math.sqrt(
            int((new[0]-self.image_mid[0])**2) + int((new[1]-self.image_mid[1])**2))
        # print(distance_1, distance_2)
        if distance_1 > distance_2:
            self.closest_object = new
        # print(closest_object)

    def getOffset(self, image, color):

        scale_percent = 350
        width = int(image.shape[1])
        height = int(image.shape[0])

        start_point = (int( width * 0.3), int(height * 0.2 ))
        end_point = (int(width * 0.7), int(height * 0.8 ))


        image = image[start_point[1]:end_point[1],start_point[0]:end_point[0]]
            

        hsv = None
        top = 130
        bottom = 100

        blue, green, red = cv2.split(image)

        # print(blue)
        # we will go by x,y and do all three colors
        for row in range(blue.shape[0]):
            for col in range(blue.shape[1]):
                if (green[row][col] > 150):
                    #green or white
                    if (blue[row][col] < 150):
                        # green
                        blue[row][col] = 131
                        green[row][col] = 209
                        red[row][col] = 114
                    else:
                        # white
                        blue[row][col] = 255
                        green[row][col] = 255
                        red[row][col] = 255

                elif (red[row][col] > 145):
                    #red or white
                    if (green[row][col] < 130):
                        # red
                        blue[row][col] = 30
                        green[row][col] = 25
                        red[row][col] = 250
                    else:
                        # white
                        blue[row][col] = 255
                        green[row][col] = 255
                        red[row][col] = 255
                elif (blue[row][col] > 100):
                    #blue or white
                    if (red[row][col] < 110):
                        # blue
                        blue[row][col] = 210
                        green[row][col] = 108
                        red[row][col] = 31
                    else:
                        # white
                        blue[row][col] = 255
                        green[row][col] = 255
                        red[row][col] = 255
                else:
                    # black (but switch to white)
                    # blue[row][col] = 0
                    # green[row][col] = 0
                    # red[row][col] = 0
                    blue[row][col] = 255
                    green[row][col] = 255
                    red[row][col] = 255
        image = cv2.merge([blue, green, red])
        if color == 2 or color == 4 :
            hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        elif color == 1 or color == 3:
            hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        elif color == 0:
            hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
            top = 85
            bottom = 40
        hsv_channels = cv2.split(hsv)
        # gray = cv2.cvtColor(hsv.copy(), cv2.COLOR_RGBA2GRAY)
        # thresh = cv2.threshold(gray, 90, 255, cv2.THRESH_BINARY)[1]
        # kernel = np.ones((11,11), np.uint8)

        # image_new = cv2.erode(thresh, kernel, iterations=2)
        # cv2.imshow("gray", gray)
        # cv2.imshow("thresh", thresh)
        # cv2.imshow("thresh_new", image_new)
        # cv2.waitKey(0)

        rows = image.shape[0]
        cols = image.shape[1]

        self.closest_object = (rows, cols)

        image_mid_x = int(image.shape[1] / 2)
        image_mid_y = int(image.shape[0] / 2)
        self.image_mid = (image_mid_x, image_mid_y)

        for i in range(0, rows):
            for j in range(0, cols):
                h = hsv_channels[0][i][j]

                if h > bottom and h < top:
                    hsv_channels[2][i][j] = 255
                else:
                    hsv_channels[2][i][j] = 0

        # cv2.imshow("show", hsv_channels[0])
        image_new = hsv_channels[2].copy()
        kernel = np.ones((5, 5), np.uint8)

        image_new = cv2.erode(image_new, kernel, iterations=2)
        if dev_mode:
            cv2.imshow("black", image_new)
            cv2.waitKey(0)

        resized = imutils.resize(image_new, width=300)
        ratio = image_new.shape[0] / float(resized.shape[0])
        # convert the resized image to grayscale, blur it slightly,
        # and threshold it
        # gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(resized, (5, 5), 0)
        thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
        # find contours in the thresholded image and initialize the
        # shape detector
        cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)

        for c in cnts:
            # compute the center of the contour, then detect the name of the
            # shape using only the contour
            M = cv2.moments(c)
            if M["m00"] == 0 or M["m10"] == 0 or M["m01"] == 0:
                print("could not wrap")
                continue
            cX = int((M["m10"] / M["m00"]) * ratio)
            cY = int((M["m01"] / M["m00"]) * ratio)
            if cX == 0 or cY == 0:
                print("could not wrap")
                continue
            # multiply the contour (x, y)-coordinates by the resize ratio,
            # then draw the contours and the name of the shape on the image
            shape = "unidentified"
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.01 * peri, True)
            c = c.astype("float")
            c *= ratio
            c = c.astype("int")

            detected_shape = False
            if len(approx) <= 8:
                # compute the bounding box of the contour and use the
                # bounding box to compute the aspect ratio
                (a, b, w, h) = cv2.boundingRect(approx)
                ar = w / float(h)
                if ar >= 0.70 and ar <= 1.3:
                    # if ar == 1:
                    # print(ar)

                    shape = "square"
                    # print(shape)
                    detected_shape = True
                else:
                    # cv2.imshow("rectangle", thresh)
                    # cv2.waitKey(100)
                    detected_shape = True
                    shape = "rectangle"

            # otherwise, we assume the shape is a circle
            else:
                shape = "circle"
                detected_shape = True

            # return the name of the shape

            # cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
            # cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
            # 	0.5, (255, 255, 255), 2)
            # # show the output image

            if detected_shape:
                # print(shape)
                # print(len(approx))
                cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
                # cv2.putText(image_temp, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                # print(shape)
                # print(cX, cY)
                # print(x//intZoneSize,y//intZoneSize)
                # boxX =  x//self.intZoneSize
                # boxY =  y//self.intZoneSize
                # print(intZoneSize, intZoneSize)
                # centerX = int(boxX * self.intZoneSize + cY + addBackX)
                # centerY = int(boxY * self.intZoneSize + cX + addBackX)
                # self.location_matrix[x//self.intZoneSize][y//self.intZoneSize] = (centerX, centerY)
                # print(centerX, centerY)
                # cv2.putText(image, shape, (centerX, centerX), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.putText(image, shape, (cX, cY),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.circle(image, (cX, cY), radius=1,
                           color=(0, 0, 0), thickness=2)
                self.isSmaller((cX, cY))
                # cv2.imshow("image", image)
                # cv2.waitKey()

                # cv2.imshow("Image", image)
                # cv2.waitKey(10)

        offset = ((- self.closest_object[1] + self.image_mid[1]),
                  (self.closest_object[0] - self.image_mid[0]))
        print(offset)
        cv2.circle(image, self.closest_object, radius=5,
                   color=(0, 0, 225), thickness=4)
        cv2.circle(image, self.image_mid, radius=5,
                   color=(225, 0, 0), thickness=4)
        if dev_mode:
            cv2.imshow("Center Image", image)
            cv2.waitKey(0)

        if detected_shape == False:
            cv2.imshow("Center Image", image)
            cv2.waitKey(0)
        return offset


gpc = getPieceCenter()
subscriber = None
global_image = None
bridge = CvBridge()


def stop():
    rospy.sleep(0.5)
    cv2.destroyAllWindows()
    subscriber.unregister()


def image_callback(msg):
    global global_image
    # print("Received an image!" + str(count))

    try:
        # Convert your ROS Image message to OpenCV2
        cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
        # cv2.imshow("New image", cv2_img)
        # cv2.waitKey(1)
    except CvBridgeError as e:
        print(e)
    else:
        global_image = cv2_img
        # cb.loadImageRun(cv2_img)
        cv2.imshow("bounded", cv2_img)

        # cv2.waitKey(1)


def start():
    global subscriber
    subscriber = rospy.Subscriber("/checkerBoard", Image, image_callback)
    print("Started Captrue")


def captureOffset(color):
    start()
    stop()
    return gpc.getOffset(global_image, color)


# captureOffset(2)
