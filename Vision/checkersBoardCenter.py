import pyrealsense2 as rs
import numpy as np
import cv2
import cv2.aruco as aruco
import math
import rospy
from std_msgs.msg import String
import std_msgs.msg as msg
# import BoundingBoxes
from sensor_msgs.msg import Image as Image_type
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError

pub = rospy.Publisher('checkerBoardOffset', msg.String, queue_size=10)
image_pub = rospy.Publisher('checkerCroppedImage', Image_type, queue_size=1)
reg_image_pub = rospy.Publisher('checkerBoard', Image_type, queue_size=1)
bridge = CvBridge()

def points(image):
    height = image.shape[0]
    width = image.shape[1]
    size = height * 0.5
    start_point = (int(width/2 - size), int(height/2 - size))
    end_point = (int(width/2 + size), int(height/2 + size))
    return (start_point, end_point)


def locate(image, scale):
            # Save your OpenCV2 image as a jpeg 
        
        # cv2.imwrite('/home/tobenna/robot_capstone/Vision/GazeboImages/Image.jpeg', cv2_img)
        if len(image) == 0:
            return
        newImage = image.copy()
        unedited_image = image.copy()
        global offset
        (z, w) = points(image)
        color = (0, 255, 0)
        thickness = 2
        # adding box around cube
        # print(z , w , color, thickness)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
       
        arucoParameters = aruco.DetectorParameters_create()
        aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_100)
        corners, ids, rejectedImgPoints = aruco.detectMarkers( gray, aruco_dict, parameters=arucoParameters)
        # cv2.aruco.drawMarker(arucoDict, args["id"], 300, tag, 1)
        
        if len(corners) >= 2:
            # image = aruco.drawDetectedMarkers(image.copy(), corners, ids)

            mid_id_1 = (0 , 0)
            mid_id_2 = (0 , 0)
            for i in range(len(ids)): 

                # print(i , corners[0])

                x_sum = corners[i][0][0][0]+ corners[i][0][1][0]+ corners[i][0][2][0]+ corners[i][0][3][0]
                y_sum = corners[i][0][0][1]+ corners[i][0][1][1]+ corners[i][0][2][1]+ corners[i][0][3][1]
        
                x_centerPixel = int( x_sum*.25)
                y_centerPixel = int(y_sum*.25)

                # print(x_centerPixel,y_centerPixel )
                image = cv2.circle(image, (x_centerPixel,y_centerPixel), radius=1, color=(0, 225, 0), thickness=2)

                # print(ids[i][0])
                identi_ID =  ids[i][0]

                if identi_ID == 3 :
                    mid_id_1 = (x_centerPixel,y_centerPixel)
                elif identi_ID == 4: 
                    mid_id_2 = (x_centerPixel,y_centerPixel)
        

            print(mid_id_1, mid_id_2 )
            cv2.line(image, mid_id_1, mid_id_2, (0,0,225), 2)
            line_mid = (int((mid_id_2[0]+mid_id_1[0])/2),int((mid_id_2[1]+mid_id_1[1])/2))
            # print(line_mid)
            cv2.circle(image, line_mid, radius=1, color=(225, 0, 0), thickness=4)
            # print(mid_id_1, mid_id_2)

            image_mid_x = int(image.shape[1] / 2)
            image_mid_y = int(image.shape[0] / 2)
            image_mid = (image_mid_x, image_mid_y)

            length_line = math.sqrt(int((mid_id_2[0]-mid_id_1[0])**2) + int((mid_id_2[1]-mid_id_1[1])**2))
            # print(length_line)
            # factor = 0.86
            factor = 0.865
            checker_height = length_line * factor
            checker_width = length_line * factor

            start_point = (int( line_mid[0] - checker_width/2), int(line_mid[1] - checker_height/2 ))
            end_point = (int(checker_width/2 + line_mid[0]), int(checker_height/2 + line_mid[1]))


            newImage = unedited_image.copy()[start_point[1]:end_point[1],start_point[0]:end_point[0]]
            cv2.rectangle(image, start_point, end_point, color, thickness)

            

            offset = ((- line_mid[1] + image_mid[1]) * scale  ,  (line_mid[0] - image_mid[0] ) * scale  )
            print(offset)
            # print("publishing")
            # .pub.publish(offset)

            if len(newImage) != 0:
                pub.publish(str(offset[0]) + " " + str(offset[1]))
                image_pub.publish(bridge.cv2_to_imgmsg(newImage.copy(), "bgr8"))
            else:
                newImage = unedited_image


            # BoundingBoxes.loadImageRun(newImage.copy())
        
        reg_image_pub.publish(bridge.cv2_to_imgmsg(unedited_image, "bgr8"))
        image_mid_x = int(image.shape[1] / 2)
        image_mid_y = int(image.shape[0] / 2)
        image_mid = (image_mid_x, image_mid_y)
        cv2.circle(image, image_mid, radius=5, color=(225, 0, 0), thickness=4)

        cv2.imshow('Whole Board', image)
        cv2.imshow('Cropped Out', newImage)
        cv2.waitKey(1)
