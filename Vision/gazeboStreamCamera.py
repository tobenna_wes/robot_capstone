
#!/usr/bin/env python3
# rospy for the subscriber
import rospy
# ROS Image message
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import numpy as np
import cv2
import cv2.aruco as aruco
import checkersBoardCenter

# Instantiate CvBridge
bridge = CvBridge()

image = None
subscriber = None
image_topic = "/camera/color/image_raw"
rospy.init_node('image_listener')


cap = cv2.VideoCapture(0)

offset = (0,0)

# Check if the webcam is opened correctly
if not cap.isOpened():
    print("Cannot open webcam")


def readWebam():
    while True:
        ret, frame = cap.read()
        frame = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
        cv2.imshow('Input', frame)

        c = cv2.waitKey(1)
        # if c == 27:
        #     break


def readandDetect():
    while True:
        ret, frame = cap.read()
        frame = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
        locate(frame, 1)


def image_callback(msg):
    global image
    print("Received an image!")
    try:
        # Convert your ROS Image message to OpenCV2
        cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
        cv2_img = cv2.rotate(cv2_img, cv2.ROTATE_180)
    except CvBridgeError as e:
        print(e)
    else:
        checkersBoardCenter.locate(cv2_img, 2.55)

def getOffset():
    start()
    stop()
    return offset

def main():
    global subscriber
    
    # Define your image topic
    #image_topic = "/camera/color/image_raw"
     #image_topic = "/camera/color/image_raw"
    # Set up your subscriber and define its callback
    subscriber = rospy.Subscriber(image_topic, Image, image_callback)
    # Spin until ctrl + c
    # rospy.spin()
    # rospy.sleep(2)

# if __name__ == '__main__':
#     main()



def stop():
    
    rospy.sleep(2)
    cv2.destroyAllWindows()
    subscriber.unregister()

def start():
    global subscriber
    subscriber = rospy.Subscriber(image_topic, Image, image_callback)
    rospy.spin()
    print("Started Captrue")


def capture():
    start()
    stop()
    return image

# print(getOffset())
start()
# readandDetect()





