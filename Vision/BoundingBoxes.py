#!/usr/bin/env python3
from IPython.display import Image, display
from PIL import Image
import cv2
import cv2 as other_cv2
import numpy as np
from pylab import *
import skimage
from skimage import io
from skimage.viewer import ImageViewer
import sys
import pandas as pd
from matplotlib import pyplot as plt
import glob
import json
import imutils
import detectBoard
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
# import cropImage
# import gazeboCameraCapture
import rospy
from std_msgs.msg import String
# rospy.init_node('boundingBoxes')

lower_blue = np.array([110, 50, 50])
upper_blue = np.array([130, 255, 255])

lower_red = np.array([50, 50, 110])
upper_red = np.array([255, 255, 130])


class checkerBounders:

    
    def __init__(self):
        self.image = None

        self.height = 0
        self.width = 0
        self.initMidPoint = 0
        self.intZoneSize = 0

        # initialising capture matrix
        self.rows, self.cols = (8, 8)
        self.arr_matrix = [[0 for i in range(self.cols)] for j in range(self.rows)]
        self.location_matrix = [[(0.0, 0.0) for i in range(self.cols)] for j in range(self.rows)]
        # for row in arr_matrix:
        # print(row)


        # initialise pixel array
        self.arr_pixel = np.array([0, 0, 0])
        self.x, self.y = (0, 0)
        self.count = 0
        self.vals = []



    def loadImageData(self , new_image):
        self.image = cv2.rotate(new_image, cv2.ROTATE_180)
        # image = cropImage.cropImage(image)
        #image = detectBoard.getImage(image)
        alpha = 3  # Contrast control (1.0-3.0)
        beta = 30  # Brightness control (0-100)
        # image = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)
        # cv2.imshow('img', image)
        # cv2.waitKey()
        self.height = self.image.shape[0]
        self.width = self.image.shape[1]
        print("Board Pixel Size:height:",self.height , " ", self.width )
        ZoneSize = self.height/8
        self.intZoneSize = int(floor(ZoneSize))
        self.initMidPoint = ZoneSize/2
        # print(ZoneSize)
        self.vals = []
        for x in range(8):
            temp = floor(self.initMidPoint + ZoneSize * x)
            self.vals.append(temp)
        


    def points(self, x, y):
        temp_mid = int(self.initMidPoint)
        start_point = (y-temp_mid, x-temp_mid)
        end_point = (y+temp_mid, x+temp_mid)

        return (start_point, end_point)


    def boundBox(self, x, y):

        (z, w) = self.points(x, y)
        color = (0, 255, 0)
        thickness = 2
        # adding box around cube
        self.image = cv2.rectangle(image, z, w, color, thickness)


    def boundShape(self, x, y, is_black):

        temp_mid = int(self.initMidPoint * 0.95)
        addBackX = self.initMidPoint - temp_mid
        addBackY =  self.initMidPoint - temp_mid
        image_temp = self.image[x-temp_mid:x+temp_mid, y-temp_mid:y+temp_mid]
        # image = image[0:64][192:256][:]
        # cv2_imshow(image_temp)
        resized = imutils.resize(image_temp, width=300)
        ratio = image_temp.shape[0] / float(resized.shape[0])
        # convert the resized image to grayscale, blur it slightly,
        # and threshold it

        # cv2.imshow("image", image_temp)
        # cv2.waitKey(0)


        gray = cv2.cvtColor(resized, cv2.COLOR_RGBA2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)

        blurred = cv2.blur(gray, (5, 5))


        thresh = None
        if is_black:
            thresh = cv2.threshold(blurred, 10, 255, cv2.THRESH_BINARY)[1]
            # thresh = cv2.bitwise_not(thresh)
            # thresh = cv2.bitwise_not(thresh)
            print("IsBlack")
            #cv2.imshow("image", thresh)
            #cv2.waitKey(300)
        else:
            thresh = cv2.threshold(blurred, 120, 255, cv2.THRESH_BINARY)[1]
            thresh = cv2.bitwise_not(thresh)

        # cv2.imshow("image", thresh)
        # cv2.waitKey()

        cnts = cv2.findContours(
            thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        # sd = ShapeDetector()
        # cv2_imshow(image)
        # loop over the contours
        for c in cnts:
            # compute the center of the contour, then detect the name of the
            # shape using only the contour
            M = cv2.moments(c)
            if M["m00"] == 0 or M["m10"] == 0 or M["m01"] == 0:
                # print("could not wrap")
                continue
            cX = int((M["m10"] / M["m00"]) * ratio)
            cY = int((M["m01"] / M["m00"]) * ratio)
            if cX == 0 or cY == 0:
                # print("could not wrap")
                continue
            # print(cX, cY)
                    # initialize the shape name and approximate the contour
            shape = "unidentified"
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.01 * peri, True)

            # if the shape has 4 vertices, it is either a square or
            # a rectangle
            # print(len(approx))

            c = c.astype("float")
            c *= ratio
            c = c.astype("int")

            detected_shape = False
            if len(approx) <= 8:
                # compute the bounding box of the contour and use the
                # bounding box to compute the aspect ratio
                (a, b, w, h) = cv2.boundingRect(approx)
                ar = w / float(h)
                if ar >= 0.70 and ar <= 1.3:
                    # if ar == 1:
                    # print(ar)

                    shape = "square"
                    # print(shape)
                    detected_shape = True
                    old_type = self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize]
                    if old_type == 4: 
                        self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = 2
                    elif old_type == 3:
                        self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = 1
                else:
                    #cv2.imshow("rectangle", thresh)
                    #cv2.waitKey(100)
                    shape = "rectangle"

            # otherwise, we assume the shape is a circle
            else:
                shape = "circle"
                detected_shape = True
                print(len(approx))
                old_type = self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize]
                print(old_type)
                print(x//self.intZoneSize,y//self.intZoneSize)
                if old_type == 2: 
                    self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = 4
                elif old_type == 1:
                    self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = 3
                # cv2.imshow("rectangle", thresh)
                # cv2.waitKey(0)
            # return the name of the shape

            #
            if detected_shape:
                #print(shape)
                #print(len(approx))
                cv2.drawContours(image_temp, [c], -1, (0, 255, 0), 2)
                # cv2.putText(image_temp, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                # print(shape)
                # print(cX, cY)
                # print(x//intZoneSize,y//intZoneSize)
                boxX =  x//self.intZoneSize
                boxY =  y//self.intZoneSize
                # print(intZoneSize, intZoneSize)
                centerX = int(boxX * self.intZoneSize + cY + addBackX)
                centerY = int(boxY * self.intZoneSize + cX + addBackX)
                self.location_matrix[x//self.intZoneSize][y//self.intZoneSize] = (centerX, centerY)
                # print(centerX, centerY)
                # cv2.putText(image, shape, (centerX, centerX), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.putText(self.image, shape, (centerY, centerX), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                self.image = cv2.circle(self.image, (centerY,centerX), radius=1, color=(0, 0, 0), thickness=2)
                # cv2.imshow("image", self.image)
                # cv2.waitKey()



    def loopImages(self, image):
        #print(vals)
        image = cv2.rotate(image, cv2.ROTATE_180)
        for x in range(self.height):

            for y in range(self.width):

                # checking first block
                if x in self.vals and y in self.vals:
                    is_black = False
                    if (x//self.intZoneSize) % 2 == (y//self.intZoneSize) % 2:
                        is_black = True

                    flag = False
                    temp_mid = int(self.initMidPoint  * 0.9)
                    for j in range(-temp_mid, temp_mid):
                        if flag:
                            break
                        for k in range(-temp_mid, temp_mid):
                            #print([x-j],[y-k])
                            if y-k >= image.shape[1]:
                                continue
                            arr_pixel = image[x-j][y-k]
                            if (arr_pixel[0] > 140) and (arr_pixel[1] < 100) and (arr_pixel[2] < 100):
                                piece_type = 1
                                self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = piece_type
                                flag = True
                                # print("B",arr_pixel )
                                # boundBox(x, y)
                                self.boundShape(x, y, is_black)
                                break
                            elif (arr_pixel[0] < 100) and (arr_pixel[1] < 100) and (arr_pixel[2] > 140):
                                piece_type = 2
                                self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = piece_type
                                flag = True
                                # print("R",arr_pixel )
                                # boundBox(x, y)
                                self.boundShape(x, y, is_black)
                                flag = True
                                break
                    if not flag:
                        # print("none: ", arr_pixel)
                        piece_type = 0
                        self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = piece_type  # empty

                    # self.arr_matrix[x//self.intZoneSize][y//self.intZoneSize] = piece_type

        # printMatrix()
        # cv2.imshow("bounded", image)
        # cv2.waitKey(100)
        # rospy.sleep(100)


    def printMatrix(self):
        for j in self.arr_matrix:
            print(j , "\n")

        for j in self.location_matrix:
            print(j , "\n")

    def getFixedMatrix(self):

        new_array = self.location_matrix
        for i in range(len(new_array)):
            for j in range(len(new_array[i])):
                if new_array[i][j] != (0,0):
                    x_offset = -self.width/2 
                    y_offset = -self.height/2 
                    new_array[i][j] = float(-1*(new_array[i][j][0] + y_offset) * 1.23), float((new_array[i][j][1] + x_offset) * 1.23)
        # for j in new_array:
        #     print(j , "\n")

        return new_array


    def getBoardMatrix(self):
        board_matrix = [[(0, 0) for i in range(self.cols)] for j in range(self.rows)]
        each_box_size = self.initMidPoint
        # each_box_size = self.height/8
        for i in range(len(board_matrix)):
            for j in range(len(board_matrix[i])):
                x_offset =  -self.width/2 + each_box_size + 2 * each_box_size * j
                y_offset = -self.height/2 + each_box_size + 2 * each_box_size * i
                board_matrix[i][j] = ( - y_offset, x_offset)

        #for j in board_matrix:
            #print(j , "\n")
        return board_matrix
    
    def loadImageRun(self,new_image):
        print("started BOunding")
        self.image = new_image
        self.loadImageData(new_image)
        self.loopImages(new_image)
        return self.arr_matrix, self.location_matrix

# def mainSequence(): 
#     global image
#     i = 1
#     while i > 0:
#         gazeboCameraCapture.start()
#         gazeboCameraCapture.stop()
#         image = gazeboCameraCapture.image
#         loadImageData(image)
#         loopImages(image)


# def runOneSequence(): 
#     global image

#     #gazeboCameraCapture.start()
#     #gazeboCameraCapture.stop()
#     #image = gazeboCameraCapture.image
#     image = cv2.imread(
#     '/home/tobenna/robot_capstone/Vision/GazeboCapture/Image.jpeg')
#     cb.loadImageData(image)
#     cb.loopImages(image)
#     return cb.arr_matrix, cb.location_matrix









count = 0
def image_callback(msg):
    global count, global_image
    count += 1
    # print("Received an image!" + str(count))

    try:
        # Convert your ROS Image message to OpenCV2
        cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
        # cv2.imshow("New image", cv2_img)
        # cv2.waitKey(1)
    except CvBridgeError as e:
        print(e)
    else:
        global_image = cv2_img
        # cb.loadImageRun(cv2_img)
        cv2.imshow("bounded", cv2_img)
        cv2.waitKey(1)
        
offset = (0.0,0.0)
def offset_callback(msg):
    global offset
    # print(msg.data)

    array = msg.data.split()
    
    offset = (float(array[0]), float(array[1]))


cb = checkerBounders()
bridge = CvBridge()
# subscriber = rospy.Subscriber("/checkerCroppedImage", Image, image_callback)
# subscriber_offset = rospy.Subscriber("/checkerBoardOffset", Image, offset_callback)
subscriber = None
subscriber_offset = None
global_image = None



# try:
#     while True:
#         print("test")

#         sec = input('Say when:')
#         if sec  == "a":
#             print("started")
#             count = 0
#             # cb.loadImageRun(global_image.copy())
#             print("done")
#             # rospy.sleep(1)
#             cv2.imshow("bounded", global_image)
#             # rospy.sleep(1)
#             cv2.waitKey(0)
#             cv2.destroyAllWindows()
#             # rospy.sleep(1)
#         # other_cv2.waitKey(1)
#         # rospy.sleep(0.5)
        
# except KeyboardInterrupt:
#     print('interrupted!')
#     cv2.destroyAllWindows()
#     subscriber.unregister()
#     rospy.signal_shutdown()
#     pass


# rospy.spin()

def stop():
    rospy.sleep(1)
    cv2.destroyAllWindows()
    subscriber.unregister()
    subscriber_offset.unregister()

def start():
    global subscriber, subscriber_offset
    subscriber = rospy.Subscriber("/checkerCroppedImage", Image, image_callback)
    subscriber_offset = rospy.Subscriber("/checkerBoardOffset", String, offset_callback)
    print("Started Captrue")


def captureOffset():
    start()
    stop()
    cb.loadImageRun(global_image.copy())
    return offset

def captureLocationMatrix():
    start()
    stop()
    cb.loadImageRun(global_image.copy())
    return cb.getFixedMatrix()

def captureBoardMatrix():
    start()
    stop()
    cb.loadImageRun(global_image.copy())
    return cb.getBoardMatrix()

def captureTypeMatrix():
    start()
    stop()
    cb.loadImageRun(global_image.copy())
    return cb.arr_matrix


def captureImage():
    start()
    stop()
    cb.loadImageRun(global_image.copy())
    print("Done")
    # cv2.imshow("bounded", global_image)
    # rospy.sleep(1)
    # cv2.waitKey(0)
    return cb.image
    




# runOneSequence()

