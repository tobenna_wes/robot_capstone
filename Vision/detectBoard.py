#!/usr/bin/env python3
from IPython.display import Image, display
from PIL import Image
import cv2
import numpy as np
from pylab import *
import skimage
from skimage import io
from skimage.viewer import ImageViewer
import sys
import pandas as pd
from matplotlib import pyplot as plt
import glob
import json
import imutils

global image
image = cv2.imread(
    "/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/Screenshot from 2021-03-09 18-21-17.png")
#image = cv2.imread( "/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/download.png")
#image = cv2.imread( "/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/Screenshot from 2021-03-09 18-21-17.png")
#image = cv2.imread( "/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/Screenshot from 2021-03-09 18-21-17.png")
image = cv2.imread(
    "/home/tobenna/robot_capstone/Vision_ws/ImagesFolder/Screenshot from 2021-03-10 14-45-41 (copy).png")


def getImage(image):
    alpha = 1.0  # Contrast control (1.0-3.0)
    beta = 0  # Brightness control (0-100)

    image = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # cv2.imshow('img', image)
    # cv2.waitKey(000)
    print("image recived")
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((7*7, 3), np.float32)
    objp[:, :2] = np.mgrid[0:7, 0:7].T.reshape(-1, 2)

    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imshow('capture', gray)
    cv2.waitKey(2000)
    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (5, 5), None)

    # If found, add object points, image points (after refining them)
    print(ret)
    print(len(corners))
    if ret == True:
        objpoints.append(objp)

        corners2 = cv2.cornerSubPix(
            gray, corners, (11, 11), (-1, -1), criteria)
        imgpoints.append(corners2)
        print(corners)

        # Draw and display the corners
        temp_image = cv2.drawChessboardCorners(image, (7, 7), corners2, ret)
        cv2.imshow('img', temp_image)
        cv2.waitKey(000)

        h,  w = image.shape[:2]
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
            objpoints, imgpoints, gray.shape[::-1], None, None)
        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(
            mtx, dist, (w, h), 1, (w, h))
        dst = cv2.undistort(image, mtx, dist, None, newcameramtx)

        # crop the image
        x, y, w, h = roi
        dst = dst[y:y+h, x:x+w]
        cv2.imshow('img', dst)
        cv2.waitKey(0)
        return dst
        # cv2.imwrite('calibresult.png',dst)
    else:
        return image


# getImage(image)
