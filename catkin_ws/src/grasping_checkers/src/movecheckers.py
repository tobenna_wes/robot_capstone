#! /usr/bin/env python3
import rospy
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.srv import GetModelState


rospy.init_node('get_model_state')



rospy.wait_for_service('/gazebo/get_model_state')
model_coordinates  = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
resp_coordinates = model_coordinates("can4_clone_1", "table")



#rospy.init_node('set_pose', "2")
rospy.wait_for_service('/gazebo/set_model_state')
state_msg = ModelState()
state_msg.model_name = 'can4_clone_1'
state_msg.pose = resp_coordinates.pose
state_msg.pose.position.x = resp_coordinates.pose.position.x + 0.55
#state_msg.pose.position.y = 0
#state_msg.pose.position.z = 0.3
#state_msg.pose.orientation.x = 0
#state_msg.pose.orientation.y = 0
#state_msg.pose.orientation.z = 0
#state_msg.pose.orientation.w = 0

checker_msg.pose.position.x = resp_coordinates.pose.position.x +  BOX_OFFSET
checker_msg.pose.position.y = resp_coordinates.pose.position.y - BOX_OFFSET



set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
resp = set_state( state_msg )


print(resp)