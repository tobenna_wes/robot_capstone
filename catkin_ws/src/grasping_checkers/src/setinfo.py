#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")
pose_target = geometry_msgs.msg.Pose()



arm_group.set_joint_value_target([1.4231111132768293, -1.4129680333375418, 1.9873616123662057, -2.166306917700462, -1.5200804137482917, 3.0656009911865034])
arm_group.set_rpy_target([-1.595731052036244, -0.04895309847519466, 3.071045190992502])
planb = arm_group.go()

rospy.sleep(3)
