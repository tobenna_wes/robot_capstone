#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
gripper_group.set_named_target("OpenGripper")
plana = gripper_group.go()
arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")
pose_target = geometry_msgs.msg.Pose()


def gohome():
    arm_group.set_named_target("Pick")
    plan1 = arm_group.go()
    rospy.sleep(3)


##MOVE TO CLOSE POSITION

