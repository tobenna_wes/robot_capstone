#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
#print(gripper_group.get_path_constraints())


##OPEN GRIPPER 
gripper_group.set_named_target("OpenGripper")
plana = gripper_group.go()
rospy.sleep(3)
#print(gripper_group.get_current_joint_values())


##MOVE TO SAFE POSITION

arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")
pose_target = geometry_msgs.msg.Pose()


i = 1.1432198044719495
print(arm_group.get_current_joint_values())
print(arm_group.get_current_rpy())
print(arm_group.get_current_pose())




Error = 0.009170

X_POS = -0.316397
Y_POS = 0.140835
Z_POZ_DROP = 1.055
BOX_OFFSET = 0.0
MOVEOFFEST = -0.095
Grip_length = 0.209



##MOVE TO CLOSE POSITION

for y in range(3):
    ##MOVE TO CLOSE POSITION
    for x in range(4):

        pose_target.orientation.x = -0.7142126013476185
        pose_target.orientation.y = -0.00023103050360981063
        pose_target.orientation.z = -0.000746655961981416
        pose_target.orientation.w = 0.6999283886270427
        pose_target.position.x = X_POS + BOX_OFFSET
        pose_target.position.y = Y_POS - BOX_OFFSET
        pose_target.position.z = 1.145
        arm_group.set_pose_target(pose_target)
        plan2 = arm_group.go()
        rospy.sleep(1)


        ##MOVE TO OPEN GRIPPPER
        gripper_group.set_joint_value_target([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        planb = gripper_group.go()
        rospy.sleep(1)


        ##MOVE TO CLOSE POSITION
        pose_target.position.x = X_POS + BOX_OFFSET
        pose_target.position.y = Y_POS - BOX_OFFSET
        pose_target.position.z = Z_POZ_DROP
        arm_group.set_pose_target(pose_target)
        plan3 = arm_group.go()
        print("HOLDING PART ")
        rospy.sleep(1)

        ##MOVE TO cLOSE GRIPPPER
        gripper_group.set_joint_value_target([Grip_length, Grip_length, Grip_length, Grip_length, Grip_length, Grip_length])
        planb = gripper_group.go()
        rospy.sleep(1)


        ##MOVE TO uP
        pose_target.position.x = X_POS + BOX_OFFSET
        pose_target.position.y = Y_POS - BOX_OFFSET
        pose_target.position.z = 1.145
        arm_group.set_pose_target(pose_target)
        plan3 = arm_group.go()
        print("HOLDING PART ")
        rospy.sleep(1)


        pose_target.position.x = X_POS + BOX_OFFSET 
        pose_target.position.y = Y_POS - BOX_OFFSET + MOVEOFFEST
        pose_target.position.z = 1.145
        arm_group.set_pose_target(pose_target)
        plan3 = arm_group.go()
        print("HOLDING PART ")
        rospy.sleep(1)


        pose_target.position.x = X_POS + BOX_OFFSET 
        pose_target.position.y = Y_POS - BOX_OFFSET + MOVEOFFEST
        pose_target.position.z = Z_POZ_DROP
        arm_group.set_pose_target(pose_target)
        plan3 = arm_group.go()
        print("HOLDING PART ")
        rospy.sleep(1)


        gripper_group.set_joint_value_target([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        planb = gripper_group.go()
        rospy.sleep(1)

        pose_target.position.x = X_POS + BOX_OFFSET 
        pose_target.position.y = Y_POS - BOX_OFFSET + MOVEOFFEST
        pose_target.position.z = 1.145
        arm_group.set_pose_target(pose_target)
        plan3 = arm_group.go()
        print("HOLDING PART ")
        rospy.sleep(1)


        X_POS = X_POS + 0.1877

    if  y == 0 :
        X_POS = -0.316397
    else:
        X_POS = -0.221129
    
    Y_POS = Y_POS + 0.1877/2



print("ENDED")
moveit_commander.roscpp_shutdown()

