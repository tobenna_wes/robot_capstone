#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
#print(gripper_group.get_path_constraints())


##OPEN GRIPPER 
gripper_group.set_named_target("OpenGripper")
plana = gripper_group.go()
rospy.sleep(3)

arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")
pose_target = geometry_msgs.msg.Pose()


i = 1.1432198044719495


Error = 0.009170
X_POS = -0.128697
Y_POS = 0.328535
Z_POZ_DROP = 1.055
BOX_OFFSET = 0.0
MOVEOFFEST = 0.095
Grip_length = 0.209

def closeGripper():
    gripper_group.set_joint_value_target([Grip_length, Grip_length, Grip_length, Grip_length, Grip_length, Grip_length])
    planb = gripper_group.go()
    rospy.sleep(2)
    return

def openGripper():
    gripper_group.set_joint_value_target([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    planb = gripper_group.go()
    rospy.sleep(2)
    return


def drop():
    pose_target.position.z = Z_POZ_DROP
    arm_group.set_pose_target(pose_target)
    plan3 = arm_group.go()
    rospy.sleep(2)
    return


def raisegripper():
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan3 = arm_group.go()
    rospy.sleep(2)
    return

def move(xoriginal, xoffet, yoriginal, yoffset):
    pose_target.position.y = 0.14082 - BOX_OFFSET - 2 * MOVEOFFEST
    pose_target.position.y = 0.14082 - BOX_OFFSET - 2 * MOVEOFFEST
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()
    rospy.sleep(2)
    return


def gohome():
    arm_group.set_named_target("Pick")
    plan1 = arm_group.go()
    rospy.sleep(3)
    return

gohome()

openGripper()

pose_target.orientation.x = -0.7142126013476185
pose_target.orientation.y = -0.00023103050360981063
pose_target.orientation.z = -0.000746655961981416
pose_target.orientation.w = 0.6999283886270427
pose_target.position.x = -0.128695 + BOX_OFFSET
pose_target.position.y = 0.14082 - BOX_OFFSET
pose_target.position.z = 1.145
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)

drop()

closeGripper()

raisegripper()



pose_target.position.x = -0.128695
pose_target.position.y = 0.14082 - 2 * MOVEOFFEST
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)

drop()
openGripper()
raisegripper()


pose_target.position.y = -0.139163
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)


pose_target.position.x = 0.154273
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)

drop()
closeGripper()
raisegripper()

pose_target.position.x = 0.154273
pose_target.position.y = -0.139163 + 2 * MOVEOFFEST
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)

drop()
openGripper()
raisegripper()


pose_target.position.x = -0.033429 
pose_target.position.y = -0.139163
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)

drop()
closeGripper()
raisegripper()


pose_target.position.x = -0.033429 - 2 * MOVEOFFEST
pose_target.position.y = -0.139163 + 2 * MOVEOFFEST
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)


drop()
openGripper()
raisegripper()



pose_target.position.x = -0.128695
pose_target.position.y = 0.14082 - 2 * MOVEOFFEST
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)


drop()
closeGripper()
raisegripper()


pose_target.position.x = -0.316397 - MOVEOFFEST
pose_target.position.y = -0.233013 
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)

drop()
openGripper()
raisegripper()



pose_target.position.x = 0.059003 
pose_target.position.y = 0.1408353 
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)


drop()
closeGripper()
raisegripper()


pose_target.position.x = 0.059003 + 2 * MOVEOFFEST
pose_target.position.y = 0.1408353 - 2 * MOVEOFFEST
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)



drop()
openGripper()
raisegripper()

pose_target.position.x = 0.156828
pose_target.position.y = 0.052029
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)


drop()
closeGripper()
raisegripper()


pose_target.position.x = 0.341971 + MOVEOFFEST
pose_target.position.y = -0.139163 
arm_group.set_pose_target(pose_target)
plan2 = arm_group.go()
rospy.sleep(2)

drop()
openGripper()
raisegripper()



gohome()














moveit_commander.roscpp_shutdown()









