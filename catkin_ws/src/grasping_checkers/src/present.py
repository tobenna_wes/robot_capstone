#! /usr/bin/env python3
import sys
import rospy
import moveit_commander
import geometry_msgs.msg 
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.srv import GetModelState


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()

arm_group = moveit_commander.MoveGroupCommander("robot_arm_moveit_group")
gripper_group = moveit_commander.MoveGroupCommander("robot_gripper_moveit_group")
gripper_group.set_named_target("OpenGripper")
pose_target = geometry_msgs.msg.Pose()
plana = gripper_group.go()
rospy.sleep(3)



rospy.wait_for_service('/gazebo/get_model_state')
model_coordinates  = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
rospy.wait_for_service('/gazebo/set_model_state')

set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)


i = 1.1432198044719495


Error = 0.009170
X_POS = -0.128697
Y_POS = 0.328535
Z_POZ_DROP = 1.055
BOX_OFFSET = 0.0
MOVEOFFEST = 0.095
Grip_length = 0.209

def closeGripper():
    gripper_group.set_joint_value_target([Grip_length, Grip_length, Grip_length, Grip_length, Grip_length, Grip_length])
    planb = gripper_group.go()
    rospy.sleep(1)
    return

def openGripper():
    gripper_group.set_joint_value_target([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    planb = gripper_group.go()
    rospy.sleep(1)
    return


def drop():
    pose_target.position.z = Z_POZ_DROP
    arm_group.set_pose_target(pose_target)
    plan3 = arm_group.go()
    rospy.sleep(1)
    return


def raisegripper():
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan3 = arm_group.go()
    rospy.sleep(1)
    return

def move(xoriginal, xoffet, yoriginal, yoffset):
    pose_target.position.y = 0.14082 - BOX_OFFSET - 2 * MOVEOFFEST
    pose_target.position.y = 0.14082 - BOX_OFFSET - 2 * MOVEOFFEST
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()
    rospy.sleep(2)
    return


def gohome():
    arm_group.set_named_target("Pick")
    plan1 = arm_group.go()
    rospy.sleep(2)
    return



def robotmoveblueleft(name):
    gohome()
    openGripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.orientation.x = -0.7142126013476185
    pose_target.orientation.y = -0.00023103050360981063
    pose_target.orientation.z = -0.000746655961981416
    pose_target.orientation.w = 0.6999283886270427
    pose_target.position.x = resp_coordinates.pose.position.x
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()


    drop()
    closeGripper()
    raisegripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.orientation.x = -0.7142126013476185
    pose_target.orientation.y = -0.00023103050360981063
    pose_target.orientation.z = -0.000746655961981416
    pose_target.orientation.w = 0.6999283886270427
    pose_target.position.x = resp_coordinates.pose.position.x - MOVEOFFEST
    pose_target.position.y = resp_coordinates.pose.position.y +  MOVEOFFEST
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    drop()
    openGripper()
    raisegripper()
    gohome()


def robotmoveblueright(name):
    gohome()
    openGripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.orientation.x = -0.7142126013476185
    pose_target.orientation.y = -0.00023103050360981063
    pose_target.orientation.z = -0.000746655961981416
    pose_target.orientation.w = 0.6999283886270427
    pose_target.position.x = resp_coordinates.pose.position.x
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()
    

    drop()
    closeGripper()
    raisegripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.orientation.x = -0.7142126013476185
    pose_target.orientation.y = -0.00023103050360981063
    pose_target.orientation.z = -0.000746655961981416
    pose_target.orientation.w = 0.6999283886270427
    pose_target.position.x = resp_coordinates.pose.position.x + MOVEOFFEST
    pose_target.position.y = resp_coordinates.pose.position.y +  MOVEOFFEST
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()


    drop()
    openGripper()
    raisegripper()
    gohome()

def krobotmoveblueleft(name ,  kname):
    gohome()
    openGripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.orientation.x = -0.7142126013476185
    pose_target.orientation.y = -0.00023103050360981063
    pose_target.orientation.z = -0.000746655961981416
    pose_target.orientation.w = 0.6999283886270427
    pose_target.position.x = resp_coordinates.pose.position.x
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()


    drop()
    closeGripper()
    raisegripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.position.x = resp_coordinates.pose.position.x - 2 * MOVEOFFEST
    pose_target.position.y = resp_coordinates.pose.position.y + 2 * MOVEOFFEST
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    drop()
    openGripper()
    raisegripper()



    resp_coordinates = model_coordinates(kname, "table")
    pose_target.position.x = resp_coordinates.pose.position.x
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    drop()
    closeGripper()
    raisegripper()

    resp_coordinates = model_coordinates(kname, "table")
    pose_target.position.x = -0.506805
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145 
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    openGripper()
    gohome()

def krobotmoveblueright(name ,  kname):
    gohome()
    openGripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.orientation.x = -0.7142126013476185
    pose_target.orientation.y = -0.00023103050360981063
    pose_target.orientation.z = -0.000746655961981416
    pose_target.orientation.w = 0.6999283886270427
    pose_target.position.x = resp_coordinates.pose.position.x
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()


    drop()
    closeGripper()
    raisegripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.position.x = resp_coordinates.pose.position.x + 2 * MOVEOFFEST
    pose_target.position.y = resp_coordinates.pose.position.y + 2 * MOVEOFFEST
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    drop()
    openGripper()
    raisegripper()



    resp_coordinates = model_coordinates(kname, "table")
    pose_target.position.x = resp_coordinates.pose.position.x
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    drop()
    closeGripper()
    raisegripper()

    resp_coordinates = model_coordinates(kname, "table")
    pose_target.position.x = -0.506805
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145 
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    openGripper()
    gohome()


def moveredleft(name):

    resp_coordinates = model_coordinates(name, "table")
    checker_msg = ModelState()
    checker_msg.model_name = name
    checker_msg.pose = resp_coordinates.pose
    checker_msg.pose.position.x = resp_coordinates.pose.position.x +  MOVEOFFEST
    checker_msg.pose.position.y = resp_coordinates.pose.position.y - MOVEOFFEST
    resp = set_state( checker_msg )



def kmoveredleft(name, kname):


    resp_coordinates = model_coordinates(kname, "table")
    checker_msg = ModelState()
    checker_msg.model_name = kname
    checker_msg.pose = resp_coordinates.pose
    checker_msg.pose.position.z = 0
    resp = set_state( checker_msg )
    rospy.sleep(1)
    resp_coordinates = model_coordinates(name, "table")
    checker_msg = ModelState()
    checker_msg.model_name = name
    checker_msg.pose = resp_coordinates.pose
    checker_msg.pose.position.x = resp_coordinates.pose.position.x + 2 * MOVEOFFEST
    checker_msg.pose.position.y = resp_coordinates.pose.position.y - 2 *  MOVEOFFEST
    resp = set_state( checker_msg )




def moveredright(name):

    resp_coordinates = model_coordinates(name, "table")
    checker_msg = ModelState()
    checker_msg.model_name = name
    checker_msg.pose = resp_coordinates.pose
    checker_msg.pose.position.x = resp_coordinates.pose.position.x -  MOVEOFFEST
    checker_msg.pose.position.y = resp_coordinates.pose.position.y - MOVEOFFEST
    resp = set_state( checker_msg )



def kmoveredright(name, kname):

    resp_coordinates = model_coordinates(kname, "table")
    checker_msg = ModelState()
    checker_msg.model_name = kname
    checker_msg.pose = resp_coordinates.pose
    checker_msg.pose.position.z = 0
    resp = set_state( checker_msg )
    rospy.sleep(1)
    resp_coordinates = model_coordinates(name, "table")
    checker_msg = ModelState()
    checker_msg.model_name = name
    checker_msg.pose = resp_coordinates.pose
    checker_msg.pose.position.x = resp_coordinates.pose.position.x - 2 * MOVEOFFEST
    checker_msg.pose.position.y = resp_coordinates.pose.position.y - 2 *  MOVEOFFEST
    resp = set_state( checker_msg )


def movefwd(name):
    gohome()
    openGripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.orientation.x = -0.7142126013476185
    pose_target.orientation.y = -0.00023103050360981063
    pose_target.orientation.z = -0.000746655961981416
    pose_target.orientation.w = 0.6999283886270427
    pose_target.position.x = resp_coordinates.pose.position.x
    pose_target.position.y = resp_coordinates.pose.position.y
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()


    drop()
    closeGripper()
    raisegripper()

    resp_coordinates = model_coordinates(name, "table")
    pose_target.position.y = resp_coordinates.pose.position.y + MOVEOFFEST
    pose_target.position.z = 1.145
    arm_group.set_pose_target(pose_target)
    plan2 = arm_group.go()

    drop()
    openGripper()
    raisegripper()
    gohome()




moveredleft("can4_clone_1")

robotmoveblueleft("can1_clone_6")

moveredright("can4_clone_1")

robotmoveblueright("can1_clone_6")
kmoveredleft("can4_clone_2", 'can1_clone_6')

krobotmoveblueleft("can1_clone_7" , "can4_clone_2")

moveredleft("can3_clone_1")

krobotmoveblueleft("can1_clone_7" , "can3_clone_1")

kmoveredleft("can4_clone_5", 'can1_clone_7')

krobotmoveblueleft("can1_clone_5" , "can4_clone_1")


kmoveredleft("can4_clone_0", "can1_clone_5" )

krobotmoveblueright("can1_clone_4" , "can4_clone_0")

kmoveredright("can4_clone_5", "can1_clone_4" )



#movefwd("can1_clone_5")
#movefwd("can4_clone_5")
#movefwd("can4_clone_7")

moveit_commander.roscpp_shutdown()









