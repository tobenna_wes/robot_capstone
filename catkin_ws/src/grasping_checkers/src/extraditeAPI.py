#! /usr/bin/env python3

import extradite
import requests
from std_srvs.srv import Empty


extradite.rospy.wait_for_service('/gazebo/reset_world')
reset_world = extradite.rospy.ServiceProxy('/gazebo/reset_world', Empty)

reset_world()


url = 'https://example.com'
headers = {'Authorization': 'Bearer example-auth-code'}
payload = {'name':'Mark', 'email': 'mark@bearer.sh'}

parameters = {
    "lat": 40.71,
    "lon": -74
}

#response = requests.get('https://example.com')
#response = requests.get('https://pokeapi.co/api/v2/pokemon/squirtle/')
#response = requests.get("http://api.open-notify.org/astros.json")
response = requests.get("http://api.open-notify.org/iss-pass.json", params=parameters)

#print(response.json())

data =  response.json()

if data.get("messages"): 
    print(data["messages"])
else: 
    print(data["message"])

if data.get("request"): 
    altitude =  data["request"]['altitude']
    print(altitude/2)

#refering to boxes only
#From the boxes I would then 
#cross reference the x-y axis location using Vision Data
#the value of the "remove" is optional but the "remove" field must be there

#that was this simple dataset can handle even a 
#king moving backward even if it has to kill



boxNameArray = [

"1_clone_0", "1_clone_1", "1_clone_2", "1_clone_3", 
"2_clone_0", "2_clone_1", "2_clone_2", "2_clone_3", 
"3_clone_0", "3_clone_1", "3_clone_2", "3_clone_3", 
"4_clone_0", "4_clone_1", "4_clone_2", "4_clone_3", 
"5_clone_0", "5_clone_1", "5_clone_2", "5_clone_3", 
"6_clone_0", "6_clone_1", "6_clone_2", "6_clone_3", 
"7_clone_0", "7_clone_1", "7_clone_2", "7_clone_3", 
"8_clone_0", "8_clone_1", "8_clone_2", "8_clone_3"

]



checkersNameArray = [

"11_clone_0", "11_clone_1", "11_clone_2", "11_clone_3", 
"12_clone_0", "12_clone_1", "12_clone_2", "12_clone_3", 
"13_clone_0", "13_clone_1", "13_clone_2", "13_clone_3", 
"", "", "", "", 
"", "", "", "",
"16_clone_0", "16_clone_1", "16_clone_2", "16_clone_3", 
"17_clone_0", "17_clone_1", "17_clone_2", "17_clone_3", 
"18_clone_0", "18_clone_1", "18_clone_2", "18_clone_3"

]


checkersColorArray = [

"red", "red", "red", "red", 
"red", "red", "red", "red", 
"red", "red", "red", "red", 
"", "", "", "", 
"", "", "", "",
"black", "black", "black", "black", 
"black", "black", "black", "black", 
"black", "black", "black", "black", 

]

checkersTypeArray = [

"pawn", "pawn", "pawn", "pawn", 
"pawn", "pawn", "pawn", "pawn", 
"pawn", "pawn", "pawn", "pawn", 
"", "", "", "", 
"", "", "", "",
"pawn", "pawn", "pawn", "pawn", 
"pawn", "pawn", "pawn", "pawn", 
"pawn", "pawn", "pawn", "pawn", 

]












def pickPiece(name):
    print("should go home")
    extradite.gohome()
    extradite.openGripper()

    resp_coordinates = extradite.model_coordinates(name, "boardmaterial")
    extradite.pose_target.orientation.x = extradite.orientation_x
    extradite.pose_target.orientation.y = extradite.orientation_y
    extradite.pose_target.orientation.z = extradite.orientation_z
    extradite.pose_target.orientation.w = extradite.orientation_w
    extradite.pose_target.position.x = resp_coordinates.pose.position.x
    extradite.pose_target.position.y = resp_coordinates.pose.position.y
    extradite.pose_target.position.z = 1.145
    extradite.arm_group.set_pose_target(extradite.pose_target)
    extradite.plan2 = extradite.arm_group.go()


    extradite.drop()
    extradite.closeGripper()
    extradite.raisegripper()



def movePieceTo(name):
    resp_coordinates = extradite.model_coordinates(name, "boardmaterial")
    extradite.pose_target.orientation.x = -0.7142126013476185
    extradite.pose_target.orientation.y = -0.00023103050360981063
    extradite.pose_target.orientation.z = -0.000746655961981416
    extradite.pose_target.orientation.w = 0.6999283886270427
    extradite.pose_target.position.x = resp_coordinates.pose.position.x
    extradite.pose_target.position.y = resp_coordinates.pose.position.y
    extradite.pose_target.position.z = 1.145
    extradite.arm_group.set_pose_target(extradite.pose_target)
    plan2 = extradite.arm_group.go()












def robotPlayAction(fromPiece, toBox):
    print("Startd")
    pickPiece(fromPiece)
    print("shouldMOve")
    movePieceTo(toBox)

    print("shouldDrop")
    extradite.drop()
    extradite.openGripper()
    extradite.raisegripper()
    extradite.gohome()


def robotRemoveAction(fromPiece):

    pickPiece(fromPiece)
    extradite.movetoTrash(fromPiece)
    extradite.openGripper()
    extradite.gohome()








def moveRobot(robotInstruction):

    fromBox = robotInstruction["from"]
    toBox = robotInstruction["to"]
    removeBox = robotInstruction["remove"]


    input_fromBox = input ("move Robot to pick at Box: ")
    fromBox = int(input_fromBox)

    input_toBox = input ("move Robot to place at Box: ")
    toBox = int(input_toBox)

    input_removeBox = input ("move Robot to remove at Box: ")
    removeBox = int(input_removeBox)


    fromPeiceName =  checkersNameArray[fromBox]
    toPeiceName =  checkersNameArray[toBox]
    
    toBoxName = boxNameArray[toBox]

    #--- Add Replace Function. 

    if removeBox != 32:
        removePeiceName =  checkersNameArray[removeBox]
        robotRemoveAction(removePeiceName)
        checkersNameArray[removeBox] = ""
#--Check if poster pirce is present

    if fromPeiceName != "":
        print("Part Present")
        if toPeiceName == "" :
            print("safe to pick")
            print(fromPeiceName)
            print(toBoxName)
            robotPlayAction(fromPeiceName, toBoxName)
            checkersNameArray[toBox] = checkersNameArray[fromBox]
            checkersNameArray[fromBox] = ""
        else:
            print("Box is obstructed")

    else:
        print("Part not Present")








def userPlayAction(fromPiece, toBox):
    resp_coordinates = extradite.model_coordinates(toBox, "boardmaterial")
    extradite.checker_msg = extradite.ModelState()
    extradite.checker_msg.model_name = fromPiece
    extradite.checker_msg.pose = resp_coordinates.pose
    extradite.checker_msg.pose.position.x = resp_coordinates.pose.position.x 
    extradite.checker_msg.pose.position.y = resp_coordinates.pose.position.y 
    extradite.checker_msg.pose.position.z = 1.145 
    resp = extradite.set_state( extradite.checker_msg )
    #extradite.rospy.sleep(1)






def userRemoveAction(fromPiece):
    resp_coordinates = extradite.model_coordinates(fromPiece, "boardmaterial")
    extradite.checker_msg = extradite.ModelState()
    extradite.checker_msg.model_name = fromPiece
    extradite.checker_msg.pose = resp_coordinates.pose
    extradite.checker_msg.pose.position.z = 0
    resp = extradite.set_state( extradite.checker_msg )
    #extradite.rospy.sleep(1)







def moveUser():


    input_fromBox = input ("Box to move: ")
    fromBox = int(input_fromBox)

    input_toBox = input ("Box to place at: ")
    toBox = int(input_toBox)

    input_removeBox = input ("Remove Box to place at: ")
    removeBox = int(input_removeBox)


    fromPeiceName =  checkersNameArray[fromBox]
    toPeiceName =  checkersNameArray[toBox]
    toBoxName = boxNameArray[toBox]

    #--- Add Replace Function. 

    if removeBox != 32:
        print("removePiece")
        removePeiceName =  checkersNameArray[removeBox]
        userRemoveAction(removePeiceName)
        checkersNameArray[removeBox] = ""
    #--Check if poster pirce is present

    if fromPeiceName != "":
        print("Part Present")
        if toPeiceName == "" :
            print("safe to pick")
            print(fromPeiceName)
            print(toBoxName)
            userPlayAction(fromPeiceName, toBoxName)
            checkersNameArray[toBox] = checkersNameArray[fromBox]
            checkersNameArray[fromBox] = ""
        else:
            print("Box is obstructed")

    else:
        print("Part not Present")























robotInstruction = {
    "from": 8,
    "to": 13,
    "remove" : 32,
    "replace" : 32,
}


#moveRobot(robotInstruction)



robotInstruction = {
    "from": 22,
    "to": 17,
    "remove" : 32,
    "replace" : 32,
}





n = 5
while n > 0:
    moveRobot(robotInstruction)
    moveUser()



extradite.moveit_commander.roscpp_shutdown()
















































