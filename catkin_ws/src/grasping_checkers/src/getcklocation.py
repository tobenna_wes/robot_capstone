#! /usr/bin/env python3
import rospy
import std_srvs.srv 
from gazebo_msgs.srv import GetModelState

rospy.init_node('get_model_state')

rospy.wait_for_service('/gazebo/get_model_state')
model_coordinates  = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
resp_coordinates = model_coordinates("robot_pilar", "table")


print(resp_coordinates)
