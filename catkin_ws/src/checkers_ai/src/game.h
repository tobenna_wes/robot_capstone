// Headder File for Board State Class
// Referee Functions Will be Included in this Class

#ifndef GAME_H
#define GAME_H

//#include "defines.h"
//#include "boardState.h"
#include "mcts.h"


class Game {
private:
public:
	BoardState currBoard;
	// Input shape is either {3, 8, 8} or {1, 8, 8} with a conversion to one array
	int inputShape[3] = { 3, 8, 8 };
	int totalMoveCount;
	Game() {
		currBoard = BoardState();
		totalMoveCount = 0;
	};
	void updateBoard(BoardState newBoard) {
		currBoard = newBoard;
		return;
	}
	bool tester() {
		return true;
	}
	// bool test(checkers_tester::nextMoveTester::Request  &req,
    //      checkers_tester::nextMoveTester::Response &res) {
	// 	res.startPt = 0;
	// 	res.endpts = {0};
	// 	res.captured = {0};

	// 	ROS_INFO("request: currPlayer=%ld, oppntPlayer=%ld, bothIsKing=%ld, moveDir=%ld", (long int)req.currPlayer, (long int)req.oppntPlayer, (long int)req.bothIsKing, (long int)req.moveDir);
	// 	ROS_INFO("sending back response: [%ld]", (long int)res.startPt);
	// 	return true;
	// }
	void getMoves(ArryMove& moves) {
		currBoard.legalMoves(moves);
		return;
	}
	bool movePiece(const Move& move, bool& makeKing) {
		//doesn't check if move is legal or is possible
		//only checks for totalMoves count
		bool moveMade = False;
		if (totalMoveCount < MOVELIMIT) {
			currBoard.makeMove(move, makeKing);
			totalMoveCount++;
			moveMade = True;
		}

		return moveMade;
	}
	bool isFinished(int& winner){
		//winner is 1 for black, -1 for red, and 0 for draw
		bool finished = False;
		//count both colored pieces
		int numBlack = 0;
		int numRed = 0;
		currBoard.countPieces(numBlack, numRed);
		if (numRed == 0) {
			//black won
			winner = 1;
			finished = True;
		}
		else if (numBlack == 0) {
			//red won
			winner = -1;
			finished = True;
		}
		else if (totalMoveCount >= MOVELIMIT) {
			//draw
			winner = 0;
			finished = True;
		}
		//else finished is false (nothing to do)

		return finished;
	};
};


#endif