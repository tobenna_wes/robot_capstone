#ifndef AGENT_H
#define AGENT_H

#include "game.h"


class User {
private:
public:
	int moveDir;
	User(int moveDirIn) {
		moveDir = moveDirIn;
	}
	Move takeTurn(Game& game) {
		ArryMove possibleMoves;
		game.getMoves(possibleMoves);
		std::cout << "Possible Moves for ";
		if (moveDir == 1) {
			std::cout << "Black:" << std::endl;
		}
		else {
			std::cout << "Red:" << std::endl;
		}
		for (int i = 0; i < possibleMoves.getSize(); i++) {
			//printf("{%i}  ", i);
			printf("{%c}  ", 'a' + i);
			possibleMoves[i].print();
		}
		std::cout << "Select Move Letter:" << std::endl;
		char moveChar = '0';
		std::cin >> moveChar;
		moveChar = tolower(moveChar);
		int moveNum = moveChar - 'a';
		if ((moveNum < 0) || (moveNum >= possibleMoves.getSize())) {
			//invalid move selected
			moveNum = 0;
		}
		std::cout << "Making Move " << moveChar << "..." << std::endl;

		bool makeKing = false;
		game.movePiece(possibleMoves[moveNum], makeKing);
		return possibleMoves[moveNum];
	};
};


class Agent {
private:
public:
	int moveDir;
	MCTS mcts;
	Agent() {
		moveDir = -1;
	}
	void resetMCTS(BoardState rootState) {
		MCTS tmp(rootState);
		mcts = tmp;
		return;
	}
	Agent(int moveDirIn, BoardState rootState) {
		moveDir = moveDirIn;
		mcts = MCTS(rootState);
	};
	Move takeTurn(Game& game, bool& makeKing) {
		Move move = mcts.mcts();
		move.print();
		game.movePiece(move, makeKing);
		return move;
	};
};







#endif