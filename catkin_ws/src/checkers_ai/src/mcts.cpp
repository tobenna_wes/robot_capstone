//CPP File for MCTS Class

#include "mcts.h"


//*can cut top part of tree off (before current root node) and keep track of offset when 
//*resizing to reduce memory usage (if not keeping game tree between full games and do not 
//*need full game tree for preventing either repeating moves or repeating states)

//***FIXME: MUST PREVENT EITHER REPEATING MOVES OR REPEATING STATES

//*Idea: hash table would be too big. Can keep a list of just the ids of prev states with the 
//current number of pieces to make searching for repeating states slightly faster. That way you
//don't have to search through the entire tree array from beginning to end.



Node::Node() {
	numWins = 0;
	numSims = 0;
	depth = 0;
	fullyExpanded = False;
}
Node::Node(BoardState boardStateIn) {
	numWins = 0;
	numSims = 0;
	boardState = boardStateIn;
	depth = 0;
	fullyExpanded = False;
}
//all other nodes
Node::Node(BoardState boardStateIn, Move moveIn, int depthIn) {
	numWins = 0;
	numSims = 0;
	move = moveIn;
	boardState = boardStateIn;
	depth = depthIn;
	fullyExpanded = False;
}
//no destructor, copy constructor, =, == or != operator needed
//probably not used
/*
bool isLeaf() {
	if (childrenArry.getSize() > 0) {
		return False;
	}
	return True;
};*/

/*
bool isFinished(int& winner){
	//winner is 1 for black, -1 for red, and 0 for draw
	bool finished = False;
	//count both colored pieces
	int numBlack = 0;
	int numRed = 0;
	currBoard.countPieces(numBlack, numRed);
	if (numRed == 0) {
		//black won
		winner = 1;
		finished = True;
	}
	else if (numBlack == 0) {
		//red won
		winner = -1;
		finished = True;
	}
	else if (totalMoveCount == MOVELIMIT) {
		//draw
		winner = 0;
		finished = True;
	}
	//else finished is false (nothing to do)

	return finished;
};
*/

bool Node::isTerminal(int& result) {
	if (childrenArry.getSize() > 0) {
		result = -1;
		return False;
	}
	ArryMove childrenMoves;
	boardState.legalMoves(childrenMoves);
	if (childrenMoves.getSize() == 0) {
		if (boardState.getMoveDir() == 1) {
			//red won
			result = -1;
		}
		else {
			//black won
			result = 1;
		}
		return True;
	}
	int numBlack = 0;
	int numRed = 0;
	boardState.countPieces(numBlack, numRed);
	if (numRed == 0) {
		//black won
		result = 1;
		return True;
	}
	else if (numBlack == 0) {
		//red won
		result = -1;
		return True;
	}
	else if (depth == MOVELIMIT) {
		//draw
		result = 0;
		return True;
	}
	//case of when children moves just haven't been created yet
	//and it is not a winning/draw state
	return False;
}


ArryNode::ArryNode() {
	startingSize = STARTINGSIZENODE;
	sizeTotal = startingSize;
	size = 0;
	frontPtr = new Node[startingSize];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	return;
}
// need destructor also
ArryNode::~ArryNode() {
	delete[] frontPtr;
	frontPtr = nullptr;
	endPtr = nullptr;
	nextPtr = nullptr;
}
//FIXME: WILL NEED TO CHANGE IF CHOPPING OFF FRONT OF TREE
void ArryNode::append(const Node& nodeIn) {
	//check if there is room
	//if not make new array and copy over
	//add and increment nextPtr and size
	if (nextPtr == endPtr) {
		//not enough room
		Node* tempStoragePtr = new Node[sizeTotal + startingSize];
		sizeTotal = sizeTotal + startingSize;
		for (int i = 0; i < size; i++) {
			tempStoragePtr[i] = frontPtr[i];
		}
		delete[] frontPtr;
		frontPtr = nullptr;
		frontPtr = tempStoragePtr;
		endPtr = frontPtr + sizeTotal;
		nextPtr = frontPtr + size;
	}
	*nextPtr = nodeIn;
	nextPtr++;
	size++;

	return;
}
int ArryNode::getSize() {
	return size;
}
//FIXME: WILL NEED TO CHANGE IF CHOPPING OFF FRONT OF TREE
Node& ArryNode::operator[](int i) {
	return frontPtr[i];
}
ArryNode& ArryNode::operator=(const ArryNode& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		//check if enough memory is allocated to copy rhs
		if (rhs.size > sizeTotal) {
			//must delete and make new arry (resize)
			delete[] frontPtr;
			frontPtr = nullptr;
			frontPtr = new Node[rhs.sizeTotal];
			sizeTotal = rhs.sizeTotal;
			endPtr = frontPtr + sizeTotal;
		}
		nextPtr = frontPtr;
		Node* rhsPtr = rhs.frontPtr;
		for (int i = 0; i < rhs.size; i++) {
			*nextPtr = *rhsPtr;
			nextPtr++;
			rhsPtr++;
		}
		size = rhs.size;
	}
	return *this;
}
ArryNode::ArryNode(const ArryNode& rhs) {
	//copy constructor (does same thing as assignment)
	startingSize = STARTINGSIZENODE;
	sizeTotal = rhs.sizeTotal;
	size = rhs.size;
	frontPtr = new Node[sizeTotal];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	Node* rhsPtr = rhs.frontPtr;
	for (int i = 0; i < rhs.size; i++) {
		*nextPtr = *rhsPtr;
		nextPtr++;
		rhsPtr++;
	}
}



int MCTS::expand(int index) {
	//expands/makes one child of node not already made
	//returns the index of the node created
	ArryMove childrenMoves;
	tree[index].boardState.legalMoves(childrenMoves);
	ArryMove newMoves;
	//finding out which moves have already been taken before
	for (int i = 0; i < childrenMoves.getSize(); i++) {
		//for each of the moves in the full list
		bool newMove = True;
		for (int j = 0; j < tree[index].childrenArry.getSize(); j++) {
			//for each of the already created list
			if (childrenMoves[i] == tree[tree[index].childrenArry[j]].move) {
				newMove = False;
			}

		}
		if (newMove == True) {
			newMoves.append(childrenMoves[i]);
		}
	}
	//now randomly select which move from newMoves
	srand((unsigned)time(NULL));
	if (newMoves.getSize() == 0) {
		printf("childrenArry.getSize(): %i\n", tree[index].childrenArry.getSize());
		tree[index].boardState.print();
		printf("\nAlready Children: \n");
		for (int i = 0; i < tree[index].childrenArry.getSize(); i++) {
			tree[tree[index].childrenArry[i]].boardState.print();
		}
		printf("\nPotential Children: \n");
		for (int i = 0; i < childrenMoves.getSize(); i++) {
			BoardState tmpState = tree[index].boardState;
			bool tmpMakeKing = false;
			tmpState.makeMove(childrenMoves[i], tmpMakeKing);
			tmpState.print();
		}

		printf("ERROR HERE: mcts.cpp 1\n");
	}
	int moveInd = rand() % newMoves.getSize();
	//now make the new node/expand
	//change board state, make new node, and add index to children list
	Move currMove = newMoves[moveInd];
	BoardState newBoard = tree[index].boardState;
	bool tmpMakeKing = false;
	newBoard.makeMove(currMove, tmpMakeKing);
	int newDepth = tree[index].depth + 1;
	//int BoardState boardStateIn, Move moveIn, int depthIn
	Node newNode(newBoard, currMove, newDepth);
	int childInd = tree.getSize(); //would be last index plus one so the index of the one open
	tree.append(newNode);
	tree[index].childrenArry.append(childInd);
	//checks if parent node has now been fully expanded
	if ((newMoves.getSize() - 1) == 0) {
		tree[index].fullyExpanded = True;
	}

	return childInd;
}


int MCTS::explore(int rootInd, ArryInt& pathArry) {
	//moves to best leaf (both for before and after expanding) and returns leaf index
	//***Make sure to pass in clear pathArry unless second explore call to select expanded leaf
	int currInd = rootInd;
	int prevInd = rootInd;
	double bestUCT = -1.0;
	int loopCount = 1;
	int temp = 0;
	//append root
	pathArry.append(rootInd);
	while (tree[currInd].fullyExpanded == True) {
		if (tree[currInd].isTerminal(temp) == True) {
			break;
		}
		//evaluation function exploration thingy goes here
		//***FIXME: EVALUATION FUNCTION IS DIFFERENT FOR WHEN THERE IS MODEL
		//Upper Confidence Bound applied to Trees UCT: (Wi/Ni) + cpuct * sqrt(ln(Np) / Ni)
		bestUCT = -1.0;
		//The last one isn't added, so that it is added as root in the second call and must be appended
		//after function call for when selecting which expanded node
		for (int i = 0; i < tree[prevInd].childrenArry.getSize(); i++) {
			//for each child node
			Node currNode = tree[tree[prevInd].childrenArry[i]];
			double currUCT = 0.0;
			if (currNode.numSims > 0) {
				currUCT = 1.0 * currNode.numWins / currNode.numSims;
				currUCT += cpuct * sqrt(log(tree[prevInd].numSims) / currNode.numSims);
			}
			else {
				printf("****ERROR WITH currUCT****\n");
				printf("here");
				//currUCT = INF;
			}
			if (currUCT > bestUCT) {
				currInd = tree[prevInd].childrenArry[i];
				bestUCT = currUCT;
			}
		}
		if (prevInd == currInd) {
			printf("**ERROR HERE: mcts.cpp 2**\n");
			printf("loopCount: %i\n", loopCount);
			printf("path: ");
			for (int i = 0; i < pathArry.getSize(); i++) {
				printf("%i ", pathArry[i]);
			}
			printf("\n");
			printTree(0, 0, -1);
			int a = 1;
		}
		pathArry.append(currInd);
		prevInd = currInd;
		loopCount++;
	};

	return currInd;
}
//******FIXME: MUST CHECK FOR REPEATING MOVES
//MOVELIMIT includes turnes for both players combined
int MCTS::rollout(int leafInd) {
	//returns 1 for win, -1 for loss, and 0 for draw
	//not checking for if given state is already finished
	int result = 2;
	bool endOfGame = False;
	//first checking if expanded node is terminal
	endOfGame = tree[leafInd].isTerminal(result);
	if (endOfGame == True) {
		return result;
	}
	result = 2;

	srand((unsigned)time(NULL));
	//srand(0);
	BoardState boardState = tree[leafInd].boardState;
	ArryMove possibleMoves;
	int moveNum = tree[leafInd].depth;
	int loopCount = 1;
	while (endOfGame == False) {
		//select random next move
		possibleMoves.clear();
		boardState.legalMoves(possibleMoves);
		if (possibleMoves.getSize() == 0) {
			if (boardState.getMoveDir() == 1) {
				//red won
				//boardState.print();
				//printf("*loopCount: %i\n", loopCount);
				result = -1;
				endOfGame = True;
				break;
			}
			else {
				//black won
				//boardState.print();
				//printf("*loopCount: %i\n", loopCount);
				result = 1;
				endOfGame = True;
				break;
			}
		}
		int randInd = rand() % possibleMoves.getSize();
		bool tmpMakeKing = false;
		boardState.makeMove(possibleMoves[randInd], tmpMakeKing);
		//boardState.print();
		moveNum++;
		int numBlack = 12;
		int numRed = 12;
		boardState.countPieces(numBlack, numRed);
		if (numBlack == 0) {
			//red won
			//boardState.print();
			//printf("*loopCount: %i\n", loopCount);
			result = -1;
			endOfGame = True;
		}
		else if (numRed == 0) {
			//black won
			//boardState.print();
			//printf("*loopCount: %i\n", loopCount);
			result = 1;
			endOfGame = True;
		}
		else if (moveNum >= MOVELIMIT) {
			//draw
			//boardState.print();
			//printf("*loopCount: %i\n", loopCount);
			result = 0;
			endOfGame = True;
		}
		loopCount++;
	}
	return result;
}

int MCTS::backprop(int result, ArryInt& pathArry) {
	//returns the actual win, loss, or draw value for root
	//propagate back through array
	//int currResult = result;
	int rootResult = result;
	for (int i = pathArry.getSize() - 1; i >= 0; i--) {
		tree[pathArry[i]].numSims++;
		//FIXME: SWITCHING SIGN FOR WINNING # 1
		if (tree[pathArry[i]].boardState.getMoveDir() == (result * -1)) {
			tree[pathArry[i]].numWins++;
		}
		/*
		if (pathArry[i] == 1) {
			printf("numSims: %i  numWins: %i\n", tree[pathArry[i]].numSims, tree[pathArry[i]].numWins);
			printf("here\n");
		}*/
		//flip the sign for win or loss and does nothing for draw
		//prevResult = currResult;
		//currResult = currResult * -1;
	}
	//FIXME: SWITCHING SIGN FOR WINNING # 2
	/*
	if (tree[pathArry[0]].boardState.getMoveDir() == result) {
		rootResult = 1;
	}
	else if (result == 0) {
		rootResult = 0;
	}
	else {
		rootResult = -1;
	}
	*/
	if (tree[pathArry[0]].boardState.getMoveDir() == result) {
		rootResult = -1;
	}
	else if (result == 0) {
		rootResult = 0;
	}
	else {
		rootResult = 1;
	}
	//returns the result with respect to the current root / turn for AI
	return rootResult;
}

MCTS::MCTS() {
	root = -1;
}
MCTS::MCTS(BoardState rootState) {
	Node rootNode(rootState);
	tree.append(rootNode);
	pastStates.append(rootState);
	root = 0;
}
MCTS::MCTS(const MCTS& rhs) {
	//copy constructor (does same thing as assignment)
	tree = rhs.tree;
	cpuct = rhs.cpuct;
	root = rhs.root;
	pastStates = rhs.pastStates;
}
MCTS& MCTS::operator=(const MCTS& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		tree = rhs.tree;
		cpuct = rhs.cpuct;
		root = rhs.root;
		pastStates = rhs.pastStates;
	}
	return *this;
}
//should not need destructor, copy constructor, =, ==, and !=
void MCTS::changeRoot(Move moveMade) {
	//only states resulting from this player moving matter for repetition
	//(pastState.append is called separately and only when mcts called by this player)
	for (int i = 0; i < tree[root].childrenArry.getSize(); i++) {
		int currInd = tree[root].childrenArry[i];
		if (tree[currInd].move == moveMade) {
			root = currInd;
			return;
		}
	}
	printf("ERROR: Did not find move that was made in MCTS tree");
	return;
}
bool MCTS::tryRoot(BoardState boardState) {
	//only states resulting from this player moving matter for repetition
	//(pastState.append is called separately and only when mcts called by this player)
	bool nodeFound = false;
	for (int i = 0; i < tree[root].childrenArry.getSize(); i++) {
		int currInd = tree[root].childrenArry[i];
		if (tree[currInd].boardState == boardState) {
			root = currInd;
			nodeFound = true;
			return nodeFound;
		}
	}
	//printf("ERROR: Did not find move that was made in MCTS tree");
	return nodeFound;
}
Move MCTS::mcts() {
	ArryInt path;
	int currInd = 0;
	int numChildren = 0;
	int result = 2;
	clock_t startTime = clock();
	clock_t currTime = clock();
	double timeDiff = (double)(currTime - startTime) / CLOCKS_PER_SEC;
	int numRollouts = 0;
	//while (timeDiff < MCTSSEC) {
	while (numRollouts < 1000) {
		path.clear();
		//explore
		currInd = explore(root, path);
		//returns the index of the new node
		result = 2;
		if (tree[currInd].isTerminal(result) == False) {
			//expand
			currInd = expand(currInd);
			path.append(currInd);
			//rollout
			result = rollout(currInd);
		}
		else {
			//printf("* terminal node found *\n");
			//result returned from isTerminal
		}


		//backprop
		result = backprop(result, path);

		currTime = clock();
		timeDiff = (double)(currTime - startTime) / CLOCKS_PER_SEC;
		numRollouts++;
	}

	//now pick best of next moves
	Move bestMove;
	int bestMoveInd = 0;
	double maxUCT = -1.0;
	double currUCT = 0.0;
	printf("UCTs: ");
	for (int i = 0; i < tree[root].childrenArry.getSize(); i++) {
		currInd = tree[root].childrenArry[i];
		if (tree[currInd].numSims > 0) {
			currUCT = 1.0 * tree[currInd].numWins / tree[currInd].numSims;
		}
		else {
			currUCT = 0.0;
		}

		if (currUCT > maxUCT) {
			bestMove = tree[currInd].move;
			bestMoveInd = currInd;
			maxUCT = currUCT;
		}
		printf(" %f", currUCT);
	}
	printf("\n");
	//only states resulting from this player moving matter for repetition
	pastStates.append(tree[bestMoveInd].boardState);
	root = bestMoveInd;
	printf("Sims: %i\n", numRollouts);

	return bestMove;
}

void MCTS::printTree(int ind, int depth, int parent) {
	//will use preorder
	//print node
	printf("ind: %i  depth: %i  parent: %i\n", ind, depth, parent);
	//call for each child
	depth++;
	for (int i = 0; i < tree[ind].childrenArry.getSize(); i++) {
		if (tree[ind].childrenArry[i] != ind) {
			printTree(tree[ind].childrenArry[i], depth, ind);
		}
		else {
			printf("ind: %i  depth: %i\n", tree[ind].childrenArry[i], depth, ind);
		}
	}
	return;
}

