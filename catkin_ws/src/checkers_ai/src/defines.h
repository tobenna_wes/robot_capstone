#ifndef DEFINES_H
#define DEFINES_H

#include <cstdint>

// Typedefs:
typedef uint32_t uint;

// Defines:
#define True 1
#define False 0
#define STARTINGSIZEPT 4
#define STARTINGSIZEMOVE 10
#define STARTINGSIZEQUEUE 10
//MOVELIMIT	is total for red and black (80 moves each)
//decides draw condition
#define MOVELIMIT 160
#define STARTINGSIZENODE 4096
#define STARTINGSIZEINT 8
#define CPUCT 1.0 //range of 0 to 1 for how much favoring unexplored nodes
#define MCTSSEC 5
#define STARTINGSIZEBOARDSTATE 160
#define INF 2147483647

#endif
