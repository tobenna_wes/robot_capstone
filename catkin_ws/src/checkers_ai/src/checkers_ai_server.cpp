#include "ros/ros.h"
#include "checkers_ai/checkers_ai.h"
#include "agent.h"


//***************
Game game;
//int winner = 0;
//user object only used for moveDir
User user(1);
Agent ai;
//***************

bool getCaptured(Move& move, ArryInt& caps) {
  //returns whether pieces were captured
	bool isJump = False;
	int startRow = 0;
	int startCol = 0;
	int endRow = 0;
	int endCol = 0;
	if (move.moveNum == 1) {
		//check for jump (set isJump)
		sqrNumToRowCol(move.startPt.sqrNum, startRow, startCol);
		sqrNumToRowCol(move.endPts[0].sqrNum, endRow, endCol);
		if (abs(endRow - startRow) > 1) {
			//jump
			isJump = True;
		}
		//else no jump
	}
	else {
		//definitely jump
		isJump = True;
	}

	if (isJump == True) {
		int currRow = 0;
		int currCol = 0;
		sqrNumToRowCol(move.startPt.sqrNum, currRow, currCol);
		endRow = 0;
		endCol = 0;
		//for each endPt find and subtract captured piece from oppntPlayerBoard;
		for (int i = 0; i < move.endPts.getSize(); i++) {
			sqrNumToRowCol(move.endPts[i].sqrNum, endRow, endCol);
			int rowDir = (endRow - currRow) / 2;
			int colDir = (endCol - currCol) / 2;
			int captureRow = currRow + rowDir;
			int captureCol = currCol + colDir;
			int captureSqrNum = rowColToSqrNum(captureRow, captureCol);
      caps.append(captureSqrNum);
			currRow = endRow;
			currCol = endCol;
    }
	}
	
	return isJump;
}


bool test(checkers_ai::checkers_ai::Request  &req,
         checkers_ai::checkers_ai::Response &res)
{
  res.startPt = 0;
  res.endpts = {0};
  res.captured = {0};

  //BoardState tmp;
  //game.updateBoard(tmp);
  //ROS_INFO("worked = %d", game.tester());
  //ROS_INFO("worked = %d", 10);

  //******************************************
  printf("****************************************\n");
  //check if game was won before user took turn
  int winner = 0;
  if (game.isFinished(winner) == True) {
    ROS_INFO("** Game Finished **");
    if (winner == 1) {
      ROS_INFO("\n	***  BLACK WON  ***\n");
    }
    else if (winner == -1) {
      ROS_INFO("\n	***  RED WON  ***\n");
    }
    else {
      ROS_INFO("\n	***  DRAW WON  ***\n");
    }
    printf("****************************************\n");
    return true;
  }
  game.totalMoveCount++;
  BoardState currState(req.currPlayer, req.oppntPlayer, req.bothIsKing, req.moveDir);
  ROS_INFO("Updating Board State...");
  game.updateBoard(currState);
  //check if game was won after user turn
  winner = 0;
  if (game.isFinished(winner) == True) {
    ROS_INFO("** Game Finished **");
    if (winner == 1) {
      ROS_INFO("\n	***  BLACK WON  ***\n");
    }
    else if (winner == -1) {
      ROS_INFO("\n	***  RED WON  ***\n");
    }
    else {
      ROS_INFO("\n	***  DRAW WON  ***\n");
    }
    printf("****************************************\n");
    return true;
  }

  //game not over so now onto ai move
  bool nodeFound = false;
  if (game.totalMoveCount > 1) {
    //Try to change root to node with startState
    //ROS_INFO("tryRoot called...");
    nodeFound = ai.mcts.tryRoot(currState);
  }
  else {
    //first call so must reset mcts in ai
    ROS_INFO("Reset MCTS...");
    ai.resetMCTS(currState);
    nodeFound = true;
  }
  //if no match then reset mcts
  if (nodeFound == false) {
    ROS_INFO("*** ERROR: Node not found. Resetting MCTS... ***");
    ai.resetMCTS(currState);
  }
  //can now do mcts and take turn (good to go)
  bool makeKing = false;
  ROS_INFO("Taking AI Turn...");
  Move moveMade = ai.takeTurn(game, makeKing);
  ArryInt caps;
  //ROS_INFO("Getting Captured Pieces...");
  bool capsFound = getCaptured(moveMade, caps);
  game.currBoard.print();

  //then check for if game just ended
  //ROS_INFO("Checking for Winner...");
  winner = 0;
  if (game.isFinished(winner) == True) {
    ROS_INFO("** Game Finished **");
    if (winner == 1) {
      ROS_INFO("\n	***  BLACK WON  ***\n");
    }
    else if (winner == -1) {
      ROS_INFO("\n	***  RED WON  ***\n");
    }
    else {
      ROS_INFO("\n	***  DRAW WON  ***\n");
    }
  }
  
  //now setting response values
  /*
  res.startPt = 0;
  res.endpts = {0};
  res.captured = {0};
  */
  //ROS_INFO("Setting Response Values...");
  ROS_INFO("request: currPlayer=%ld, oppntPlayer=%ld, bothIsKing=%ld, moveDir=%ld", (long int)req.currPlayer, (long int)req.oppntPlayer, (long int)req.bothIsKing, (long int)req.moveDir);
  res.startPt = moveMade.startPt.sqrNum;
  ROS_INFO("sending back response: %ld", (long int)res.startPt);
  //ArryInt endPts;
  std::vector<int> endPts;
  printf("endPts: ");
  for (int i = 0; i < moveMade.endPts.getSize(); i++) {
    //endPts.append(maveMade.endPts[i].sqrNum);
    endPts.push_back(moveMade.endPts[i].sqrNum);
    printf("%d ", moveMade.endPts[i].sqrNum);
  }
  printf("\n");
  //res.endpts = endPts.frontPtr;
  res.endpts = endPts;
  std::vector<int> captured;
  printf("captured: ");
  for (int i = 0; i < caps.getSize(); i++) {
    captured.push_back(caps[i]);
    printf("%d ", caps[i]);
  }
  printf("\n");
  //res.captured = caps.frontPtr;
  res.captured = captured;
  if (makeKing == false) {
    res.makeKing = -1;
  }
  else {
    res.makeKing = endPts.back();
  }
  printf("makeKing: %d\n", res.makeKing);
  printf("****************************************\n");


  //******************************************
 
  return true;
}




int main(int argc, char **argv)
{
  ROS_INFO("Starting Program...");
  
  
  

  ros::init(argc, argv, "checkers_ai_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("checkers_ai", test);
  ROS_INFO("Ready to Return Next Move.");
  ros::spin();

  return 0;
}





/*
int main() {
	cout << "Starting Program..." << endl;
	Game game;
	int turnCount = 1;
	int winner = 0;
	User user(1);
	

	//First Round with Initializing MCTS
	//User's Turn
	game.currBoard.print();
	Move moveMade = user.takeTurn(game);

	//AI's Turn
	//initialize AI
	Agent ai(-1, game.currBoard);
	//no need for change root
	game.currBoard.print();
	moveMade = ai.takeTurn(game);

	while (game.isFinished(winner) == False) {
		//User's Turn
		game.currBoard.print();
		Move moveMade = user.takeTurn(game);

		//AI's Turn
		ai.mcts.changeRoot(moveMade);
		game.currBoard.print();
		moveMade = ai.takeTurn(game);
	}
	game.currBoard.print();

	if (winner == 1) {
		cout << endl << "	***  BLACK WON  ***" << endl << endl;
	}
	else if (winner == -1) {
		cout << endl << "	***  RED WON  ***" << endl << endl;
	}
	else {
		cout << endl << "	***  DRAW  ***" << endl << endl;
	}

	cout << "Exiting Program...";

	system("pause");
	return 0;
};
*/
