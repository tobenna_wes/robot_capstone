// Headder File for Board State Class
// Referee Functions Will be Included in this Class

#ifndef BOARDSTATE_H
#define BOARDSTATE_H

#include <iostream>
#include "generalFunctions.h"



class BoardState {
private:
	uint currPlayerBoard;
	uint oppntPlayerBoard;
	uint bothIsKingBoard;
	int moveDir;
	// FIXME: STARTING OUT WITH 26 CHARS FOR ID
	//FIXME: FIX ID
	//Converting uint to hex for ids. Requires 26 chars total with end of line
	//In order: 8 for curr, 8 for oppnt, 8 for isKing, 1 for moveDir (1 and 0 for -1), 
	//and 1 for end of line. Largest value for curr or oppnt is 4293918720
	//char id[26] = "sssssssssssssssssssssssss";
	std::string id = "sssssssssssssssssssssssss";
	// FIXME: WRITE FUNCTION DEFINITION IN CPP FILE
	//FIXME: FIX ID
	void  makeID();
	bool isOpen(Pt);
	bool isOpen(uint);
	bool isOpen(int);
	void getSurSqrs(Pt, ArryPt&, bool);
	bool validEndPt(Pt, Pt);
	bool isOppnt(Pt);
	bool isOppnt(int);
	bool isOppnt(uint);
	bool isCurrPlayer(int);
	bool isKing(Pt);
	bool singleJump(Pt, Pt, Pt, Pt&);
	bool nextJumps(Move&, ArryPt&);
	bool howCanMove(Pt, ArryMove&);
	/*
	void  makeID();
	bool isOpen(Pt pt);
	bool isOpen(uint maskIn);
	bool isOpen(int sqrNum);
	void getSurSqrs(Pt pt, ArryPt& sqrs, bool isKing);
	bool validEndPt(Pt origStartPt, Pt endPt);
	bool isOppnt(Pt pt);
	bool isOppnt(int sqrNum);
	bool isOppnt(uint maskIn);
	bool isCurrPlayer(int sqrNum);
	bool isKing(Pt pt);
	bool singleJump(Pt origStartPt, Pt startPt, Pt skipPt, Pt& endPtIn);
	bool nextJumps(Move& partMove, ArryPt& endPts);
	bool howCanMove(Pt startPt, ArryMove& moves);
	*/

public:
	BoardState();
	BoardState(uint, uint, uint, int);
	BoardState(const BoardState&);
	BoardState& operator=(const BoardState&);
	bool operator==(BoardState&);
	bool operator!=(BoardState&);
	void legalMoves(ArryMove&);
	void makeMove(Move, bool&);
	void countPieces(int&, int&);
	void print();
	int getMoveDir();
	/*
	BoardState();
	BoardState(uint currPlayerBoardIn, uint oppntPlayerBoardIn, uint bothIsKingBoardIn, int moveDirIn);
	BoardState(const BoardState& rhs);
	BoardState& operator=(const BoardState& rhs);
	bool operator==(BoardState& rhs);
	bool operator!=(BoardState& rhs);
	void legalMoves(ArryMove& moves);
	void makeMove(Move move);
	void countPieces(int& numBlack, int& numRed);
	void print();
	int getMoveDir();
	*/
};


class ArryBoardState {
	//indexing can be changed for removing top of tree
	//incremental array instead of doubling
private:
	int size;
	int sizeTotal;
	int startingSize;
	// actual indices are 0 to sizeTotal - 1
	BoardState* frontPtr;
	//one past the last
	BoardState* endPtr;
	BoardState* nextPtr;

public:
	ArryBoardState();
	~ArryBoardState();
	ArryBoardState& operator=(const ArryBoardState&);
	ArryBoardState(const ArryBoardState&);
	void append(const BoardState&);
	int getSize();
	BoardState& operator[](int);
	/*
	ArryBoardState();
	~ArryBoardState();
	ArryBoardState& operator=(const ArryBoardState& rhs);
	ArryBoardState(const ArryBoardState& rhs);
	void append(const BoardState& nodeIn);
	int getSize();
	BoardState& operator[](int i);
	*/
};


#endif
	



