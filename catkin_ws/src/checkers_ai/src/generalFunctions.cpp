// CPP File for Board State Class

#include "generalFunctions.h"


int rowColToSqrNum(int rowIn, int colIn) {
	if ((rowIn < 0) || (rowIn > 7)) {
		return -1;
	}
	if ((colIn < 0) || (colIn > 7)) {
		return -1;
	}
	if ((rowIn % 2) == (colIn % 2)) {
		//even rows can only have odd columns and vice versa
		return -1;
	}
	//using integer division to handle +1 diff for even and odd
	int sqrNum = rowIn * 4 + (colIn / 2);

	return sqrNum;
}
void sqrNumToRowCol(int sqrNumIn, int& rowIn, int& colIn) {
	if ((sqrNumIn < 0) || (sqrNumIn > 31)) {
		rowIn = -1;
		colIn = -1;
	}
	int row = sqrNumIn / 4;
	int col = sqrNumIn % 4;
	if ((row % 2) == 0) {
		//even row so odd col
		col = col * 2 + 1;
		rowIn = row;
		colIn = col;
	}
	else {
		//odd row so even col
		col = col * 2;
		rowIn = row;
		colIn = col;
	}
	return;
}



Pt::Pt() {
	mask = 0;
	sqrNum = -1;
}
uint Pt::makeMask(int sqrNumTemp) {
	//+ 0.5 to make sure not accidentally rounded down wrong)
	uint tempMask = (uint)(pow(2, sqrNumTemp) + 0.5);
	return tempMask;
}
int Pt::makeSqrNum(uint maskTemp) {
	int tempSqrNum = (int)((log((double)maskTemp) / log(2.0)) + 0.5);
	return tempSqrNum;
}
Pt::Pt(int sqrNumIn) {
	mask = makeMask(sqrNumIn);
	sqrNum = sqrNumIn;
	return;
}
Pt::Pt(uint maskIn) {
	mask = maskIn;
	sqrNum = makeSqrNum(maskIn);
	return;
}
Pt::Pt(int rowIn, int colIn) {
	sqrNum = rowColToSqrNum(rowIn, colIn);
	mask = makeMask(sqrNum);
	return;
}
void Pt::change(int sqrNumIn) {
	mask = makeMask(sqrNumIn);
	sqrNum = sqrNumIn;
	return;
}
void Pt::change(uint maskIn) {
	mask = maskIn;
	sqrNum = makeSqrNum(maskIn);
	return;
}
Pt& Pt::operator=(const Pt& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		mask = rhs.mask;
		sqrNum = rhs.sqrNum;
	}
	return *this;
}
Pt::Pt(const Pt& rhs) {
	//copy constructor (does same thing as assignment)
	mask = rhs.mask;
	sqrNum = rhs.sqrNum;
}
bool Pt::operator==(const Pt& rhs) {
	if (rhs.mask != mask) {
		return False;
	}
	return True;
}
bool Pt::operator!=(const Pt& rhs) {
	if (rhs.mask != mask) {
		return True;
	}
	return False;
}
void Pt::print() {
	printf("Pt| sqrNum: %i  mask: %u\n", sqrNum, mask);
	return;
}

uint makeMask(int sqrNumTemp) {
	//+ 0.5 to make sure not accidentally rounded down wrong)
	uint tempMask = (uint)(pow(2, sqrNumTemp) + 0.5);
	return tempMask;
}


ArryPt::ArryPt() {
	startingSizePt = STARTINGSIZEPT;
	sizeTotal = startingSizePt;
	size = 0;
	frontPtr = new Pt[startingSizePt];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	return;
}
// need destructor also
ArryPt::~ArryPt() {
	delete[] frontPtr;
	frontPtr = nullptr;
	endPtr = nullptr;
	nextPtr = nullptr;
}
ArryPt& ArryPt::operator=(const ArryPt& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		//check if enough memory is allocated to copy rhs
		if (rhs.size > sizeTotal) {
			//must delete and make new arry (resize)
			delete[] frontPtr;
			frontPtr = nullptr;
			frontPtr = new Pt[rhs.sizeTotal];
			sizeTotal = rhs.sizeTotal;
			endPtr = frontPtr + sizeTotal;
		}
		nextPtr = frontPtr;
		Pt* rhsPtr = rhs.frontPtr;
		for (int i = 0; i < rhs.size; i++) {
			*nextPtr = *rhsPtr;
			nextPtr++;
			rhsPtr++;
		}
		size = rhs.size;
	}
	return *this;
}
ArryPt::ArryPt(const ArryPt& rhs) {
	//copy constructor (does same thing as assignment)
	startingSizePt = STARTINGSIZEPT;
	sizeTotal = rhs.sizeTotal;
	size = rhs.size;
	frontPtr = new Pt[sizeTotal];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	//check if enough memory is allocated to copy rhs
	Pt* rhsPtr = rhs.frontPtr;
	for (int i = 0; i < rhs.size; i++) {
		*nextPtr = *rhsPtr;
		nextPtr++;
		rhsPtr++;
	}
}
bool ArryPt::operator==(const ArryPt& rhs) {
	//check for self assignment
	//compare size (sizeTotal is not considered) and then check each element
	if (this != &rhs) {
		if (rhs.size != size) {
			return False;
		}
		for (int i = 0; i < size; i++) {
			if (rhs.frontPtr[i] != frontPtr[i]) {
				return False;
			}
		}
	}
	return True;
}
bool ArryPt::operator!=(const ArryPt& rhs) {
	//switched Trues and Falses
	if (this != &rhs) {
		if (rhs.size != size) {
			return True;
		}
		for (int i = 0; i < size; i++) {
			if (rhs.frontPtr[i] != frontPtr[i]) {
				return True;
			}
		}
	}
	return False;
}
void ArryPt::append(const Pt ptIn) {
	//check if there is room
	//if not make new array and copy over
	//add and increment nextPtr and size
	if (nextPtr == endPtr) {
		//not enough room
		Pt* tempStoragePtr = new Pt[sizeTotal * 2];
		sizeTotal = sizeTotal * 2;
		for (int i = 0; i < size; i++) {
			tempStoragePtr[i] = frontPtr[i];
		}
		delete[] frontPtr;
		frontPtr = nullptr;
		frontPtr = tempStoragePtr;
		endPtr = frontPtr + sizeTotal;
		nextPtr = frontPtr + size;
	}
	*nextPtr = ptIn;
	nextPtr++;
	size++;

	return;
}
bool ArryPt::isEmpty() {
	if (size > 0) {
		return False;
	}
	return True;
}
int ArryPt::getSize() {
	return size;
}
Pt ArryPt::operator[](int i) {
	return frontPtr[i];
}
void ArryPt::clear() {
	size = 0;
	nextPtr = frontPtr;
	return;
}
Pt ArryPt::last() {
	Pt returnPt;
	if (size > 0) {
		returnPt = frontPtr[size - 1];
	}
	return returnPt;
}

Move::Move() {
	moveNum = 0;
	return;
}
Move::Move(Pt startPtIn, Pt endPtIn) {
	startPt = startPtIn;
	endPts.append(endPtIn);
	moveNum = 1;
	return;
}
void Move::addEndPt(Pt endPtIn) {
	endPts.append(endPtIn);
	moveNum++;
	return;
}
Move& Move::operator=(const Move& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		startPt = rhs.startPt;
		endPts = rhs.endPts;
		moveNum = rhs.moveNum;
	}
	return *this;
}
Move::Move(const Move& rhs) {
	//copy constructor (does same thing as assignment)
	startPt = rhs.startPt;
	endPts = rhs.endPts;
	moveNum = rhs.moveNum;
}
bool Move::operator==(const Move& rhs) {
	if ((startPt != rhs.startPt) || (endPts != rhs.endPts)) {
		return False;
	}
	return True;
}
bool Move::operator!=(const Move& rhs) {
	if ((startPt != rhs.startPt) || (endPts != rhs.endPts)) {
		return True;
	}
	return False;
}
void Move::prevStep(Pt& prevStartPt, Pt& currPt) {
	currPt = endPts.last();
	if (moveNum > 1) {
		prevStartPt = endPts[moveNum - 2];
	}
	else {
		prevStartPt = startPt;
	}
}
void Move::print() {
	printf("*********************\n");
	printf("Move| startPt: ");
	startPt.print();
	for (int i = 0; i < moveNum; i++) {
		endPts[i].print();
	}
	printf("moveNum: %i\n", moveNum);
	printf("*********************\n");
	return;
}


ArryMove::ArryMove() {
	startingSize = STARTINGSIZEPT;
	sizeTotal = startingSize;
	size = 0;
	frontPtr = new Move[startingSize];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	return;
}
// need destructor also
ArryMove::~ArryMove() {
	delete[] frontPtr;
	frontPtr = nullptr;
	endPtr = nullptr;
	nextPtr = nullptr;
}
ArryMove& ArryMove::operator=(const ArryMove& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		//check if enough memory is allocated to copy rhs
		if (rhs.size > sizeTotal) {
			//must delete and make new arry (resize)
			delete[] frontPtr;
			frontPtr = nullptr;
			frontPtr = new Move[rhs.sizeTotal];
			sizeTotal = rhs.sizeTotal;
			endPtr = frontPtr + sizeTotal;
		}
		nextPtr = frontPtr;
		Move* rhsPtr = rhs.frontPtr;
		for (int i = 0; i < rhs.size; i++) {
			*nextPtr = *rhsPtr;
			nextPtr++;
			rhsPtr++;
		}
		size = rhs.size;
	}
	return *this;
}
ArryMove::ArryMove(const ArryMove& rhs) {
	//copy constructor (does same thing as assignment)
	startingSize = STARTINGSIZEPT;
	sizeTotal = rhs.sizeTotal;
	size = rhs.size;
	frontPtr = new Move[sizeTotal];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	Move* rhsPtr = rhs.frontPtr;
	for (int i = 0; i < rhs.size; i++) {
		*nextPtr = *rhsPtr;
		nextPtr++;
		rhsPtr++;
	}
}
bool ArryMove::operator==(const ArryMove& rhs) {
	//check for self assignment
	//compare size (sizeTotal is not considered) and then check each element
	if (this != &rhs) {
		if (rhs.size != size) {
			return False;
		}
		for (int i = 0; i < size; i++) {
			if (rhs.frontPtr[i] != frontPtr[i]) {
				return False;
			}
		}
	}
	return True;
}
bool ArryMove::operator!=(const ArryMove& rhs) {
	//switched Trues and Falses
	if (this != &rhs) {
		if (rhs.size != size) {
			return True;
		}
		for (int i = 0; i < size; i++) {
			if (rhs.frontPtr[i] != frontPtr[i]) {
				return True;
			}
		}
	}
	return False;
}
void ArryMove::append(const Move& moveIn) {
	//check if there is room
	//if not make new array and copy over
	//add and increment nextPtr and size
	if (nextPtr == endPtr) {
		//not enough room
		Move* tempStoragePtr = new Move[sizeTotal * 2];
		sizeTotal = sizeTotal * 2;
		for (int i = 0; i < size; i++) {
			tempStoragePtr[i] = frontPtr[i];
		}
		delete[] frontPtr;
		frontPtr = nullptr;
		frontPtr = tempStoragePtr;
		endPtr = frontPtr + sizeTotal;
		nextPtr = frontPtr + size;
	}
	*nextPtr = moveIn;
	nextPtr++;
	size++;

	return;
}
bool ArryMove::isEmpty() {
	if (size > 0) {
		return False;
	}
	return True;
}
int ArryMove::getSize() {
	return size;
}
Move ArryMove::operator[](int i) {
	Move tempMove = frontPtr[i];
	return tempMove;
}
void ArryMove::clear() {
	size = 0;
	nextPtr = frontPtr;
	return;
}

void ArryMove::remove(int ind) {
	if (ind < (size - 1)) {
		for (int i = ind; i < (size - 1); i++) {
			//i is spot to be copied to and i + 1 is next/ one being copied
			frontPtr[i] = frontPtr[i + 1];
		}
		nextPtr = frontPtr + (size - 1);
		size = size - 1;
	}
	else if (ind < size) {
		//removing the last one
		nextPtr = frontPtr + (size - 1);
		size = size - 1;
	}
	return;
}

QueueMove::QueueMove() {
	startingSize = STARTINGSIZEQUEUE;
	sizeTotal = startingSize;
	size = 0;
	storagePtr = new Move[startingSize];
	frontPtr = storagePtr;
	nextPtr = frontPtr;
	return;
}
// need destructor also
QueueMove::~QueueMove() {
	delete[] storagePtr;
	storagePtr = nullptr;
	frontPtr = nullptr;
	nextPtr = nullptr;
	return;
}
void QueueMove::enqueue(const Move& ptIn) {
	//check if there is room
	//if not make new array and copy over
	//add and increment nextPtr and size
	if (size == sizeTotal) {
		//not enough room
		Move* tempStoragePtr = new Move[sizeTotal * 2];
		for (int i = 0; i < size; i++) {
			//tempStoragePtr[i] = frontPtr[i];
			tempStoragePtr[i] = *(storagePtr + ((frontPtr - storagePtr + i) % sizeTotal));
		}
		sizeTotal = sizeTotal * 2;
		delete[] storagePtr;
		storagePtr = nullptr;
		storagePtr = tempStoragePtr;
		frontPtr = storagePtr;
		nextPtr = frontPtr + size;
	}
	*nextPtr = ptIn;
	nextPtr = storagePtr + ((nextPtr - storagePtr + 1) % sizeTotal);
	size++;

	return;
}
Move QueueMove::dequeue() {
	//remove from front
	Move tempPt;
	if (size > 0) {
		tempPt = *frontPtr;
		frontPtr = storagePtr + ((frontPtr - storagePtr + 1) % sizeTotal);
		size = size - 1;
	}

	return tempPt;
}
bool QueueMove::isEmpty() {
	if (size > 0) {
		return False;
	}
	return True;
}
int QueueMove::getSize() {
	return size;
}
void QueueMove::clear() {
	size = 0;
	frontPtr = storagePtr;
	nextPtr = frontPtr;
	return;
}
Move QueueMove::front() {
	Move returnPt;
	if (size > 0) {
		returnPt = *frontPtr;
	}
	return returnPt;
}

ArryInt::ArryInt() {
	startingSizeInt = STARTINGSIZEINT;
	sizeTotal = startingSizeInt;
	size = 0;
	frontPtr = new int[startingSizeInt];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	return;
}
// need destructor also
ArryInt::~ArryInt() {
	delete[] frontPtr;
	frontPtr = nullptr;
	endPtr = nullptr;
	nextPtr = nullptr;
}
void ArryInt::append(const int intIn) {
	//check if there is room
	//if not make new array and copy over
	//add and increment nextPtr and size
	if (nextPtr == endPtr) {
		//not enough room
		int* tempStoragePtr = new int[sizeTotal * 2];
		sizeTotal = sizeTotal * 2;
		for (int i = 0; i < size; i++) {
			tempStoragePtr[i] = frontPtr[i];
		}
		delete[] frontPtr;
		frontPtr = nullptr;
		frontPtr = tempStoragePtr;
		endPtr = frontPtr + sizeTotal;
		nextPtr = frontPtr + size;
	}
	*nextPtr = intIn;
	nextPtr++;
	size++;

	return;
}
bool ArryInt::isEmpty() {
	if (size > 0) {
		return False;
	}
	return True;
}
int ArryInt::getSize() {
	return size;
}
int ArryInt::operator[](int i) {
	return frontPtr[i];
}
void ArryInt::clear() {
	size = 0;
	nextPtr = frontPtr;
	return;
}
ArryInt& ArryInt::operator=(const ArryInt& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		//check if enough memory is allocated to copy rhs
		if (rhs.size > sizeTotal) {
			//must delete and make new arry (resize)
			delete[] frontPtr;
			frontPtr = nullptr;
			frontPtr = new int[rhs.sizeTotal];
			sizeTotal = rhs.sizeTotal;
			endPtr = frontPtr + sizeTotal;
		}
		nextPtr = frontPtr;
		int* rhsPtr = rhs.frontPtr;
		for (int i = 0; i < rhs.size; i++) {
			*nextPtr = *rhsPtr;
			nextPtr++;
			rhsPtr++;
		}
		size = rhs.size;
	}
	return *this;
}
ArryInt::ArryInt(const ArryInt& rhs) {
	//copy constructor (does same thing as assignment)
	startingSizeInt = STARTINGSIZEINT;
	sizeTotal = rhs.sizeTotal;
	size = rhs.size;
	frontPtr = new int[sizeTotal];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	int* rhsPtr = rhs.frontPtr;
	for (int i = 0; i < rhs.size; i++) {
		*nextPtr = *rhsPtr;
		nextPtr++;
		rhsPtr++;
	}
}