// Main File for Testing C++ Checkers Classes
//#include <iostream>
//#include "game.h"
//#include "defines.h"
#include "agent.h"

using namespace std;

/*
int main() {
	cout << "Starting Program..." << endl;
	Game game;
	ArryMove possibleMoves;
	int turnCount = 1;
	int winner = 0;
	while (game.isFinished(winner) == False) {
		game.currBoard.print();
		//game.currBoard.legalMoves(possibleMoves);
		game.getMoves(possibleMoves);
		for (int i = 0; i < possibleMoves.getSize(); i++) {
			//printf("{%i}  ", i);
			printf("{%c}  ", 'a' + i);
			possibleMoves[i].print();
		}

		//user input
		//Uses letters instead of numbers so works for 26 moves options
		cout << "Select Move Letter:" << endl;
		char moveChar = '0';
		cin >> moveChar;
		moveChar = tolower(moveChar);
		int moveNum = moveChar - 'a';
		if ((moveNum < 0) || (moveNum >= possibleMoves.getSize())) {
			//invalid move selected
			moveNum = 0;
		}
		cout << "Making Move " << moveChar << "..." << endl;

		//game.currBoard.makeMove(possibleMoves[moveNum]);
		game.movePiece(possibleMoves[moveNum]);
		possibleMoves.clear();
	}
	game.currBoard.print();

	if (winner == 1) {
		cout << endl << "	***  BLACK WON  ***" << endl << endl;
	}
	else if (winner == -1) {
		cout << endl << "	***  RED WON  ***" << endl << endl;
	}
	else {
		cout << endl << "	***  DRAW  ***" << endl << endl;
	}

	cout << "Exiting Program...";

	system("pause");
	return 0;
};
*/

int main() {
	cout << "Starting Program..." << endl;
	Game game;
	int turnCount = 1;
	int winner = 0;
	User user(1);
	

	//First Round with Initializing MCTS
	//User's Turn
	game.currBoard.print();
	Move moveMade = user.takeTurn(game);

	//AI's Turn
	//initialize AI
	Agent ai(-1, game.currBoard);
	//no need for change root
	game.currBoard.print();
	moveMade = ai.takeTurn(game);

	while (game.isFinished(winner) == False) {
		//User's Turn
		game.currBoard.print();
		Move moveMade = user.takeTurn(game);

		//AI's Turn
		ai.mcts.changeRoot(moveMade);
		game.currBoard.print();
		moveMade = ai.takeTurn(game);
	}
	game.currBoard.print();

	if (winner == 1) {
		cout << endl << "	***  BLACK WON  ***" << endl << endl;
	}
	else if (winner == -1) {
		cout << endl << "	***  RED WON  ***" << endl << endl;
	}
	else {
		cout << endl << "	***  DRAW  ***" << endl << endl;
	}

	cout << "Exiting Program...";

	system("pause");
	return 0;
};