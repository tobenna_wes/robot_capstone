// Headder File for Board State Class
// Referee Functions Will be Included in this Class

#ifndef GENERALFUNCTIONS_H
#define GENERALFUNCTIONS_H

#include "defines.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>


int rowColToSqrNum(int, int);
void sqrNumToRowCol(int,int&, int&);


class Pt {
public:
	uint mask;
	int sqrNum;
	Pt();
	uint makeMask(int);
	int makeSqrNum(uint);
	Pt(int);
	Pt(uint);
	Pt(int, int);
	void change(int);
	void change(uint);
	Pt& operator=(const Pt&);
	Pt(const Pt&);
	bool operator==(const Pt&);
	bool operator!=(const Pt&);
	void print();
};

uint makeMask(int);


class ArryPt {
	//indexing is normal but must use insert function
private:
	int size;
	int sizeTotal;
	int startingSizePt;
	// actual indices are 0 to sizeTotal - 1
	Pt* frontPtr;
	//one past the last
	Pt* endPtr;
	Pt* nextPtr;

public:
	ArryPt();
	// need destructor also
	~ArryPt();
	ArryPt& operator=(const ArryPt&);
	ArryPt(const ArryPt&);
	bool operator==(const ArryPt&);
	bool operator!=(const ArryPt&);
	void append(const Pt);
	bool isEmpty();
	int getSize();
	Pt operator[](int);
	void clear();
	Pt last();
};

class Move {
public:
	Pt startPt;
	ArryPt endPts;
	int moveNum;
	Move();
	Move(Pt, Pt);
	void addEndPt(Pt);
	Move& operator=(const Move&);
	Move(const Move&);
	bool operator==(const Move&);
	bool operator!=(const Move&);
	void prevStep(Pt&, Pt&);
	void print();
};


class ArryMove {
	//indexing is normal but must use insert function
private:
	int size;
	int sizeTotal;
	int startingSize;
	// actual indices are 0 to sizeTotal - 1
	Move* frontPtr;
	//one past the last
	Move* endPtr;
	Move* nextPtr;

public:
	ArryMove();
	// need destructor also
	~ArryMove();
	ArryMove& operator=(const ArryMove&);
	ArryMove(const ArryMove&);
	bool operator==(const ArryMove&);
	bool operator!=(const ArryMove&);
	void append(const Move&);
	bool isEmpty();
	int getSize();
	Move operator[](int);
	void clear();
	void remove(int);
};

class QueueMove {
private:
	int size;
	int sizeTotal;
	int startingSize;
	Move* storagePtr;
	//don't need one past last
	Move* frontPtr;
	Move* nextPtr;

public:
	QueueMove();
	// need destructor also
	~QueueMove();
	void enqueue(const Move&);
	Move dequeue();
	bool isEmpty();
	int getSize();
	void clear();
	Move front();
};

class ArryInt {
	//indexing is normal but must use insert function
private:
	int size;
	int sizeTotal;
	int startingSizeInt;
	// actual indices are 0 to sizeTotal - 1
	int* frontPtr;
	//one past the last
	int* endPtr;
	int* nextPtr;

public:
	ArryInt();
	// need destructor also
	~ArryInt();
	void append(const int);
	bool isEmpty();
	int getSize();
	int operator[](int);
	void clear();
	ArryInt& operator=(const ArryInt&);
	ArryInt(const ArryInt&);
};


#endif