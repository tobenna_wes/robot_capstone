#ifndef MCTS_H
#define MCTS_H

#include "boardState.h"
#include <time.h>

//***important to store actual taken moves in separate list***

//*can cut top part of tree off (before current root node) and keep track of offset when 
//*resizing to reduce memory usage (if not keeping game tree between full games and do not 
//*need full game tree for preventing either repeating moves or repeating states)

//***FIXME: MUST PREVENT EITHER REPEATING MOVES OR REPEATING STATES

//*Idea: hash table would be too big. Can keep a list of just the ids of prev states with the 
//current number of pieces to make searching for repeating states slightly faster. That way you
//don't have to search through the entire tree array from beginning to end.

class Node {
private:
public:
	int numWins; //from previous node to this one
	int numSims; //from previous node to this one
	Move move; //from previous node to this one
	//boardState has different moveDir than prev move, numWins, and numSims
	BoardState boardState; //resulting board state
	ArryInt childrenArry; //list indices of children nodes
	int depth;
	bool fullyExpanded;
	//for root node
	Node();
	Node(BoardState);
	//all other nodes
	Node(BoardState, Move, int);
	//no destructor, copy constructor, =, == or != operator needed
	//probably not used
	/*
	bool isLeaf() {
		if (childrenArry.getSize() > 0) {
			return False;
		}
		return True;
	};*/

	/*
	bool isFinished(int& winner){
		//winner is 1 for black, -1 for red, and 0 for draw
		bool finished = False;
		//count both colored pieces
		int numBlack = 0;
		int numRed = 0;
		currBoard.countPieces(numBlack, numRed);
		if (numRed == 0) {
			//black won
			winner = 1;
			finished = True;
		}
		else if (numBlack == 0) {
			//red won
			winner = -1;
			finished = True;
		}
		else if (totalMoveCount == MOVELIMIT) {
			//draw
			winner = 0;
			finished = True;
		}
		//else finished is false (nothing to do)

		return finished;
	};
	*/
	bool isTerminal(int&);
};


class ArryNode {
	//indexing can be changed for removing top of tree
	//incremental array instead of doubling
private:
	int size;
	int sizeTotal;
	int startingSize;
	// actual indices are 0 to sizeTotal - 1
	Node* frontPtr;
	//one past the last
	Node* endPtr;
	Node* nextPtr;

public:
	ArryNode();
	// need destructor also
	~ArryNode();
	//FIXME: WILL NEED TO CHANGE IF CHOPPING OFF FRONT OF TREE
	void append(const Node&);
	int getSize();
	//FIXME: WILL NEED TO CHANGE IF CHOPPING OFF FRONT OF TREE
	Node& operator[](int);
	ArryNode& operator=(const ArryNode&);
	ArryNode(const ArryNode&);
};


class MCTS {
private:
	ArryNode tree;
	double cpuct = CPUCT;
	int root;
	//FIXME: Could change to array of ids instead of full board states
	ArryBoardState pastStates;

	int expand(int);
	
	int explore(int, ArryInt&);
	//******FIXME: MUST CHECK FOR REPEATING MOVES
	//MOVELIMIT includes turnes for both players combined
	int rollout(int);

	int backprop(int, ArryInt&);

public:
	MCTS();
	MCTS(BoardState);
	MCTS(const MCTS&);
	MCTS& operator=(const MCTS&);
	//should not need destructor, copy constructor, =, ==, and !=
	void changeRoot(Move);
	bool tryRoot(BoardState);
	Move mcts();

	void printTree(int, int, int);
};


#endif
