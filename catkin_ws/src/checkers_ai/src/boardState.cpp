// CPP File for Board State Class

#include "boardState.h"



void  BoardState::makeID() {
	//converts uints to hex
	uint uintArry[3] = { currPlayerBoard, oppntPlayerBoard, bothIsKingBoard };
	char hexChars[17] = "0123456789ABCDEF";
	uint mask = 15;
	for (int i = 0; i < 3; i++) {
		//for each uint
		uint binVal = uintArry[i];
		for (int j = 0; j < 8; j++) {
			//for each set of 4 bits in uint from row 0 to 7 (right to left)
			//indexing  (7 - j) + i * 8  char in id
			uint hexNum = mask & binVal;
			id[7 - j + (i * 8)] = hexChars[hexNum];

			//should chop off bottom 4 bits
			binVal = binVal >> 4;
		}
	}
	//now add moveDir
	if (moveDir == 1) {
		id[24] = '1';
	}
	else {
		id[24] = '0';
	}

	//std::cout << "ID: " << id << std::endl;

	return;
}


bool BoardState::isOpen(Pt pt) {
	if ((pt.mask & currPlayerBoard) != 0) {
		return False;
	}
	if ((pt.mask & oppntPlayerBoard) != 0) {
		return False;
	}
	return True;
}
bool BoardState::isOpen(uint maskIn) {
	if ((maskIn & currPlayerBoard) != 0) {
		return False;
	}
	if ((maskIn & oppntPlayerBoard) != 0) {
		return False;
	}
	return True;
}
bool BoardState::isOpen(int sqrNum) {
	uint mask = makeMask(sqrNum);
	if ((mask & currPlayerBoard) != 0) {
		return False;
	}
	if ((mask & oppntPlayerBoard) != 0) {
		return False;
	}
	return True;
}

void BoardState::getSurSqrs(Pt pt, ArryPt& sqrs, bool isKing) {
	//is king is needed for multiple jumps with king piece
	//don't worry about checking if pt is valid
	//***NOTE: RETURNS ALL SQUARES WHETHER OPEN OR NOT
	//even or odd row
	//one side for edge
	sqrs.clear();
	int rowNum = 0;
	int colNum = 0;
	sqrNumToRowCol(pt.sqrNum, rowNum, colNum);

	if ((rowNum % 2) == 0) {
		//even row
		if (moveDir == 1) {
			//normal foward and back
			uint sqrMask = pt.mask;
			//forward right
			sqrMask = sqrMask << 4;
			Pt frPt(sqrMask);
			sqrs.append(frPt);
			//forward left
			if (colNum != 7) {
				//not edge sqr
				sqrMask = sqrMask << 1;
				Pt flPt(sqrMask);
				sqrs.append(flPt);
			}
		}
		else if (rowNum > 0) {
			//flip forward and back
			uint sqrMask = pt.mask;
			//forward left
			if (colNum != 7) {
				//not edge sqr
				sqrMask = sqrMask >> 3;
				Pt flPt(sqrMask);
				sqrs.append(flPt);
			}
			//forward right
			sqrMask = pt.mask >> 4;
			Pt frPt(sqrMask);
			sqrs.append(frPt);
		}
	}
	else {
		//odd row
		if ((moveDir == 1) && (rowNum < 7)) {
			//normal foward and back
			uint sqrMask = pt.mask;
			//forward right
			if (colNum != 0) {
				//not edge sqr
				sqrMask = sqrMask << 3;
				Pt frPt(sqrMask);
				sqrs.append(frPt);
			}
			//forward left
			sqrMask = pt.mask << 4;
			Pt flPt(sqrMask);
			sqrs.append(flPt);
		}
		else if (moveDir == -1) {
			//flip forward and back
			uint sqrMask = pt.mask;
			//forward left
			sqrMask = sqrMask >> 4;
			Pt flPt(sqrMask);
			sqrs.append(flPt);
			//forward right
			if (colNum != 0) {
				//not edge sqr
				sqrMask = sqrMask >> 1;
				Pt frPt(sqrMask);
				sqrs.append(frPt);
			}
		}
	}

	//backwards squares if king
	if (isKing == False) {
		//pawn
		return;
	}
	//king
	if ((moveDir == 1) && (rowNum == 0)) {
		return;
	}
	if ((moveDir == -1) && (rowNum == 7)) {
		return;
	}

	if ((rowNum % 2) == 0) {
		//even row
		if (moveDir == 1) {
			//normal back
			uint sqrMask = pt.mask;
			//back left
			if (colNum != 7) {
				//not edge sqr
				sqrMask = sqrMask >> 3;
				Pt blPt(sqrMask);
				sqrs.append(blPt);
			}
			//back right
			sqrMask = pt.mask >> 4;
			Pt brPt(sqrMask);
			sqrs.append(brPt);
		}
		else {
			//flipped back
			uint sqrMask = pt.mask;
			//back right
			sqrMask = sqrMask << 4;
			Pt brPt(sqrMask);
			sqrs.append(brPt);
			//back left
			if (colNum != 7) {
				//not edge sqr
				sqrMask = sqrMask << 1;
				Pt blPt(sqrMask);
				sqrs.append(blPt);
			}

		}
	}
	else {
		//odd row
		if (moveDir == 1) {
			//normal back
			uint sqrMask = pt.mask;
			//back left
			sqrMask = sqrMask >> 4;
			Pt blPt(sqrMask);
			sqrs.append(blPt);
			//back right
			if (colNum != 0) {
				//not edge sqr
				sqrMask = sqrMask >> 1;
				Pt brPt(sqrMask);
				sqrs.append(brPt);
			}
		}
		else {
			//flipped back
			uint sqrMask = pt.mask;
			//back right
			if (colNum != 0) {
				//not edge sqr
				sqrMask = sqrMask << 3;
				Pt brPt(sqrMask);
				sqrs.append(brPt);
			}
			//back left
			sqrMask = pt.mask << 4;
			Pt blPt(sqrMask);
			sqrs.append(blPt);
		}
	}

	return;
}

bool BoardState::validEndPt(Pt origStartPt, Pt endPt) {
	//not checking for correct sqrs for piece or jumping rules
	//only checking for sqr on board and empty squares (unless startPt)
	//should only be moving for current player

	//check endPt
	if ((endPt.sqrNum < 0) || (endPt.sqrNum > 31)) {
		return False;
	}
	//must have a piece at endPt
	if ((!isOpen(endPt)) && (endPt != origStartPt)) {
		return False;
	}

	return True;
}
bool BoardState::isOppnt(Pt pt) {
	if ((pt.mask & oppntPlayerBoard) > 0) {
		return True;
	}
	return False;
}
bool BoardState::isOppnt(int sqrNum) {
	Pt tempPt(sqrNum);
	if ((tempPt.mask & oppntPlayerBoard) > 0) {
		return True;
	}
	return False;
}
bool BoardState::isOppnt(uint maskIn) {
	if ((maskIn & oppntPlayerBoard) > 0) {
		return True;
	}
	return False;
}
bool BoardState::isCurrPlayer(int sqrNum) {
	Pt tempPt(sqrNum);
	if ((tempPt.mask & currPlayerBoard) > 0) {
		return True;
	}
	return False;
}
bool BoardState::isKing(Pt pt) {
	if ((pt.mask & bothIsKingBoard) == 0) {
		return False;
	}
	return True;
}

bool BoardState::singleJump(Pt origStartPt, Pt startPt, Pt skipPt, Pt& endPtIn) {
	//check if skipPt is occupied by same color
	if ((skipPt.mask & oppntPlayerBoard) == 0) {
		return False;
	}
	//get jump direction
	//row and col of startPt and skipPt
	int startRow = 0;
	int startCol = 0;
	sqrNumToRowCol(startPt.sqrNum, startRow, startCol);
	int skipRow = 0;
	int skipCol = 0;
	sqrNumToRowCol(skipPt.sqrNum, skipRow, skipCol);
	int colDiff = skipCol - startCol;
	int rowDiff = skipRow - startRow;
	int endRow = skipRow + rowDiff;
	int endCol = skipCol + colDiff;
	//check if next square on diagonal is invalid/not open
	int endSqrNum = rowColToSqrNum(endRow, endCol);
	Pt endPt(endSqrNum);
	if (validEndPt(origStartPt, endPt) == False) {
		return False;
	}
	//can make first jump
	endPtIn = endPt;
	return True;
}

bool BoardState::nextJumps(Move& partMove, ArryPt& endPts) {
	//can't just end after finishing circle in case then taking diff path
	Pt origStartPt = partMove.startPt;
	//since it's the second jump 
	Pt currPt = partMove.endPts.last();
	Pt prevStartPt;
	if (partMove.endPts.getSize() > 1) {
		prevStartPt = partMove.endPts[partMove.endPts.getSize() - 2];
	}
	else {
		prevStartPt = origStartPt;
	}

	ArryPt surSqrs;
	getSurSqrs(currPt, surSqrs, (origStartPt.mask & bothIsKingBoard));
	int prevStartRow = 0;
	int prevStartCol = 0;
	sqrNumToRowCol(prevStartPt.sqrNum, prevStartRow, prevStartCol);
	int currRow = 0;
	int currCol = 0;
	sqrNumToRowCol(currPt.sqrNum, currRow, currCol);
	int rowDir = (currRow - prevStartRow) / 2;
	int colDir = (currCol - prevStartCol) / 2;
	Pt prevSkipPt(prevStartRow + rowDir, prevStartCol + colDir);
	bool jumpsFound = False;
	for (int i = 0; i < surSqrs.getSize(); i++) {
		//first check that surSqrs[i] was not between currPt and prevStartPt
		if ((surSqrs[i] != prevSkipPt) && (isOppnt(surSqrs[i]) == True)) {
			//need jump result
			Pt tempEndPt;
			bool canJump = singleJump(origStartPt, currPt, surSqrs[i], tempEndPt);
			if (canJump == True) {
				//now make sure that both jump startPt and endPt are not both in list or startPt
				bool currPtRep = False;
				bool endPtRep = False;
				if (currPt == origStartPt) {
					currPtRep = True;
				}
				if (tempEndPt == origStartPt) {
					endPtRep = True;
				}
				//now itterate through list of endpts checking for currPt and tempEndPt
				for (int j = 0; j < partMove.endPts.getSize(); j++) {
					if (partMove.endPts[j] == currPt) {
						currPtRep = True;
					}
					if (partMove.endPts[j] == tempEndPt) {
						endPtRep = True;
					}
				}
				if ((currPtRep == False) || (endPtRep == False)) {
					jumpsFound = canJump;
					endPts.append(tempEndPt);
				}
			}

			//singleJump automatically appends allowed endPt to endPts array
		}

	}

	return jumpsFound;
}

bool BoardState::howCanMove(Pt startPt, ArryMove& moves) {
	//bool is for jumpFound
	//only for one piece/startPt
	//get surrounding sqrs
	ArryPt surSqrs;
	getSurSqrs(startPt, surSqrs, (startPt.mask & bothIsKingBoard));
	//split into open and not open sqrs pieces
	ArryPt endPtsNoJump;
	ArryPt skipPtsJump;
	for (int i = 0; i < surSqrs.getSize(); i++) {
		if ((surSqrs[i].sqrNum < 0) || (surSqrs[i].sqrNum > 31)) {
			printf("**Bad sqrNum: %i\n", surSqrs[i].sqrNum);
			printf("**ERROR HERE: boardState.cpp 1**");
		}
		if (isOpen(surSqrs[i]) == True) {
			endPtsNoJump.append(surSqrs[i]);
		}
		else {
			skipPtsJump.append(surSqrs[i]);
		}
	}
	//start jumping combo loop
	bool jumpFound = False;
	QueueMove queue;
	for (int i = 0; i < skipPtsJump.getSize(); i++) {
		Pt jumpEndPt;
		if (singleJump(startPt, startPt, skipPtsJump[i], jumpEndPt) == True) {
			jumpFound = True;
			if ((jumpEndPt.sqrNum < 0) || (jumpEndPt.sqrNum > 31)) {
				printf("**Bad sqrNum: %i\n", jumpEndPt.sqrNum);
				printf("**ERROR HERE: boardState.cpp 2**");
			}
			Move tempMove(startPt, jumpEndPt);
			queue.enqueue(tempMove);
		}
	}

	while (queue.isEmpty() == False) {
		Move currMove = queue.dequeue();
		// if (currMove.endPts.getSize() > 4) {
		// 	printf("\n*** ERROR HERE: boardState.cpp 2.1 ***\n");
		// 	printf("startPt: \n");
		// 	startPt.print();
		// 	printf("\nBoard State: \n");
		// 	print();
		// 	printf("\n*** ERROR HERE: boardState.cpp 2.2 ***\n");
		// }
		Pt prevStartPt;
		Pt currPt;
		ArryPt newEndPts;
		currMove.prevStep(prevStartPt, currPt);
		//bool stillJumping = nextJumps(startPt, prevStartPt, currPt, newEndPts);
		bool stillJumping = nextJumps(currMove, newEndPts);
		if (stillJumping == False) {
			//end of jumping move
			if ((currMove.endPts[currMove.endPts.getSize() - 1].sqrNum < 0) || (currMove.endPts[currMove.endPts.getSize() - 1].sqrNum > 31)) {
				printf("**Bad sqrNum: %i\n", currMove.endPts[currMove.endPts.getSize() - 1].sqrNum);
				printf("**ERROR HERE: boardState.cpp 3**");
			}
			moves.append(currMove);
		}
		else {
			//still jumping
			//go through list of new endPoints and make new moves
			for (int i = 0; i < newEndPts.getSize(); i++) {
				Move newMove = currMove;
				newMove.addEndPt(newEndPts[i]);
				if ((newEndPts[i].sqrNum < 0) || (newEndPts[i].sqrNum > 31)) {
					printf("**Bad sqrNum: %i\n", newEndPts[i].sqrNum);
					printf("**ERROR HERE: boardState.cpp 4**");
				}
				queue.enqueue(newMove);
			}
		}
	}
	//check if any jump endpts were found
	if (jumpFound == False) {
		//make moves from endPtsNoJump and append to endPts
		for (int i = 0; i < endPtsNoJump.getSize(); i++) {
			//endPtsNoJump[i].print();
			Move newMove(startPt, endPtsNoJump[i]);
			//endPtsNoJump[i].print();
			//newMove.print();
			moves.append(newMove);
			//newMove.print();
			//moves[0].print();
		}
		/*for (int i = 0; i < moves.getSize(); i++) {
			moves[i].print();
		}*/
	}

	return jumpFound;
}

BoardState::BoardState() {
	currPlayerBoard = 4095;
	oppntPlayerBoard = 4293918720;
	bothIsKingBoard = 0;
	moveDir = 1;
	//FIXME: FIX ID
	makeID();
}
BoardState::BoardState(uint currPlayerBoardIn, uint oppntPlayerBoardIn, uint bothIsKingBoardIn, int moveDirIn) {
	currPlayerBoard = currPlayerBoardIn;
	oppntPlayerBoard = oppntPlayerBoardIn;
	bothIsKingBoard = bothIsKingBoardIn;
	moveDir = moveDirIn;
	//FIXME: FIX ID
	makeID();
}
BoardState::BoardState(const BoardState& rhs) {
	currPlayerBoard = rhs.currPlayerBoard;
	oppntPlayerBoard = rhs.oppntPlayerBoard;
	bothIsKingBoard = rhs.bothIsKingBoard;
	moveDir = rhs.moveDir;
	//FIXME: FIX ID
	id = rhs.id;
}
BoardState& BoardState::operator=(const BoardState& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		currPlayerBoard = rhs.currPlayerBoard;
		oppntPlayerBoard = rhs.oppntPlayerBoard;
		bothIsKingBoard = rhs.bothIsKingBoard;
		moveDir = rhs.moveDir;
		//FIXME: FIX ID
		id = rhs.id;
	}
	return *this;
}
bool BoardState::operator==(BoardState& rhs) {
	// or could change to just comparing ids
	// FIXME: DETERMINE WHETHER OR NOT MOVEDIR MATTERS FOR DECIDING IF BOARDSTATES ARE EQUAL
	if (rhs.currPlayerBoard != currPlayerBoard) {
		return False;
	}
	if (rhs.oppntPlayerBoard != oppntPlayerBoard) {
		return False;
	}
	if (rhs.bothIsKingBoard != bothIsKingBoard) {
		return False;
	}
	// FIXME: CHANGE WOULD BE HERE
	if (rhs.moveDir != moveDir) {
		return False;
	}

	return True;
}
bool BoardState::operator!=(BoardState& rhs) {
	// or could change to just comparing ids
	// FIXME: DETERMINE WHETHER OR NOT MOVEDIR MATTERS FOR DECIDING IF BOARDSTATES ARE EQUAL
	if (rhs.currPlayerBoard != currPlayerBoard) {
		return True;
	}
	if (rhs.oppntPlayerBoard != oppntPlayerBoard) {
		return True;
	}
	if (rhs.bothIsKingBoard != bothIsKingBoard) {
		return True;
	}
	// FIXME: CHANGE WOULD BE HERE
	if (rhs.moveDir != moveDir) {
		return True;
	}

	return False;
}

void BoardState::legalMoves(ArryMove& moves) {
	//current player is always moving
	//find all of current player's pieces (pts)
	//check sqr numbers 0 - 31
	ArryPt piecePts;
	ArryMove noJumpMoves;
	for (int i = 0; i <= 31; i++) {
		if (isCurrPlayer(i) == True) {
			Pt piecePt(i);
			piecePts.append(piecePt);
		}
	}
	//for each piece call howCanMove with checking for jumps found or not
	bool jumpFound = False;
	for (int i = 0; i < piecePts.getSize(); i++) {
		Pt currStartPt = piecePts[i];
		ArryMove possibleMoves;
		if (howCanMove(currStartPt, possibleMoves) == False) {
			//no jumps found
			if (jumpFound == False) {
				for (int j = 0; j < possibleMoves.getSize(); j++) {
					if ((possibleMoves[j].endPts[0].sqrNum < 0) || (possibleMoves[j].endPts[0].sqrNum > 31)) {
						printf("**Bad sqrNum: %i\n", possibleMoves[j].endPts[0].sqrNum);
						printf("**ERROR HERE: boardState.cpp 5**\n");
					}
					noJumpMoves.append(possibleMoves[j]);
				}
			}
			//else do nothing because must take jump move
		}
		else {
			//jumps found
			jumpFound = True;
			for (int j = 0; j < possibleMoves.getSize(); j++) {
				if ((possibleMoves[j].endPts[0].sqrNum < 0) || (possibleMoves[j].endPts[0].sqrNum > 31)) {
					printf("**Bad sqrNum: %i\n", possibleMoves[j].endPts[0].sqrNum);
					printf("**ERROR HERE: boardState.cpp 6**\n");
				}
				moves.append(possibleMoves[j]);
			}
		}
	}
	if (jumpFound == False) {
		for (int i = 0; i < noJumpMoves.getSize(); i++) {
			moves.append(noJumpMoves[i]);
		}
	}

	//printf("Returning from Legal Moves...\n");

	return;

}
void BoardState::makeMove(Move move, bool& makeKing) {
	//just changes the current BoardState instance instead of creating another one
	//move pieces and handle captures (curr, oppnt, and king)
	makeKing = false;
	bool isJump = False;
	int startRow = 0;
	int startCol = 0;
	int endRow = 0;
	int endCol = 0;
	if (move.moveNum == 1) {
		//check for jump (set isJump)
		sqrNumToRowCol(move.startPt.sqrNum, startRow, startCol);
		sqrNumToRowCol(move.endPts[0].sqrNum, endRow, endCol);
		if (abs(endRow - startRow) > 1) {
			//jump
			isJump = True;
		}
		//else no jump
	}
	else {
		//definitely jump
		isJump = True;
	}

	if (isJump == False) {
		//don't worry about captures and only move piece on currPlayerBoard
		if ((currPlayerBoard & move.startPt.mask) == 0) {
			printf("**ERROR HERE: boardState.cpp 7**\n");
		}
		currPlayerBoard = currPlayerBoard - move.startPt.mask;
		if ((currPlayerBoard & move.endPts[0].mask) > 0) {
			printf("**ERROR HERE: boardState.cpp 8**\n");
		}
		currPlayerBoard = currPlayerBoard + move.endPts[0].mask;
		if (isKing(move.startPt)) {
			if ((bothIsKingBoard & move.startPt.mask) == 0) {
				printf("**ERROR HERE: boardState.cpp 9**\n");
			}
			bothIsKingBoard = bothIsKingBoard - move.startPt.mask;
			if ((bothIsKingBoard & move.endPts[0].mask) > 0) {
				printf("**ERROR HERE: boardState.cpp 10**\n");
			}
			bothIsKingBoard = bothIsKingBoard + move.endPts[0].mask;
		}
		//check for king
		if ((endRow == 7) && (moveDir == 1)) {
			//make king if piece is pawn
			if ((bothIsKingBoard & move.endPts[0].mask) == 0) {
				if ((bothIsKingBoard & move.endPts[0].mask) > 0) {
					printf("**ERROR HERE: boardState.cpp 11**\n");
				}
				bothIsKingBoard = bothIsKingBoard + move.endPts[0].mask;
				makeKing = true;
			}

		}
		else if ((endRow == 0) && (moveDir == -1)) {
			//make king if piece is pawn
			if ((bothIsKingBoard & move.endPts[0].mask) == 0) {
				if ((bothIsKingBoard & move.endPts[0].mask) > 0) {
					printf("**ERROR HERE: boardState.cpp 12**\n");
				}
				bothIsKingBoard = bothIsKingBoard + move.endPts[0].mask;
				makeKing = true;
			}
		}
	}
	else {
		//first subtract moving piece
		if ((currPlayerBoard & move.startPt.mask) == 0) {
			move.print();
			printf("**ERROR HERE: boardState.cpp 13**\n");
		}
		currPlayerBoard = currPlayerBoard - move.startPt.mask;
		int currRow = 0;
		int currCol = 0;
		sqrNumToRowCol(move.startPt.sqrNum, currRow, currCol);
		endRow = 0;
		endCol = 0;
		//for each endPt find and subtract captured piece from oppntPlayerBoard;
		for (int i = 0; i < move.endPts.getSize(); i++) {
			sqrNumToRowCol(move.endPts[i].sqrNum, endRow, endCol);
			int rowDir = (endRow - currRow) / 2;
			int colDir = (endCol - currCol) / 2;
			int captureRow = currRow + rowDir;
			int captureCol = currCol + colDir;
			int captureSqrNum = rowColToSqrNum(captureRow, captureCol);
			Pt capturePt(captureSqrNum);
			//removing capture pt
			if ((oppntPlayerBoard & capturePt.mask) == 0) {
				printf("**ERROR HERE: boardState.cpp 14**\n");
			}
			oppntPlayerBoard = oppntPlayerBoard - capturePt.mask;
			//check if captured piece is king
			if (isKing(capturePt)) {
				if ((bothIsKingBoard & capturePt.mask) == 0) {
					printf("**ERROR HERE: boardState.cpp 15**\n");
				}
				bothIsKingBoard = bothIsKingBoard - capturePt.mask;
			}
			currRow = endRow;
			currCol = endCol;
		}
		//add moving piece at end
		if ((currPlayerBoard & move.endPts.last().mask) > 0) {
			printf("**ERROR HERE: boardState.cpp 16**\n");
		}
		currPlayerBoard = currPlayerBoard + move.endPts.last().mask;
		if (isKing(move.startPt)) {
			if ((bothIsKingBoard & move.startPt.mask) == 0) {
				printf("**ERROR HERE: boardState.cpp 17**\n");
			}
			bothIsKingBoard = bothIsKingBoard - move.startPt.mask;
			if ((bothIsKingBoard & move.endPts.last().mask) > 0) {
				printf("**ERROR HERE: boardState.cpp 18**\n");
			}
			bothIsKingBoard = bothIsKingBoard + move.endPts.last().mask;
		}
		//check for king
		if ((endRow == 7) && (moveDir == 1)) {
			//make king if piece is pawn
			if ((bothIsKingBoard & move.endPts.last().mask) == 0) {
				if ((bothIsKingBoard & move.endPts.last().mask) > 0) {
					printf("**ERROR HERE: boardState.cpp 19**\n");
				}
				bothIsKingBoard = bothIsKingBoard + move.endPts.last().mask;
				makeKing = true;
			}

		}
		else if ((endRow == 0) && (moveDir == -1)) {
			//make king if piece is pawn
			if ((bothIsKingBoard & move.endPts.last().mask) == 0) {
				if ((bothIsKingBoard & move.endPts.last().mask) > 0) {
					printf("**ERROR HERE: boardState.cpp 20**\n");
				}
				bothIsKingBoard = bothIsKingBoard + move.endPts.last().mask;
				makeKing = true;
			}
		}
	}

	//swap currPlayerBoard with oppntPlayerBoard and flip moveDir
	uint tempCurrPlayerBoard = currPlayerBoard;
	currPlayerBoard = oppntPlayerBoard;
	oppntPlayerBoard = tempCurrPlayerBoard;
	moveDir = moveDir * -1;
	//make new ID
	makeID();

	return;
}

void BoardState::countPieces(int& numBlack, int& numRed) {
	//find all of current player's  and oppnt's pieces
	//check sqr numbers 0 - 31
	int numCurr = 0;
	int numOppnt = 0;
	for (int i = 0; i <= 31; i++) {
		if (isCurrPlayer(i) == True) {
			numCurr++;
		}
		if (isOppnt(i) == True) {
			numOppnt++;
		};
	}
	if (moveDir == 1) {
		numBlack = numCurr;
		numRed = numOppnt;
	}
	else {
		numBlack = numOppnt;
		numRed = numCurr;
	}

	return;
}

void BoardState::print() {
	//x X for black and o O for Red
	char currPawn = 'a';
	char currKing = 'a';
	char oppntPawn = 'a';
	char oppntKing = 'a';
	if (moveDir == 1) {
		currPawn = 'x';
		currKing = 'X';
		oppntPawn = 'o';
		oppntKing = 'O';
	}
	else {
		currPawn = 'o';
		currKing = 'O';
		oppntPawn = 'x';
		oppntKing = 'X';
	}
	//first mask
	uint mask = 1 << 31;
	//print top to bottom and left to right by row
	for (int i = 7; i >= 0; i--) {
		if ((i % 2) > 0) {
			//odd row
			for (int j = 0; j < 4; j++) {
				printf(" * ");
				//then print square
				if ((mask & currPlayerBoard) > 0) {
					//current player piece
					//check for king
					if ((mask & bothIsKingBoard) == 0) {
						//not king
						printf(" %c ", currPawn);
					}
					else {
						//king
						printf(" %c ", currKing);
					}
				}
				else if ((mask & oppntPlayerBoard) > 0) {
					//oppnt player piece
					//check for king
					if ((mask & bothIsKingBoard) == 0) {
						//not king
						printf(" %c ", oppntPawn);
					}
					else {
						//king
						printf(" %c ", oppntKing);
					}

				}
				else {
					//empty square
					printf(" _ ");
				}
				mask = mask >> 1;
			}
			printf("\n");
		}
		else {
			//even row
			for (int j = 0; j < 4; j++) {
				//then print square
				if ((mask & currPlayerBoard) > 0) {
					//current player piece
					//check for king
					if ((mask & bothIsKingBoard) == 0) {
						//not king
						printf(" %c ", currPawn);
					}
					else {
						//king
						printf(" %c ", currKing);
					}
				}
				else if ((mask & oppntPlayerBoard) > 0) {
					//oppnt player piece
					//check for king
					if ((mask & bothIsKingBoard) == 0) {
						//not king
						printf(" %c ", oppntPawn);
					}
					else {
						//king
						printf(" %c ", oppntKing);
					}

				}
				else {
					//empty square
					printf(" _ ");
				}
				printf(" * ");
				mask = mask >> 1;
			}
			printf("\n");
		}
		printf("\n");
	}


	//printf("moveDir: %i  id: %s\n", moveDir, id);
	printf("moveDir: %i\n", moveDir);

	return;
}
int BoardState::getMoveDir() {
	return moveDir;
}

ArryBoardState::ArryBoardState() {
	startingSize = STARTINGSIZEBOARDSTATE;
	sizeTotal = startingSize;
	size = 0;
	frontPtr = new BoardState[startingSize];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	return;
}
ArryBoardState::~ArryBoardState() {
	delete[] frontPtr;
	frontPtr = nullptr;
	endPtr = nullptr;
	nextPtr = nullptr;
}
ArryBoardState& ArryBoardState::operator=(const ArryBoardState& rhs) {
	//make sure it's not a self assignment
	if (this != &rhs) {
		//check if enough memory is allocated to copy rhs
		if (rhs.size > sizeTotal) {
			//must delete and make new arry (resize)
			delete[] frontPtr;
			frontPtr = nullptr;
			frontPtr = new BoardState[rhs.sizeTotal];
			sizeTotal = rhs.sizeTotal;
			endPtr = frontPtr + sizeTotal;
		}
		nextPtr = frontPtr;
		BoardState* rhsPtr = rhs.frontPtr;
		for (int i = 0; i < rhs.size; i++) {
			*nextPtr = *rhsPtr;
			nextPtr++;
			rhsPtr++;
		}
		size = rhs.size;
	}
	return *this;
}
ArryBoardState::ArryBoardState(const ArryBoardState& rhs) {
	//copy constructor (does same thing as assignment)
	startingSize = STARTINGSIZEBOARDSTATE;
	sizeTotal = rhs.sizeTotal;
	size = rhs.size;
	frontPtr = new BoardState[sizeTotal];
	//one past the last
	endPtr = frontPtr + sizeTotal;
	nextPtr = frontPtr;
	BoardState* rhsPtr = rhs.frontPtr;
	for (int i = 0; i < rhs.size; i++) {
		*nextPtr = *rhsPtr;
		nextPtr++;
		rhsPtr++;
	}
}
void ArryBoardState::append(const BoardState& nodeIn) {
	//check if there is room
	//if not make new array and copy over
	//add and increment nextPtr and size
	if (nextPtr == endPtr) {
		//not enough room
		BoardState* tempStoragePtr = new BoardState[sizeTotal + startingSize];
		sizeTotal = sizeTotal + startingSize;
		for (int i = 0; i < size; i++) {
			tempStoragePtr[i] = frontPtr[i];
		}
		delete[] frontPtr;
		frontPtr = nullptr;
		frontPtr = tempStoragePtr;
		endPtr = frontPtr + sizeTotal;
		nextPtr = frontPtr + size;
	}
	*nextPtr = nodeIn;
	nextPtr++;
	size++;

	return;
}
int ArryBoardState::getSize() {
	return size;
}
BoardState& ArryBoardState::operator[](int i) {
	return frontPtr[i];
}