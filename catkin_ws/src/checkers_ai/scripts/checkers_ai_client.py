#!/usr/bin/env python3

#COPY: ***********************************************
from __future__ import print_function

import sys
import rospy
from checkers_ai.srv import *
from contToAI import contToAIfunc, checkers_client, usage
#*****************************************************


if __name__ == "__main__":
    if len(sys.argv) == 5:
        currPlayer = int(sys.argv[1])
        oppntPlayer = int(sys.argv[2])
        bothIsKing = int(sys.argv[3])
        moveDir = int(sys.argv[4])
    else:
        print("I am lost")
        print(usage())
        # sys.exit(1)
    contArry = [[0, 2, 0, 2, 0, 2, 0, 2],
                [2, 0, 2, 0, 2, 0, 2, 0],
                [0, 0, 0, 2, 0, 2, 0, 2],
                [0, 0, 2, 0, 0, 0, 0, 0],
                [0, 0, 0, 1, 0, 0, 0, 0],
                [1, 0, 0, 0, 1, 0, 1, 0],
                [0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0]]
    #COPY: ***********************************************
    tup = contToAIfunc(contArry)
    currPlayer = tup[0]
    oppntPlayer = tup[1]
    bothIsKing = tup[2]
    moveDir = tup[3]
    print("Request: %s %s %s %s"%(currPlayer, oppntPlayer, bothIsKing, moveDir))
    print("\nResponse:")
    resp = checkers_client(currPlayer, oppntPlayer, bothIsKing, moveDir)
    startPt = resp.startPt
    endPts = list(resp.endpts)
    captured = list(resp.captured)
    #if makeKing is not -1 then you change the piece at makeKing to a king
    makeKing = resp.makeKing
    #*****************************************************
    print("Client Exiting...")