#!/usr/bin/env python3
from __future__ import print_function

import sys
import rospy
from checkers_ai.srv import *

def checkers_client(currPlayer, oppntPlayer, bothIsKing, moveDir):
    rospy.wait_for_service('checkers_ai')
    try:
        aiServ = rospy.ServiceProxy('checkers_ai', checkers_ai)
        resp = aiServ(currPlayer, oppntPlayer, bothIsKing, moveDir)
        print("startPt: %s"%resp.startPt)
        print("endpts: ")
        print(list(resp.endpts))
        print("captured: ")
        print(list(resp.captured))
        print("makeKing: %s"%resp.makeKing)
        print("\n")
        return resp
    except rospy.ServiceException as e:
        print("Service call to checkers_ai failed: %s"%e)

def usage():
    return "%s [currPlayer oppntPlayer bothIsKing moveDir]"%sys.argv[0]


def contToAIfunc(contArry):
    #controls array to AI request
    #returns tuple (currPlayer, oppntPlayer, bothIsKing, moveDir)
    #current player is the AI (moveDir = -1)
    #0 - 63 where 0(C) = 0AI) and 62(C) = 31(AI)
    #the user is 2/4 (red)
    #the ai is 1/3 (blue)
    rowNum = 8
    colNum = 4 #shift for whether or not even or odd row
    #colNum is 2*j+1 if even row
    #colNum is 2*j if odd row
    currPlayer = 0
    oppntPlayer = 0
    bothIsKing = 0
    #it is the AI's turn
    moveDir = -1
    for i in range(rowNum):
        row = i
        for j in range(colNum):
            col = 0
            if ((row % 2) == 0):
                col = 2 * j + 1
            else:
                col = 2 * j
            sqrNum = 4 * row + j
            #contArry[row][col]
            #mask: 1<<sqrNum
            if (contArry[row][col] != 0):
                if ((contArry[row][col] % 2) == 0):
                    #user/red (opponent)
                    oppntPlayer = oppntPlayer | (1 << sqrNum)
                    if (contArry[row][col] == 4):
                        #king piece
                        bothIsKing = bothIsKing | (1 << sqrNum)
                else:
                    #ai/blue (current player)
                    currPlayer = currPlayer | (1 << sqrNum)
                    if (contArry[row][col] == 3):
                        #king piece
                        bothIsKing = bothIsKing | (1 << sqrNum)
    #now we should be good to go
    return (currPlayer, oppntPlayer, bothIsKing, moveDir)



# def aiToCont():
#     #ai response to controls instructions
