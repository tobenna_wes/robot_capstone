// Auto-generated. Do not edit!

// (in-package checkers_ai.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class checkers_aiRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.currPlayer = null;
      this.oppntPlayer = null;
      this.bothIsKing = null;
      this.moveDir = null;
    }
    else {
      if (initObj.hasOwnProperty('currPlayer')) {
        this.currPlayer = initObj.currPlayer
      }
      else {
        this.currPlayer = 0;
      }
      if (initObj.hasOwnProperty('oppntPlayer')) {
        this.oppntPlayer = initObj.oppntPlayer
      }
      else {
        this.oppntPlayer = 0;
      }
      if (initObj.hasOwnProperty('bothIsKing')) {
        this.bothIsKing = initObj.bothIsKing
      }
      else {
        this.bothIsKing = 0;
      }
      if (initObj.hasOwnProperty('moveDir')) {
        this.moveDir = initObj.moveDir
      }
      else {
        this.moveDir = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type checkers_aiRequest
    // Serialize message field [currPlayer]
    bufferOffset = _serializer.uint32(obj.currPlayer, buffer, bufferOffset);
    // Serialize message field [oppntPlayer]
    bufferOffset = _serializer.uint32(obj.oppntPlayer, buffer, bufferOffset);
    // Serialize message field [bothIsKing]
    bufferOffset = _serializer.uint32(obj.bothIsKing, buffer, bufferOffset);
    // Serialize message field [moveDir]
    bufferOffset = _serializer.int8(obj.moveDir, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type checkers_aiRequest
    let len;
    let data = new checkers_aiRequest(null);
    // Deserialize message field [currPlayer]
    data.currPlayer = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [oppntPlayer]
    data.oppntPlayer = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [bothIsKing]
    data.bothIsKing = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [moveDir]
    data.moveDir = _deserializer.int8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 13;
  }

  static datatype() {
    // Returns string type for a service object
    return 'checkers_ai/checkers_aiRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f13d29d1796b7f4361a0b9fb988bd706';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint32 currPlayer
    uint32 oppntPlayer
    uint32 bothIsKing
    int8 moveDir
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new checkers_aiRequest(null);
    if (msg.currPlayer !== undefined) {
      resolved.currPlayer = msg.currPlayer;
    }
    else {
      resolved.currPlayer = 0
    }

    if (msg.oppntPlayer !== undefined) {
      resolved.oppntPlayer = msg.oppntPlayer;
    }
    else {
      resolved.oppntPlayer = 0
    }

    if (msg.bothIsKing !== undefined) {
      resolved.bothIsKing = msg.bothIsKing;
    }
    else {
      resolved.bothIsKing = 0
    }

    if (msg.moveDir !== undefined) {
      resolved.moveDir = msg.moveDir;
    }
    else {
      resolved.moveDir = 0
    }

    return resolved;
    }
};

class checkers_aiResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.startPt = null;
      this.endpts = null;
      this.captured = null;
      this.makeKing = null;
    }
    else {
      if (initObj.hasOwnProperty('startPt')) {
        this.startPt = initObj.startPt
      }
      else {
        this.startPt = 0;
      }
      if (initObj.hasOwnProperty('endpts')) {
        this.endpts = initObj.endpts
      }
      else {
        this.endpts = [];
      }
      if (initObj.hasOwnProperty('captured')) {
        this.captured = initObj.captured
      }
      else {
        this.captured = [];
      }
      if (initObj.hasOwnProperty('makeKing')) {
        this.makeKing = initObj.makeKing
      }
      else {
        this.makeKing = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type checkers_aiResponse
    // Serialize message field [startPt]
    bufferOffset = _serializer.int32(obj.startPt, buffer, bufferOffset);
    // Serialize message field [endpts]
    bufferOffset = _arraySerializer.int32(obj.endpts, buffer, bufferOffset, null);
    // Serialize message field [captured]
    bufferOffset = _arraySerializer.int32(obj.captured, buffer, bufferOffset, null);
    // Serialize message field [makeKing]
    bufferOffset = _serializer.int32(obj.makeKing, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type checkers_aiResponse
    let len;
    let data = new checkers_aiResponse(null);
    // Deserialize message field [startPt]
    data.startPt = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [endpts]
    data.endpts = _arrayDeserializer.int32(buffer, bufferOffset, null)
    // Deserialize message field [captured]
    data.captured = _arrayDeserializer.int32(buffer, bufferOffset, null)
    // Deserialize message field [makeKing]
    data.makeKing = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 4 * object.endpts.length;
    length += 4 * object.captured.length;
    return length + 16;
  }

  static datatype() {
    // Returns string type for a service object
    return 'checkers_ai/checkers_aiResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '0721883ac3481a7471fedded74c8ec84';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 startPt
    int32[] endpts
    int32[] captured
    int32 makeKing
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new checkers_aiResponse(null);
    if (msg.startPt !== undefined) {
      resolved.startPt = msg.startPt;
    }
    else {
      resolved.startPt = 0
    }

    if (msg.endpts !== undefined) {
      resolved.endpts = msg.endpts;
    }
    else {
      resolved.endpts = []
    }

    if (msg.captured !== undefined) {
      resolved.captured = msg.captured;
    }
    else {
      resolved.captured = []
    }

    if (msg.makeKing !== undefined) {
      resolved.makeKing = msg.makeKing;
    }
    else {
      resolved.makeKing = 0
    }

    return resolved;
    }
};

module.exports = {
  Request: checkers_aiRequest,
  Response: checkers_aiResponse,
  md5sum() { return 'd5a3501b057d6c8e8efeb3c145b335d5'; },
  datatype() { return 'checkers_ai/checkers_ai'; }
};
