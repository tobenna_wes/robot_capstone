
"use strict";

let DynamixelLoadInfo = require('./DynamixelLoadInfo.js');
let XM = require('./XM.js');
let PROExt = require('./PROExt.js');
let XH = require('./XH.js');
let XL320 = require('./XL320.js');
let MX = require('./MX.js');
let MX2Ext = require('./MX2Ext.js');
let PRO = require('./PRO.js');
let DynamixelState = require('./DynamixelState.js');
let XL = require('./XL.js');
let XMExt = require('./XMExt.js');
let RX = require('./RX.js');
let AX = require('./AX.js');
let EX = require('./EX.js');
let DynamixelInfo = require('./DynamixelInfo.js');
let MXExt = require('./MXExt.js');
let MX2 = require('./MX2.js');
let DynamixelStateList = require('./DynamixelStateList.js');

module.exports = {
  DynamixelLoadInfo: DynamixelLoadInfo,
  XM: XM,
  PROExt: PROExt,
  XH: XH,
  XL320: XL320,
  MX: MX,
  MX2Ext: MX2Ext,
  PRO: PRO,
  DynamixelState: DynamixelState,
  XL: XL,
  XMExt: XMExt,
  RX: RX,
  AX: AX,
  EX: EX,
  DynamixelInfo: DynamixelInfo,
  MXExt: MXExt,
  MX2: MX2,
  DynamixelStateList: DynamixelStateList,
};
