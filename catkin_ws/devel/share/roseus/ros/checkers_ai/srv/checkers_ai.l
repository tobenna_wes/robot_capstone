;; Auto-generated. Do not edit!


(when (boundp 'checkers_ai::checkers_ai)
  (if (not (find-package "CHECKERS_AI"))
    (make-package "CHECKERS_AI"))
  (shadow 'checkers_ai (find-package "CHECKERS_AI")))
(unless (find-package "CHECKERS_AI::CHECKERS_AI")
  (make-package "CHECKERS_AI::CHECKERS_AI"))
(unless (find-package "CHECKERS_AI::CHECKERS_AIREQUEST")
  (make-package "CHECKERS_AI::CHECKERS_AIREQUEST"))
(unless (find-package "CHECKERS_AI::CHECKERS_AIRESPONSE")
  (make-package "CHECKERS_AI::CHECKERS_AIRESPONSE"))

(in-package "ROS")





(defclass checkers_ai::checkers_aiRequest
  :super ros::object
  :slots (_currPlayer _oppntPlayer _bothIsKing _moveDir ))

(defmethod checkers_ai::checkers_aiRequest
  (:init
   (&key
    ((:currPlayer __currPlayer) 0)
    ((:oppntPlayer __oppntPlayer) 0)
    ((:bothIsKing __bothIsKing) 0)
    ((:moveDir __moveDir) 0)
    )
   (send-super :init)
   (setq _currPlayer (round __currPlayer))
   (setq _oppntPlayer (round __oppntPlayer))
   (setq _bothIsKing (round __bothIsKing))
   (setq _moveDir (round __moveDir))
   self)
  (:currPlayer
   (&optional __currPlayer)
   (if __currPlayer (setq _currPlayer __currPlayer)) _currPlayer)
  (:oppntPlayer
   (&optional __oppntPlayer)
   (if __oppntPlayer (setq _oppntPlayer __oppntPlayer)) _oppntPlayer)
  (:bothIsKing
   (&optional __bothIsKing)
   (if __bothIsKing (setq _bothIsKing __bothIsKing)) _bothIsKing)
  (:moveDir
   (&optional __moveDir)
   (if __moveDir (setq _moveDir __moveDir)) _moveDir)
  (:serialization-length
   ()
   (+
    ;; uint32 _currPlayer
    4
    ;; uint32 _oppntPlayer
    4
    ;; uint32 _bothIsKing
    4
    ;; int8 _moveDir
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint32 _currPlayer
       (write-long _currPlayer s)
     ;; uint32 _oppntPlayer
       (write-long _oppntPlayer s)
     ;; uint32 _bothIsKing
       (write-long _bothIsKing s)
     ;; int8 _moveDir
       (write-byte _moveDir s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint32 _currPlayer
     (setq _currPlayer (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _oppntPlayer
     (setq _oppntPlayer (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _bothIsKing
     (setq _bothIsKing (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int8 _moveDir
     (setq _moveDir (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _moveDir 127) (setq _moveDir (- _moveDir 256)))
   ;;
   self)
  )

(defclass checkers_ai::checkers_aiResponse
  :super ros::object
  :slots (_startPt _endpts _captured _makeKing ))

(defmethod checkers_ai::checkers_aiResponse
  (:init
   (&key
    ((:startPt __startPt) 0)
    ((:endpts __endpts) (make-array 0 :initial-element 0 :element-type :integer))
    ((:captured __captured) (make-array 0 :initial-element 0 :element-type :integer))
    ((:makeKing __makeKing) 0)
    )
   (send-super :init)
   (setq _startPt (round __startPt))
   (setq _endpts __endpts)
   (setq _captured __captured)
   (setq _makeKing (round __makeKing))
   self)
  (:startPt
   (&optional __startPt)
   (if __startPt (setq _startPt __startPt)) _startPt)
  (:endpts
   (&optional __endpts)
   (if __endpts (setq _endpts __endpts)) _endpts)
  (:captured
   (&optional __captured)
   (if __captured (setq _captured __captured)) _captured)
  (:makeKing
   (&optional __makeKing)
   (if __makeKing (setq _makeKing __makeKing)) _makeKing)
  (:serialization-length
   ()
   (+
    ;; int32 _startPt
    4
    ;; int32[] _endpts
    (* 4    (length _endpts)) 4
    ;; int32[] _captured
    (* 4    (length _captured)) 4
    ;; int32 _makeKing
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _startPt
       (write-long _startPt s)
     ;; int32[] _endpts
     (write-long (length _endpts) s)
     (dotimes (i (length _endpts))
       (write-long (elt _endpts i) s)
       )
     ;; int32[] _captured
     (write-long (length _captured) s)
     (dotimes (i (length _captured))
       (write-long (elt _captured i) s)
       )
     ;; int32 _makeKing
       (write-long _makeKing s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _startPt
     (setq _startPt (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32[] _endpts
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _endpts (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _endpts i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; int32[] _captured
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _captured (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _captured i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; int32 _makeKing
     (setq _makeKing (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass checkers_ai::checkers_ai
  :super ros::object
  :slots ())

(setf (get checkers_ai::checkers_ai :md5sum-) "d5a3501b057d6c8e8efeb3c145b335d5")
(setf (get checkers_ai::checkers_ai :datatype-) "checkers_ai/checkers_ai")
(setf (get checkers_ai::checkers_ai :request) checkers_ai::checkers_aiRequest)
(setf (get checkers_ai::checkers_ai :response) checkers_ai::checkers_aiResponse)

(defmethod checkers_ai::checkers_aiRequest
  (:response () (instance checkers_ai::checkers_aiResponse :init)))

(setf (get checkers_ai::checkers_aiRequest :md5sum-) "d5a3501b057d6c8e8efeb3c145b335d5")
(setf (get checkers_ai::checkers_aiRequest :datatype-) "checkers_ai/checkers_aiRequest")
(setf (get checkers_ai::checkers_aiRequest :definition-)
      "uint32 currPlayer
uint32 oppntPlayer
uint32 bothIsKing
int8 moveDir
---
int32 startPt
int32[] endpts
int32[] captured
int32 makeKing

")

(setf (get checkers_ai::checkers_aiResponse :md5sum-) "d5a3501b057d6c8e8efeb3c145b335d5")
(setf (get checkers_ai::checkers_aiResponse :datatype-) "checkers_ai/checkers_aiResponse")
(setf (get checkers_ai::checkers_aiResponse :definition-)
      "uint32 currPlayer
uint32 oppntPlayer
uint32 bothIsKing
int8 moveDir
---
int32 startPt
int32[] endpts
int32[] captured
int32 makeKing

")



(provide :checkers_ai/checkers_ai "d5a3501b057d6c8e8efeb3c145b335d5")


