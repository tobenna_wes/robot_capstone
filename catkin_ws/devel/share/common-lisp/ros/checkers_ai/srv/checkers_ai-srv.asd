
(cl:in-package :asdf)

(defsystem "checkers_ai-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "checkers_ai" :depends-on ("_package_checkers_ai"))
    (:file "_package_checkers_ai" :depends-on ("_package"))
  ))