(cl:in-package checkers_ai-srv)
(cl:export '(CURRPLAYER-VAL
          CURRPLAYER
          OPPNTPLAYER-VAL
          OPPNTPLAYER
          BOTHISKING-VAL
          BOTHISKING
          MOVEDIR-VAL
          MOVEDIR
          STARTPT-VAL
          STARTPT
          ENDPTS-VAL
          ENDPTS
          CAPTURED-VAL
          CAPTURED
          MAKEKING-VAL
          MAKEKING
))