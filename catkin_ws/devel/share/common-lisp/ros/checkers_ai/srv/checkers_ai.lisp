; Auto-generated. Do not edit!


(cl:in-package checkers_ai-srv)


;//! \htmlinclude checkers_ai-request.msg.html

(cl:defclass <checkers_ai-request> (roslisp-msg-protocol:ros-message)
  ((currPlayer
    :reader currPlayer
    :initarg :currPlayer
    :type cl:integer
    :initform 0)
   (oppntPlayer
    :reader oppntPlayer
    :initarg :oppntPlayer
    :type cl:integer
    :initform 0)
   (bothIsKing
    :reader bothIsKing
    :initarg :bothIsKing
    :type cl:integer
    :initform 0)
   (moveDir
    :reader moveDir
    :initarg :moveDir
    :type cl:fixnum
    :initform 0))
)

(cl:defclass checkers_ai-request (<checkers_ai-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <checkers_ai-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'checkers_ai-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name checkers_ai-srv:<checkers_ai-request> is deprecated: use checkers_ai-srv:checkers_ai-request instead.")))

(cl:ensure-generic-function 'currPlayer-val :lambda-list '(m))
(cl:defmethod currPlayer-val ((m <checkers_ai-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:currPlayer-val is deprecated.  Use checkers_ai-srv:currPlayer instead.")
  (currPlayer m))

(cl:ensure-generic-function 'oppntPlayer-val :lambda-list '(m))
(cl:defmethod oppntPlayer-val ((m <checkers_ai-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:oppntPlayer-val is deprecated.  Use checkers_ai-srv:oppntPlayer instead.")
  (oppntPlayer m))

(cl:ensure-generic-function 'bothIsKing-val :lambda-list '(m))
(cl:defmethod bothIsKing-val ((m <checkers_ai-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:bothIsKing-val is deprecated.  Use checkers_ai-srv:bothIsKing instead.")
  (bothIsKing m))

(cl:ensure-generic-function 'moveDir-val :lambda-list '(m))
(cl:defmethod moveDir-val ((m <checkers_ai-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:moveDir-val is deprecated.  Use checkers_ai-srv:moveDir instead.")
  (moveDir m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <checkers_ai-request>) ostream)
  "Serializes a message object of type '<checkers_ai-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'currPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'currPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'currPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'currPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'oppntPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'oppntPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'oppntPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'oppntPlayer)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'bothIsKing)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'bothIsKing)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'bothIsKing)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'bothIsKing)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'moveDir)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <checkers_ai-request>) istream)
  "Deserializes a message object of type '<checkers_ai-request>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'currPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'currPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'currPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'currPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'oppntPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'oppntPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'oppntPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'oppntPlayer)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'bothIsKing)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'bothIsKing)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'bothIsKing)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'bothIsKing)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'moveDir) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<checkers_ai-request>)))
  "Returns string type for a service object of type '<checkers_ai-request>"
  "checkers_ai/checkers_aiRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'checkers_ai-request)))
  "Returns string type for a service object of type 'checkers_ai-request"
  "checkers_ai/checkers_aiRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<checkers_ai-request>)))
  "Returns md5sum for a message object of type '<checkers_ai-request>"
  "d5a3501b057d6c8e8efeb3c145b335d5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'checkers_ai-request)))
  "Returns md5sum for a message object of type 'checkers_ai-request"
  "d5a3501b057d6c8e8efeb3c145b335d5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<checkers_ai-request>)))
  "Returns full string definition for message of type '<checkers_ai-request>"
  (cl:format cl:nil "uint32 currPlayer~%uint32 oppntPlayer~%uint32 bothIsKing~%int8 moveDir~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'checkers_ai-request)))
  "Returns full string definition for message of type 'checkers_ai-request"
  (cl:format cl:nil "uint32 currPlayer~%uint32 oppntPlayer~%uint32 bothIsKing~%int8 moveDir~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <checkers_ai-request>))
  (cl:+ 0
     4
     4
     4
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <checkers_ai-request>))
  "Converts a ROS message object to a list"
  (cl:list 'checkers_ai-request
    (cl:cons ':currPlayer (currPlayer msg))
    (cl:cons ':oppntPlayer (oppntPlayer msg))
    (cl:cons ':bothIsKing (bothIsKing msg))
    (cl:cons ':moveDir (moveDir msg))
))
;//! \htmlinclude checkers_ai-response.msg.html

(cl:defclass <checkers_ai-response> (roslisp-msg-protocol:ros-message)
  ((startPt
    :reader startPt
    :initarg :startPt
    :type cl:integer
    :initform 0)
   (endpts
    :reader endpts
    :initarg :endpts
    :type (cl:vector cl:integer)
   :initform (cl:make-array 0 :element-type 'cl:integer :initial-element 0))
   (captured
    :reader captured
    :initarg :captured
    :type (cl:vector cl:integer)
   :initform (cl:make-array 0 :element-type 'cl:integer :initial-element 0))
   (makeKing
    :reader makeKing
    :initarg :makeKing
    :type cl:integer
    :initform 0))
)

(cl:defclass checkers_ai-response (<checkers_ai-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <checkers_ai-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'checkers_ai-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name checkers_ai-srv:<checkers_ai-response> is deprecated: use checkers_ai-srv:checkers_ai-response instead.")))

(cl:ensure-generic-function 'startPt-val :lambda-list '(m))
(cl:defmethod startPt-val ((m <checkers_ai-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:startPt-val is deprecated.  Use checkers_ai-srv:startPt instead.")
  (startPt m))

(cl:ensure-generic-function 'endpts-val :lambda-list '(m))
(cl:defmethod endpts-val ((m <checkers_ai-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:endpts-val is deprecated.  Use checkers_ai-srv:endpts instead.")
  (endpts m))

(cl:ensure-generic-function 'captured-val :lambda-list '(m))
(cl:defmethod captured-val ((m <checkers_ai-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:captured-val is deprecated.  Use checkers_ai-srv:captured instead.")
  (captured m))

(cl:ensure-generic-function 'makeKing-val :lambda-list '(m))
(cl:defmethod makeKing-val ((m <checkers_ai-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader checkers_ai-srv:makeKing-val is deprecated.  Use checkers_ai-srv:makeKing instead.")
  (makeKing m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <checkers_ai-response>) ostream)
  "Serializes a message object of type '<checkers_ai-response>"
  (cl:let* ((signed (cl:slot-value msg 'startPt)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'endpts))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    ))
   (cl:slot-value msg 'endpts))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'captured))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    ))
   (cl:slot-value msg 'captured))
  (cl:let* ((signed (cl:slot-value msg 'makeKing)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <checkers_ai-response>) istream)
  "Deserializes a message object of type '<checkers_ai-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'startPt) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'endpts) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'endpts)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296)))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'captured) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'captured)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296)))))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'makeKing) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<checkers_ai-response>)))
  "Returns string type for a service object of type '<checkers_ai-response>"
  "checkers_ai/checkers_aiResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'checkers_ai-response)))
  "Returns string type for a service object of type 'checkers_ai-response"
  "checkers_ai/checkers_aiResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<checkers_ai-response>)))
  "Returns md5sum for a message object of type '<checkers_ai-response>"
  "d5a3501b057d6c8e8efeb3c145b335d5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'checkers_ai-response)))
  "Returns md5sum for a message object of type 'checkers_ai-response"
  "d5a3501b057d6c8e8efeb3c145b335d5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<checkers_ai-response>)))
  "Returns full string definition for message of type '<checkers_ai-response>"
  (cl:format cl:nil "int32 startPt~%int32[] endpts~%int32[] captured~%int32 makeKing~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'checkers_ai-response)))
  "Returns full string definition for message of type 'checkers_ai-response"
  (cl:format cl:nil "int32 startPt~%int32[] endpts~%int32[] captured~%int32 makeKing~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <checkers_ai-response>))
  (cl:+ 0
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'endpts) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'captured) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <checkers_ai-response>))
  "Converts a ROS message object to a list"
  (cl:list 'checkers_ai-response
    (cl:cons ':startPt (startPt msg))
    (cl:cons ':endpts (endpts msg))
    (cl:cons ':captured (captured msg))
    (cl:cons ':makeKing (makeKing msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'checkers_ai)))
  'checkers_ai-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'checkers_ai)))
  'checkers_ai-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'checkers_ai)))
  "Returns string type for a service object of type '<checkers_ai>"
  "checkers_ai/checkers_ai")