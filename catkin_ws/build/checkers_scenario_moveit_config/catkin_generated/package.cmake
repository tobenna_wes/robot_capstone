set(_CATKIN_CURRENT_PACKAGE "checkers_scenario_moveit_config")
set(checkers_scenario_moveit_config_VERSION "0.3.0")
set(checkers_scenario_moveit_config_MAINTAINER "Tobenna Wes <toennaw@gmail.com>")
set(checkers_scenario_moveit_config_PACKAGE_FORMAT "1")
set(checkers_scenario_moveit_config_BUILD_DEPENDS )
set(checkers_scenario_moveit_config_BUILD_EXPORT_DEPENDS "moveit_ros_move_group" "moveit_fake_controller_manager" "moveit_kinematics" "moveit_planners_ompl" "moveit_ros_visualization" "moveit_setup_assistant" "joint_state_publisher" "joint_state_publisher_gui" "robot_state_publisher" "tf2_ros" "xacro" "checkers_scenario")
set(checkers_scenario_moveit_config_BUILDTOOL_DEPENDS "catkin")
set(checkers_scenario_moveit_config_BUILDTOOL_EXPORT_DEPENDS )
set(checkers_scenario_moveit_config_EXEC_DEPENDS "moveit_ros_move_group" "moveit_fake_controller_manager" "moveit_kinematics" "moveit_planners_ompl" "moveit_ros_visualization" "moveit_setup_assistant" "joint_state_publisher" "joint_state_publisher_gui" "robot_state_publisher" "tf2_ros" "xacro" "checkers_scenario")
set(checkers_scenario_moveit_config_RUN_DEPENDS "moveit_ros_move_group" "moveit_fake_controller_manager" "moveit_kinematics" "moveit_planners_ompl" "moveit_ros_visualization" "moveit_setup_assistant" "joint_state_publisher" "joint_state_publisher_gui" "robot_state_publisher" "tf2_ros" "xacro" "checkers_scenario")
set(checkers_scenario_moveit_config_TEST_DEPENDS )
set(checkers_scenario_moveit_config_DOC_DEPENDS )
set(checkers_scenario_moveit_config_URL_WEBSITE "http://moveit.ros.org/")
set(checkers_scenario_moveit_config_URL_BUGTRACKER "https://github.com/ros-planning/moveit/issues")
set(checkers_scenario_moveit_config_URL_REPOSITORY "https://github.com/ros-planning/moveit")
set(checkers_scenario_moveit_config_DEPRECATED "")