# generated from catkin/cmake/em/order_packages.cmake.em

set(CATKIN_ORDERED_PACKAGES "")
set(CATKIN_ORDERED_PACKAGE_PATHS "")
set(CATKIN_ORDERED_PACKAGES_IS_META "")
set(CATKIN_ORDERED_PACKAGES_BUILD_TYPE "")
list(APPEND CATKIN_ORDERED_PACKAGES "atlas_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/atlas_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "baxter_common")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "baxter_common/baxter_common")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "baxter_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "baxter_common/baxter_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "checkers_robot_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "checkers_robot_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "checkers_scenario_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "checkers_scenario_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "iri_wam_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/iri_wam_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "moveit_robots")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/moveit_robots")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "r2_moveit_generated")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/r2_moveit_generated")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rethink_ee_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "baxter_common/rethink_ee_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "baxter_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/baxter/baxter_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_2f_140_gripper_visualization")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_2f_140_gripper_visualization")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_2f_85_gripper_visualization")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_2f_85_gripper_visualization")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_2f_c2_gripper_visualization")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_2f_c2_gripper_visualization")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "realsense2_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "realsense2_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "baxter_maintenance_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "baxter_common/baxter_maintenance_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "dynamixel_workbench_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "dynamixel-workbench-msgs/dynamixel_workbench_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "universal_robot")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/universal_robot")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "universal_robots")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/universal_robots")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_dashboard_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "Universal_Robots_ROS_Driver/ur_dashboard_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_e_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_e_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "geometric_shapes")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "geometric_shapes")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "checkers_ai")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "checkers_ai")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "pysdf")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "pysdf")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "gazebo2rviz")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "gazebo2rviz")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_ethercat")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_ethercat")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_2f_gripper_control")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_2f_gripper_control")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_ft_sensor")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_ft_sensor")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_modbus_rtu")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_modbus_rtu")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_modbus_tcp")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_modbus_tcp")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_arg85_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq_arg85_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "controller_stopper")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "Universal_Robots_ROS_Driver/controller_stopper")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robotiq_2f_gripper_action_server")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robotiq/robotiq_2f_gripper_action_server")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "baxter_core_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "baxter_common/baxter_core_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "checkers_scenario")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "checkers_scenario")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "real_checkers_game")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "real_checkers_game")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "realsense2_camera")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "realsense2_camera")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "realsense_gazebo_plugin")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "realsense_gazebo_plugin")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_bringup")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_bringup")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_calibration")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "Universal_Robots_ROS_Driver/ur_calibration")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_driver")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_driver")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_e_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_e_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "baxter_ikfast_left_arm_plugin")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/baxter/baxter_ikfast_left_arm_plugin")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "baxter_ikfast_right_arm_plugin")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/baxter/baxter_ikfast_right_arm_plugin")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "atlas_v3_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_robots/atlas_v3_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "moveit_visual_tools")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "moveit_visual_tools")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_kinematics")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur_kinematics")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "grasping_checkers")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "grasping_checkers")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "fh_desc")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "smart_grasping_sandbox/fh_desc")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "smart_grasp_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "smart_grasping_sandbox/smart_grasp_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "smart_grasping_sandbox")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "smart_grasping_sandbox/smart_grasping_sandbox")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur10_e_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur10_e_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur10_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur10_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur3_e_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur3_e_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur3_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur3_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur5_e_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur5_e_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur5_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "fmauch_universal_robot/ur5_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_controllers")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "Universal_Robots_ROS_Driver/ur_controllers")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ur_robot_driver")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "Universal_Robots_ROS_Driver/ur_robot_driver")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")

set(CATKIN_MESSAGE_GENERATORS )

set(CATKIN_METAPACKAGE_CMAKE_TEMPLATE "/usr/lib/python3/dist-packages/catkin_pkg/templates/metapackage.cmake.in")
