# CMake generated Testfile for 
# Source directory: /home/tobenna/robot_capstone/catkin_ws/src
# Build directory: /home/tobenna/robot_capstone/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("moveit_robots/atlas_moveit_config")
subdirs("baxter_common/baxter_common")
subdirs("baxter_common/baxter_description")
subdirs("checkers_robot_config")
subdirs("checkers_scenario_moveit_config")
subdirs("moveit_robots/iri_wam_moveit_config")
subdirs("moveit_robots/moveit_robots")
subdirs("moveit_robots/r2_moveit_generated")
subdirs("baxter_common/rethink_ee_description")
subdirs("moveit_robots/baxter/baxter_moveit_config")
subdirs("robotiq/robotiq")
subdirs("robotiq/robotiq_2f_140_gripper_visualization")
subdirs("robotiq/robotiq_2f_85_gripper_visualization")
subdirs("robotiq/robotiq_2f_c2_gripper_visualization")
subdirs("realsense2_description")
subdirs("baxter_common/baxter_maintenance_msgs")
subdirs("dynamixel-workbench-msgs/dynamixel_workbench_msgs")
subdirs("fmauch_universal_robot/universal_robot")
subdirs("fmauch_universal_robot/universal_robots")
subdirs("Universal_Robots_ROS_Driver/ur_dashboard_msgs")
subdirs("fmauch_universal_robot/ur_description")
subdirs("fmauch_universal_robot/ur_e_description")
subdirs("fmauch_universal_robot/ur_msgs")
subdirs("geometric_shapes")
subdirs("checkers_ai")
subdirs("pysdf")
subdirs("gazebo2rviz")
subdirs("robotiq/robotiq_ethercat")
subdirs("robotiq/robotiq_2f_gripper_control")
subdirs("robotiq/robotiq_ft_sensor")
subdirs("robotiq/robotiq_modbus_rtu")
subdirs("robotiq/robotiq_modbus_tcp")
subdirs("robotiq_arg85_description")
subdirs("Universal_Robots_ROS_Driver/controller_stopper")
subdirs("robotiq/robotiq_2f_gripper_action_server")
subdirs("baxter_common/baxter_core_msgs")
subdirs("checkers_scenario")
subdirs("real_checkers_game")
subdirs("realsense2_camera")
subdirs("realsense_gazebo_plugin")
subdirs("fmauch_universal_robot/ur_bringup")
subdirs("Universal_Robots_ROS_Driver/ur_calibration")
subdirs("fmauch_universal_robot/ur_driver")
subdirs("fmauch_universal_robot/ur_e_gazebo")
subdirs("fmauch_universal_robot/ur_gazebo")
subdirs("moveit_robots/baxter/baxter_ikfast_left_arm_plugin")
subdirs("moveit_robots/baxter/baxter_ikfast_right_arm_plugin")
subdirs("moveit_robots/atlas_v3_moveit_config")
subdirs("moveit_visual_tools")
subdirs("fmauch_universal_robot/ur_kinematics")
subdirs("grasping_checkers")
subdirs("smart_grasping_sandbox/fh_desc")
subdirs("smart_grasping_sandbox/smart_grasp_moveit_config")
subdirs("smart_grasping_sandbox/smart_grasping_sandbox")
subdirs("fmauch_universal_robot/ur10_e_moveit_config")
subdirs("fmauch_universal_robot/ur10_moveit_config")
subdirs("fmauch_universal_robot/ur3_e_moveit_config")
subdirs("fmauch_universal_robot/ur3_moveit_config")
subdirs("fmauch_universal_robot/ur5_e_moveit_config")
subdirs("fmauch_universal_robot/ur5_moveit_config")
subdirs("Universal_Robots_ROS_Driver/ur_controllers")
subdirs("Universal_Robots_ROS_Driver/ur_robot_driver")
